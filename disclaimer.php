<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Disclaimer</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Disclaimer</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  





<div class="container content __web-inspector-hide-shortcut__">
      <div class="row">
        <div class="col-md-12">
  <p class="text-justify" style="color:#fff">The information contained in this website is for general information purposes only and collected from different sources. The information is provided by www.Rgyan.com and while we endeavour to keep the information up to date and correct, but, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.<br><br>In no event and not any case will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.<br><br>Through this website you are able to link to other websites which are not under the control of www.Rgyan.com. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.<br><br>Every effort is made to keep the website up and running smoothly. However, www.Rgyan.com takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</p>
  </div>
  </div>
  </div>
  
  <!--footer section start-->
   <?php include_once('pages/footer.php'); ?>