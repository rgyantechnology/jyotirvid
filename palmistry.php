<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
<?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Palmistry</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Palmistry</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  


<div class="container content __web-inspector-hide-shortcut__">
    <div class="row">
      <div class="col-md-12">
        <div class="headin_link">
          <div class="row">
            <div class="col-md-12">
              <h3 class="pull-left">The Art of Palm Reading and its Basics</h3>
              <h4 class="pull-right headin_size" class="pull-right"><a href="search-result.php?category=palmistry">Get In Touch With Palmist</a></h4>
            </div>
          </div>
        </div>
          <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/2.gif">
                        <span class="p_text">The Art:</span> of palm reading characterizes a person’s personalities, fortune and future by analyzing his/her hands. It is known as palmistry or chiromancy. This is one of the most popular methods in the world to read someone’s life and personality. The people who practice this art are generally called palmists, palm readers, hand readers, hand analysts, or chirologists.
                        <br><span class="p_text">Origin:</span> The science of palm reading is a golden gift to the world from ancient Vedic literature of India. It is originated in India and then spread throughout China finding it’s way to Egypt, Greece and then to Europe. According to some archaeologist; the concept of palm reading was developed in between 6000 to 4000 BCE. But it is functionally accepted by Indus Valley Civilization after 3000 BCE. In ancient India palm reading was practiced only by Brahmans. They were followed “Sariraka Shastra” to learn the art of palm reading. In modern age; many people practice or learn this science; but only some specific artists are able to understand and specialize in this ancient science.<br><br>
                        <span class="p_text"><b>Basics of Palm reading by Hand Shape</b></span><br>
                        The science of palm reading is not just about the reading of lines of palm. The colour, shape and size of palm or fingers also matters. 
                        <br>Vastu vidya is not completely science prove. Although it never violate any rules defined by science. The science has no answers on “how the longest towers and giant stone temples of ancient India are survived thousands years and still not aged yet?”. So it is better to belief than questioning.<br><br>
                        The major theories of palm reading on hand shape and colour are mentioned below:
                        <br><span class="p_text">Earth:</span>hands are generally identified by broad, square palms and fingers, thick or coarse skin, and ruddy colour. The length of the palm from wrist to the bottom of the fingers is usually equal to the length of the fingers.

                        <br><span class="p_text">Air:</span>hands exhibit square or rectangular palms with long fingers and sometimes protruding knuckles, low-set thumbs, and often dry skin. The length of the palm from wrist to the bottom of the fingers is usually equal to the length of the fingers.

                        <br><span class="p_text">Water:</span>hands are see-able by the long, sometimes oval-shaped palm, with long, flexible, conical fingers. The length of the palm from wrist to the bottom of the fingers is usually less than the width across the widest part of the palm, and usually equal to the length of the fingers.

                        <br><span class="p_text">Fire:</span>hands are characterized by a square or rectangular palm, flushed or pink skin, and shorter fingers. The length of the palm from wrist to the bottom of the fingers is usually greater than the length of the fingers.
                        
                        
                        
                        </p>
                        
                        
                       <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/3.gif">
                        
                        <span class="p_text"><b>Basics of palmistry by lines of palm</b></span><br>
                        In palmistry, there are mainly three major lines to read which are Life Line, Head Line (also Wisdom Line) and Heart Line (also Love Line). Besides, there are also other lines which are also important in palm line reading such as Marriage line, Fate line, Sun line, Children Line, Money Line and Health Line. 
                  

                        <br><span class="p_text">Life Line:</span>The life line is one of the three major lines and it starts from the palm edge between the thumb and forefinger and extends to the base of the thumb. Most of us know that the line is used to see the length of one's life. Actually, it mainly reflects a person's physical vitality and life energy. Also, it shows if one will have accidents or serious illnesses during the whole life. If other lines of your palm are clear and only the life line is absent, it's not a good sign. It indicates a poor health and a short life. 

                        <br><span class="p_text">Head Line:</span>The wisdom line is the second important line in palm reading. It starts from the palm edge between the thumb and forefinger, and extends across the palm in the middle part with the life line below and the heart line above. This line identifies a person’s wisdom, skills and characteristics like belief, attitude, thinking ability, strain capacity, creative ability, abilities of memory, self-control and more.    

                        <br><span class="p_text">Heart Line:</span>The heart line is also popularly known as Love line. It's just above the head line starting from the edge of the palm under the little finger, running across the palm and ending below the middle finger or forefinger or the place where they join. This line shows our attitude to Love, marriage life, family and other relationships. It also describes our affection and emotion status.

                        <br><span class="p_text">Fate Line:</span>After knowing the timing of the fate line, you could easily find out the fortune for your career and destiny at different ages. No fate line indicates you always change or don’t have specific destiny in your life.

                        <br><span class="p_text">Marriage Line:</span>Marriage line or affection line mainly reflects the situation of a person’s marriage life, love relationship, marriage time as well as your attitude towards love.
                        
                        <br><span class="p_text">Sun Line:</span>The sun line is not a major line; but important one as it reflects the capability, talent and popularity which may lead to success. A long sun line is better than a short one and people with a sun line are better than those without it.

                        <br><span class="p_text">Children Line:</span>This line shows the number of children you may have or their life status.

                        <br><span class="p_text">Money Line:</span>It reflects financial status of a person. It also indicates the flow of money or wealth in your entire life.

                        <br><span class="p_text">Health Line:</span>After knowing the timing of the fate line, you could easily find out the fortune for your career and destiny at different ages. No fate line indicates you always change or don’t have specific destiny in your life.
                        <br>We all should remember that “future is unpredictable”. It is nearly impossible to know the future. The art of palmistry never reflects any one’s future. It describes the path and energy of life and gives us an opportunity to be prepared for better future.
                        <br>Need help to have an appointment with a palm reading specialist; click here to find a palmist available on your location. 

                        
                        
                        </p>
        </div>
               

      
      </div><!--/row-fluid-->
    </div>
  
  <!--footer section start-->
 <?php include_once('pages/footer.php'); ?>