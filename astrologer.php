<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
   <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Astrologer</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Astrologer</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  




<div class="container content __web-inspector-hide-shortcut__">
    <div class="row">
      <div class="col-md-12">
        <div class="headin_link">
          <div class="row">
            <div class="col-md-12">
              <h3 class="pull-left">Astrology – The science of Planets</h3>
              <h4 class="pull-right headin_size" class="pull-right"><a href="search-result.php?category=astrologer">Get In Touch With Astrologers</a></h4>
            </div>
          </div>
        </div>
          <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/astrology-histroy.jpg">
                        Astrology is a very minute study of the celestial bodies and their positioning. They provide information about personality, human affairs, and other terrestrial matters.<br>
                        Astrological systems were developed independently in every culture around the globe. Each was unique to its society. Beyond the need for a calendar, the Egyptian, Persian, Chinese, and Mayan astrological systems have very little in common. Each expands into its own mythology of gods and alchemy.

                        <br><span class="p_text"><b>Need of Astrology</b></span><br> Desire of knowing our own future, is the reason for evolution of Astrology. What will happen in Future and if there is something we can do to prevent misfortune. Astrology has been in many times very accurate in providing people the necessary tips and tricks. Making life a better place is what this magical science aims at.<br>

                       <br><span class="p_text"><b>Astrological System is studied depending on these categories.</b></span><br>

                        <br><span class="p_text"><b>Planetary System</b></span><br>Astrology provides and helps to get planets discovered and their influence on each human being. Factors like the Date of Birth, time and location are taken into consideration. According to this the planet and its positions are calculated to formulate further decisions. Astrology has more or less been based on the naked-eye astronomy, and hence, has not accounted for the more recent discoveries. We have only discovered the nine Planets of Solar Systems but possibilities are that there may be more planets in the Solar System or any other solar systems.
                        
                        
                        </p>
                        
                        
                         <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/bodies-of-heaven.jpg">
                        <span class="p_text">Bodies of Heaven</span><br>Astrologer’s claim that heavenly bodies also have an influence on us. Things like quasars, pulsars, black-holes, or dark matter are not accounted in the system.
                        
                        <br><span class="p_text">Gravity</span> <br>It is the next big factor in astrology, which helps the astrologers in calculation and prediction of the future. As per the theory of Gravitation, the gravitational pull between two objects is inversely proportional to the distance between the two objects. It has been proved beyond doubt that there indeed is gravitational attraction between any two given objects. This pulls up another question: the pull between a planet and a human is much smaller than the pull two humans will exert on each other. Then why is the gravitational pull between humans around us is not considered in the theory of astrology.<br>
                        The article by Phil Plait’s actually does the whole lot of computations between gravitational and tidal forces that act upon us. The upshot of this is, at the moment of birth the gravitational pull of the midwife’s gloves far exceeds all of the combined gravitational pull of everything out there in space.

                        <br><span class="p_text">Frequencies: Radiation and Electromagnetic</span> <br>Similar to gravitation, radiation is believed to be the cause because each planet and body emits a frequency that somehow interacts with us. Now in the whole galaxy, considering the target as Earth, the biggest source of electromagnetic force is the Sun. In which case, it comes up as a surprise that much more emphasis is laid on stars than the sun.<br>

                        <br><span class="p_text">Precession of the Earth</span> <br>The constellations shift by about 1 degree every 72 years, thanks to the drifting of the Earth’s spin. The Earth spins around an imaginary spin axis that runs through the North and South Geographic Poles. But it doesn’t spin true. If you have ever spun a top, you’ll see that this spin axis soon begins to wobble. The spin axis will slowly sweep out a complete circle.<br>
                        The same thing happens with the spin axis of the Earth - except that it takes about 26,000 years to sweep out a complete circle. So roughly every 2,000-and-a-bit years (26,000 years divided by 12 Houses), the star signs get shifted by one House.
                        <br>This is not a new discovery. Back in 129 BC, Hipparchus was the first to find this shifting-of-the-stars when he compared the astronomical records with what he saw with his eyes.<br><br>
                        Considering this magical and vast science as an urgency for all of us, we have tried to bring the most professional astrologers on lin
                        </p>
        </div>
               

      
      </div><!--/row-fluid-->
    </div>

  
  <!--footer section start-->
   <?php include_once('pages/footer.php'); ?>