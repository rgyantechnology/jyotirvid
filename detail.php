 <?php 
error_reporting(0);
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
$condition = " WHERE astro.id = ".$_REQUEST['id']."  AND astro.is_active = 1 AND astro.is_delete = 0 HAVING astro.id";
$search = 'SELECT DISTINCT astro.*, group_concat(DISTINCT ca.name) AS astrocat, 
group_concat(DISTINCT se.name) AS astroserv FROM astrologers  AS astro
LEFT JOIN astrologercategories AS ac ON(ac.astrologer_id = astro.id)
INNER JOIN categories AS ca ON(ca.id = ac.category_id)
LEFT JOIN astrologerservices AS sa ON(sa.astrologer_id = astro.id)
INNER JOIN services AS se ON(se.id = sa.service_id)';
$sql = $search.$condition;
$searchquery = $db->prepare($sql);
$searchquery->execute();
$res = $searchquery->fetch();
?>
<!doctype html>
<head>
<title>Astrologer | <?php echo ucfirst($res['fname']).'&nbsp;'.ucfirst($res['lname']) ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link href="css/review.css" rel="stylesheet">
<link href="css/mctabs.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/media.css">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/profile.css">
<link rel="stylesheet" id="fontawesome-css" href="css/font-awesome_002.css" type="text/css" media="all">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href="css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="chosen/chosen.css">
<style type="text/css" media="all">
/* fix rtl for demo */
.chosen-rtl .chosen-drop { left: -9000px; }
</style>
<style>
.map {
	min-width: 300px;
	min-height: 300px;
	width: 100%;
	height: 100%;
}
.error{
	color:red;
}
</style>
</head>
<body>
<div id="main">
<?php include('pages/header.php')?>
  <!--end of header Section--> 
   <!--Search Aera-->
  <div class="container">
      <div class="form-section form_border">
        <div class="row">
          <form id="suggestSearch" name="suggestSearch" action="search-result.php" method="get" enctype="multipart/form-data" onsubmit="" onKeyPress="" >
            <div class="col-md-2">
              <div class="form-group form-group-padding ">
				<select data-placeholder="Select State" name="state" id="state" class="form-control"  style="width:180px; height:35px;" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:2}, 'city')" tabindex="2">
				<?php echo showstate($state); ?>
				</select>
              </div>
            </div>
			
			<div class="col-md-2">
              <div class="form-group form-group-padding ">
                <select data-placeholder="City" id="city" class="form-control"  style="width:180px; height:35px;" tabindex="2">
            <?php echo showcity(); ?>
          </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-padding">
                <label class="sr-only" for="location">Location</label>
                <input id="category" name="category" type="text" autocomplete="off" onBlur="if(this.value=='')this.value='Search Astrologer, Pandit and Dharmkathawachak'" onFocus="if(this.value=='Search Astrologer, Pandit and Dharmkathawachak')this.value=''" value="Search Astrologer, Pandit and Dharmkathawachak" class="form-control" />
              </div>
            </div>
            
            <div class="col-md-1">
              <button type="submit" class="btn btn-default btn-primary">Go</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!--end of Search Aera--> 
  
  <!--breadcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Search Astrologer, Pandit and Dharmkathawachak</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li><a href="search-result.php">Astrologer</a></li>
        <li class="active"><?php echo ucfirst($res['fname']).'&nbsp;'.ucfirst($res['lname'])?></li>
      </ul>
    </div>
  </div>
  <!--end of bradcome--> 
  
  <!--profile secion-->
 <?php 
    $name = $res['fname']."&nbsp;".$res['lname'];
	$address = $res['address1'].', '.$res['address2']."<br />".$res['area'].', '.$city = getcityname($res['city'])."<br />".$state =  getstatename($res['state']).'-'.$res['pincode'].', India';
    $id_array = $res['id'];
	$name_array = $name;
	$map_array = $res['mapembeddedcode'];
	$address_array = $address;
	?>
  <div class="container">
    <div class="box2">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2"><a href=""><?php if($res['image']){
				$path = 'upload/'.$res['image']; 
				if(file_exists($path)){ ?>
				<img class="img-thumbnail img-responsive" src="<?php echo LINK_URL_HOME ?>upload/<?php echo $res['image'] ?>" alt = "<?php echo $res['fname'].'&nbsp;'. $res['fname'];?>">
				<?php }  else{ ?><img class="img-thumbnail img-responsive" src="<?php echo LINK_URL_HOME ?>img/person-icon.jpg" alt = "<?php echo $res['fname'].'&nbsp;'. $res['fname'];?>"><?php }} ?></a> <br>
            <div class="btn_review"> <a class="btn-danger btn_padding" data-toggle="modal" data-target="#enquirypopup" href="#enquirypopup">Send Enquiry</a> <a class="btn-danger btn_padding" href="#WriteReviewHere">Write Review</a> </div>
          </div>
          <div class="col-md-7">
            <p><a href=""><span class="span_color"><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?>&nbsp;|&nbsp;Astrologer&nbsp;</span></a></p>
            <p><a href=""><span class="span_color_exp">Exp:&nbsp;<?php echo $res['experience']?>&nbsp;Years</span></a></p>
            <p> <a style = "color: #fff;" href="#searchmap" data-toggle="modal"><i class="fa fa-map-marker" aria-hidden="true" title= "Address on map"></i>&nbsp;<?php echo $res['address1']?>, <?php echo $res['address2']?><br />
                             <?php echo $res['area']?>, <?php echo $city = getcityname($res['city']);?>&nbsp;
							 <?php echo $state =  getstatename($res['state']); ?>- <?php echo $res['pincode'] ?>, <?php //echo $country =  getcountryname(100); ?> India <br /></a></p>
			<p><a href=""><span class="service_color">Services:&nbsp;</span><?php echo $res['astroserv'] ?></a></p>
           <p><span class="service_color">Categories:</span>&nbsp;<?php echo getcategory($res['id']);?><?php //echo $_res['astrocat']?></p>
            <figcaption class="ratings">
              <p><span class="service_color">Ratings: </span> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> </p>
            </figcaption>
            <ul class="list-inline social_margin">
              <li><a href="<?php echo $res['facebookpage'] ?>"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
              <li><a href="#"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
              <li><a href="#"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
              <!--<li><a href="#"><span class="socilsize"><i class="fa fa-pinterest-p" aria-hidden="true"></i></span></a></li>
                                          <li><a href="#"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></i></span></a></li>-->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel-footer">
          <div class="tabbable-panel">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active"><a href="#tab_default_1" data-toggle="tab"> About Me</a> </li>
               <!-- <li> <a href="#tab_default_2" data-toggle="tab"> Best Deal</a> </li>-->
                <li> <a href="#tab_default_5" data-toggle="tab"> Contact Us</a> </li>
                <li> <a href="#tab_default_3" data-toggle="tab"> Reviews</a> </li>
                <li> <a href="#tab_default_4" data-toggle="tab"> Feed back</a> </li>
                <li> 
                  <!-- Trigger the modal with a button --> 
                  
                  <!-- Modal -->
                  <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog"> 
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Write new news feed</h4>
                        </div>
                        <div class="modal-body">
                          <p>Some text in the modal.</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btnbtn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1" active> <br>
                  <div class="row">
                    <div class="col-md-6">
                    <h4 class="col_heading4"><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></h4>
                      <?php echo str_replace('\r\n', '<br>', strip_tags($res['aboutus'], '<p>, <strong>, <span>, <br />'))?>
                      <h4 class="col_heading4">Services</h4>
                      <p><?php echo $res['astroserv'] ?></p>
					  <?php
						$otherQuery = $db->prepare("SELECT * FROM serviceothers WHERE astrologers_id = $res[id]");
						$otherQuery->execute();
						$oser = $otherQuery->fetch();
						if(!empty($oser)){
						 ?>
					  <h4 class="col_heading4">Other</h4>
                      <p><?php echo $oser['otherinfo']?></p>
                      <?php } ?>
					   <h4 class="col_heading4">Categories</h4>
					  <p><?php echo getcategory($res['id']);?></p>
                       <h4 class="col_heading4">Timing & Availability</h4>
					   <?php if(!empty($res['timing'])){
						   ?>
                      <p><table width="55%" cellspacing="0" cellpadding="0" border="0" style="padding-left:5px;">
		  <?php 
		  $Timing= explode('<br>', $res['timing']);
		  $Length=count($Timing);
		  for($i=0; $i<$Length;$i++)
		  {
		  	$Day=explode(':', $Timing[$i]);
			$DayName=$Day[0];
			?>
			<tr>
			<td><?php echo $DayName?></td>
			<td>:</td>
			<td><?php 
			$DayClosed=array("</b> MonClosed", "</b> TueClosed", "</b> WedClosed", "</b> ThrusClosed", "</b> FriClosed", "</b> SatClosed", "</b> SunClosed" );
			if($Day[1]=="</b> MonClosed" or $Day[1]=="</b> TueClosed" or $Day[1]=="</b> WedClosed" or $Day[1]=="</b> ThursClosed" or $Day[1]=="</b> FriClosed" or $Day[1]=="</b> SatClosed" or $Day[1]=="</b> SunClosed")
			{
				echo "Closed";
				
			}
			else{
					
				$time=explode('to', $Day[1]);
				if($time[0]=="</b> Open 24 Hours")
				{
					echo $time[0];
				}
				else{
				if($time[0]=="</b> ")
				{
					echo "Time is not specified by the Business Owner";
				}
				else{
				echo $time[0]."&nbsp;&nbsp;To&nbsp;&nbsp;".$time[1];
				}
				}
				
			}
			 ?></td>
			</tr>
		<?php
		 }
		  
		 ?></table>
		  <?php }?>
		</p>
                      
                      
                     <h4 class="col_heading4" id = 'WriteReviewHere'>Write Review Here</h4>
                      <div class="horizontal-row-1">
                        <div class="clinic-container">
                          <div id="review-bg-left">
						  <span id="review-success" style="display:none;"></span>
                            <form name="reviewform" id = "reviewform" action="process.php" onsubmit="return validate_review();" method="post" enctype="multipart/form-data">
							<input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>>
							<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "REVIEW">
                              <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tbody>
                                  <tr>
                                    <td width="38%"><div class="category-left-heading">Write a Review</div></td>
                                    <td colspan="2" align="center"><div class="blue-txt" id="rateresult">Rate&nbsp;<strong><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></strong></div></td>
                                  </tr>
                                  <tr>
                                    <td valign="top" width="90%">Write a review for <strong><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></strong></td>
                                    <td width="19%" valign="top" style="text-align:right; color:#ff9900; font-size:18px; padding-right:8px; font-weight:bold;">Your Rating:</td>
                                    <td width="5%" align="right"><div id="rate-container-width">
                                        <div class="input select rating-c">
                                          <select id="example-c" name="rating" style="display: none;" class = "required">
                                            <option value=""></option>
                                            <option value="1"></option>
                                            <option value="2"></option>
                                            <option value="3"></option>
                                            <option value="4"></option>
                                            <option value="5"></option>
                                          </select>
										  <span class = "error" id="rate-error" style="display:none;"></span>
                                        </div>
                                      </div></td>
                                  </tr>
                                </tbody>
                              </table>
                              <table width="100%" border="0" align="left">
                                <tbody>
                                  <tr>
                                    <td width="19%" align="left" valign="top" style="font-size:13px; font-weight:bold;">Description <span class="orange">*</span></td>
                                    <td width="81%"><textarea id="rate_description" name="description" cols="" rows="" class="review-description"></textarea>
									<span class = "error" id="description-error" style="display:none;"></span>
									</td>
                                  
								  </tr>
                                  <tr>
                                    <td colspan="2" align="left" valign="top" style="font-size:13px; font-weight:bold;"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tbody>
                                          <tr>
                                            <td width="19%" align="left" valign="top" style="font-size:13px; font-weight:bold;">Name <span class="orange">*</span></td>
                                            <td width="49%" height="20" valign="middle"><input id = "rate_name" name="name" type="text" class="review-name required lettersonly" onKeyPress="$('#rate_name-error').fadeOut();">
											<span class = "error" id="rate_name-error" style="display:none;"></span>
											</td>
                                            <td width="33%" rowspan="3" valign="top"><table width="38%" border="0" align="center">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="middle"><div id="user-profile"><img id="uploadPreview" src="imges/user-photo.jpg" width="88" height="80"></div></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table width="100%" border="0" align="left">
                                                <tbody>
                                                  <tr>
                                                    <td align="left"><div class="custom_file_upload"> 
                                                        <!--<input type="text" class="file" name="file_info">-->
                                                        <div class="file_upload">
                                                          <input type="file" id="file_upload" name="file_upload" onchange="PreviewImage();">
                                                        </div>
                                                      </div></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" style="font-size:13px; font-weight:bold;">Mobile <span class="orange">*</span></td>
                                            <td height="25" valign="middle"><input id= "rate_mobile" name="mobile" onKeyPress="$('#error_mobile-error').fadeOut();" type="text" class="review-name required number">
											<span class = "error" id = "rate_mobile-error" style="display:none"></span>
											</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" style="font-size:13px; font-weight:bold;">Email <span class="orange">*</span></td>
                                            <td height="25" valign="middle"><input id= "rate_email" name="email"  onKeyPress="$('#error_email-error').fadeOut();" type="text" class="review-name required email">
											<span class = "error" id = "rate_email-error" style="display:none"></span>
											</td>
                                          </tr>
                                          <tr>
                                            <td width="18%" height="45" valign="top">&nbsp;</td>
                                            <td width="49%" height="45" valign="middle">&nbsp;</td>
                                            <td width="33%" height="45" align="center" valign="bottom"><input type="hidden" name="rid" value="<?php echo $res['id']?>"><br />
                                            <input name="Submit_Review" type="submit" value="Submit" class=" btn-sm  btn_size btn-primary center-block"></td>
                                              
                                          </tr>
                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                            </form>
                          </div>
						<script type="text/javascript">
                        function PreviewImage() {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("file_upload").files[0]);
                        
                        oFReader.onload = function (oFREvent) {
                        document.getElementById("uploadPreview").src = oFREvent.target.result;
                        };
                        };
                        </script> 
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="tab-pane" id="tab_default_2"> <br>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <div class="tab-pane" id="tab_default_3"> <br>
                  
                  
                  
                  					<div class="row">
<div class="col-sm-12">
<h3>User  Reviews(0)</h3>
</div><!-- /col-sm-12 -->
</div><!-- /row -->
</div>
                <div class="tab-pane" id="tab_default_5"> <br>
                  <div class="row">
                    <div class="col-md-6">
                      <div style="background-color: #201300;" class="well well-sm">
					  <p id = "contactussuccess" style = "display:none;"></p>
                        <form class="form-horizontal" method="post" id = "contactus" onsubmit = "return false;">
						<input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>>
						<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "CONTACTUS">
                          <fieldset>
                            <legend style="background:#999" class="text-center headerr">Contact us</legend>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="fname" name="fname" type="text" placeholder="First Name" label="true" class="form-control tab_form required lettersonly">
                                <span id = "fnameError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control tab_form required lettersonly">
                               <span id = "lnameError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control tab_form required email">
                              <span id = "emailError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="mobile" name="mobile" type="text" placeholder="Phone" class="form-control tab_form required number">
                              <span id = "mobileError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <textarea class="form-control tab_form required" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
								<span id = "messageError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12 text-center">
                                <button style="margin-bottom:47px;" type="submit"  onclick ="validate_enquiry();" class="btn btn-primary btn-lg">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div>
                        <div class="panel panel-default header_pannel">
                          <div class="text-center header_pad">Our Office</div>
                          <div class="panel-body text-center">
                            <h4>Address</h4>
                            <div><?php echo $res['address1']?>, <?php echo $res['address2']?><br />
                             <?php echo $res['area']?>, <?php echo $city = getcityname($res['city']);?><br />
							 <?php echo $state =  getstatename($res['state']); ?>- <?php echo $res['pincode'] ?>, <?php //echo $country =  getcountryname(100); ?> India <br />
							 </div>
                            <hr />
                            <div id="map1" class="map">
							<p class="img-thumbnail img-responsive" >
							<?php echo $res['mapembeddedcode']?>
							</p>
						</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="tab_default_4"> 
                  
                  <div class="Feed_back">
                  <div class="row">
                    <div class="col-md-6">
                      <div style="background-color: #201300;"class="well well-sm">
					  <span id = "feedback-success" style = "display:none;"></span>
                        <form class="form-horizontal" method="post" id = "astrofeedback" onsubmit = "return false;">
						<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "FEEDBACK">
                         <input name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>> 
						  <fieldset>
                            <legend class="text-center headerr">Feed back</legend>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="fname" name="fname" type="text" placeholder="First Name" class="form-control tab_form required lettersonly">
                              <span id = "fname-error" style = "display:none;"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control tab_form required lettersonly">
                               <span id = "lname-error" style = "display:none;"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="feed_email" name="email" type="text" placeholder="Email Address" class="form-control tab_form required email">
                               <span id = "feed-email-error" style = "display:none;"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="feed_mobile" name="mobile" type="text" placeholder="Phone" class="form-control tab_form required number">
                              <span id = "feed-mobile-error" style = "display:none;"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <textarea class="form-control tab_form required" id="feed_message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                              <span id = "feed-message-error" style = "display:none;"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12 text-center">
                                <button type="submit" onclick = "validate_feedback()"  class="btn btn-primary btn-lg">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                    </div>
                  </div>
                  
                  
                  
                  
                  <!--comment-->
                  <div class="row" style = "display : none;">
                    <div class="col-md-8"> <br>
                      <section class="comment-list"> 
                        <!-- First Comment -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow left">
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Second Comment Reply -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-9 col-sm-9">
                            <div class="panel panel-default arrow left">
                              <div class="panel-heading right">Reply</div>
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Third Comment -->
                        <article class="row">
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow right">
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                        <!-- Fourth Comment -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            <div class="panel panel-default arrow left">
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Fifth Comment -->
                        <article class="row">
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow right">
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                        <!-- Sixth Comment Reply -->
                        <article class="row">
                          <div class="col-md-9 col-sm-9 col-md-offset-1 col-md-pull-1 col-sm-offset-0">
                            <div class="panel panel-default arrow right">
                              <div class="panel-heading">Reply</div>
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-md-pull-1 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                      </section>
                    </div>
                  </div>
                  
                  <!--comment--> 
                  
                </div>
                <div class="tab-pane" id="tab_default_6">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
                <div class="tab-pane" id="tab_default_7">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
                <div class="tab-pane" id="tab_default_8">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--more Astrologer-->
  <div class="more_astrologer" style = "display:none;">
    <div class="container">
      <div class="profile-body margin-bottom-20"> 
        <!--Profile Blog-->
        <div class="row margin-bottom-20">
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Mikel Andrews</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">12 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">54 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Natasha Kolnikova</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">37 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">46 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Natasha Kolnikova</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">37 Notifications&nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">46 Followers&nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share&nbsp;</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--/end row--> 
        <!--End Profile Blog--> 
        
        <!--Profile Blog-->
        <div class="row margin-bottom-20">
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Sasha Elli</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">3 Notifications&nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">25 Followers&nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share&nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Frank Heller</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">7 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">77 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Frank Heller</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">7 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">77 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--/end row--> 
        <!--End Profile Blog-->
        <button type="button" class="btn-u btn-u-default btn-block text-center">Load More</button>
        <!--End Profile Blog--> 
      </div>
    </div>
  </div>
  <!--more Astrologer--> 
  <div id="searchmap" class="modal fade in" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content row">
				<div class="modal-header custom-modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title"><?php echo ucwords($name_array)?></h4>
					<h6><?php echo $address_array?></h6>
				</div>
				<div class="modal-body">
					<?php echo $map_array?>
				</div>
				
			</div>
			
		</div>
	</div>
  <!--footer section start-->
  <div class="clearfix"></div>
  <div class="footer-v1">
    <div class="footer">
      <div class="container">
        <div class="harimg"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>imges/astrologer2.png" alt=""></div>
        <div class="row"> 
          <!-- About -->
          <div class="col-md-3 media_scr"> <a href="<?php echo LINK_URL_HOME ?>"><img id="logo-footer" class="footer-logo" src="<?php echo LINK_URL_HOME ?>imges/footer_logo.png" alt=""></a>
            <p>Religious Guardian Services Private Limited is a knowledge centric organization, hosting one of its kinds of website "rgyan.com", we are providing premium online information services in field of Religious, Ritual, Spirituality, Festivals & Cultural Activities, Astrologers, Guru, Pandit and related information’s in line. Our motto is for spreading knowledge that is useful to everyone.</p>
          </div>
          <!--/col-md-3--> 
          <!-- End About -->
          <div class="col-md-1 media_scr1 divider_off "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Service List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Quick Links</h2>
            </div>
            <div class="link_font">
              <ul class="list-unstyled link-list">
                <li><a href="about-us.php">-&nbsp;About Us</a></li>
                <li><a href="terms-and-conditions.php">-&nbsp;Terms & Conditions</a></li>
                <li><a href="privacy-policy.php">-&nbsp;Privacy Policy</a></li>
                <li><a href="we-are-hiring.php">-&nbsp;We are hiring</a></li>
                <li><a href="disclaimer.php">-&nbsp;Disclaimer</a></li>
              </ul>
            </div>
          </div>
          <!--/col-md-3--> 
          <!-- End Link List -->
          <div class="col-md-1 media_scr1 divider_off"><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Services List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Services</h2>
            </div>
            <ul class="list-unstyled link-list">
              <li><a href="astrologer.php">-&nbsp;Astrologer</a></li>
              <li><a href="pandit.php">-&nbsp;Pandit</a></li>
              <li><a href="https://rgyan.com/horoscope/">-&nbsp;Online Horoscope</a></li>
             <!-- <li><a href="#">-&nbsp;Vedic Mantras</a></li>-->
            </ul>
          </div>
          <!--/col-md-3--> 
          <!-- End Services Link List -->
          <div class="col-md-1 divider_off media_scr1 "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Address -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Contact Us</h2>
            </div>
            <address class="md-margin-bottom-40">
            Religious Guardian Services Private Limited.<br />
            Plot No. 400P,(Basement) Sector 38,Gurugram(HR) 122001.  <br>
            Near Medanta Medicity <br>
            +91- 124 6588802 <br>
            <a href="#" class="">support@rgyan.com</a>
            </address>
          </div>
          <!--/col-md-3--> 
          <!-- End Address --> 
        </div>
      </div>
    </div>
    <!--/footer-->
    <!--/footer-->
    
    <div class="copyright">
      <div class="container">
        <div class="harimg1"><img class="img-responsive" src="imges/astrologer2.png" alt=""></div>
        <div class="row">
          <div class="col-md-6">
            <p style="color:#ff9900"> Copyright © <?php echo date('Y')?>, Rgyan. All Rights Reserved. 
              <!--<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>--> 
            </p>
          </div>
          
          <!-- Social Links -->
          <div class="col-md-6">
            <ul class="footer-socials list-inline">
              <li><a href="https://www.facebook.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
              <li><a href="https://twitter.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
              <li><a href="https://plus.google.com/107364248809228833539/posts"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
              <li><a href="https://in.pinterest.com/rgyan0288/"><span class="socilsize"><span style="color:#FFF" class="fa fa-pinterest"></span></span></a></li>
              <li><a href="https://www.youtube.com/channel/UC6ttpPdbioJ2I_me6lJe4bg"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></span></a></li>
            </ul>
          </div>
          <!-- End Social Links --> 
        </div>
      </div>
    </div>
    <!--/copyright--> 
  </div>
</div>

<!--end of profile secion--> 

 <div id="enquirypopup" class="modal fade in" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content row">
				<div class="modal-header custom-modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Book an Appointment</h4>
				</div>
				<span style = "color:green;" id = "success"></span>
				<div class="modal-body">
					<form name="info_form" id="info_form" class="form-inline" action="" onsubmit="return false" method="post">
					<input type="hidden" class="form-control" name="astrologer_id" id="astrologer_id" placeholder="Enter Name" value="<?php echo $res['id'] ?>">
						<div class="form-group col-sm-12 col-xs-12">
							<input type="text" class="form-control required lettersonly" name="name" id="book_name" placeholder="Enter Name" onKeyPress="$('#error_name').fadeOut();">
						<span class = "error" id="error_name" style="display:none;"></span>
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<input type="text" class="form-control required email" name="email" id="book_email" placeholder="Enter Email" onKeyPress="$('#error_email').fadeOut();">
							<span class = "error" id="error_email" style="display:none;"></span>	
						</div>
                        
						<div class="form-group col-sm-12 col-xs-12">
							<input type="text" class="form-control required number" name="mobile" id="book_mobile" placeholder="Enter Phone" onKeyPress="$('#error_mobile').fadeOut();">
							<span  class = "error" id="error_mobile" style="display:none;"></span>
						</div>
						
						<div class="form-group col-sm-12 col-xs-12">
							<textarea class="form-control required" id="book_message" name="message" rows="4" placeholder="Enter Message"></textarea>
						</div>
                        
                       <div class="form-group col-sm-12 ">
					   <input type = "hidden" name="formflag" id="formflag" value="BOOKAPPOINTMENT" />
							<button type="submit" onclick = "validate_form();"  class="btn btn-default pull-right">Submit</button>
						</div>
					</form>
				</div>
				
			</div>
			
		</div>
	</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery.barrating.js"></script>
<script type="text/javascript">
        $(function () {
           
                $('#example-a').barrating();

                $('#example-b').barrating('show', {
                    readonly:true
                });

                $('#example-c, #example-d').barrating('show', {
                    showValues:true,
                    showSelectedRating:false, onSelect:function(value, text) {
                        if(value==1)
						{
						document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#039'>Very Bad</font></strong>";
						}
						else{
								if(value==2)
								{
								document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#039'>Average</font></strong>";	
								}
								else{
								
										if(value==3)
										{
										document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#039'>Good</font></strong>";
										}
										else{
												if(value==4)
												{
													
												document.getElementById('rateresult').innerHTML="You are Rating this as  <strong><font color='#039'>Great</font></strong>";
												}
												else{
														if(value==5)
														{
															document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#039'>Excellent</font></strong>";	
														}
														else{
														
														
														}
												}
										
										}
								
								}
						
						}
                    }
                });

                $('#example-e').barrating('show', {
                    initialRating:'A',                    
                    showValues:true,
                    showSelectedRating:false,
                    onSelect:function(value, text) {
                        alert('Selected rating: ' + value);
                    }
                });

               $('#example-f').barrating('show', {showSelectedRating:false});

                $('#example-g').barrating('show', {
                    showSelectedRating:true,
                    reverse:true
                });

  });
 </script> 
<script> 
function validate_form(){
var name = document.getElementById('book_name').value;
var email = document.getElementById('book_email').value;
var mobile = document.getElementById('book_mobile').value;
var letters = "/^[A-Za-z ]+$/";
var numbers = "/^[0-9]{10}+$/";
var emailcheck = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
var flag = 0;
if(name == ""){	
document.getElementById('error_name').innerHTML = "Please enter your name.";
document.getElementById('error_name').style.display="block";
}
else if(/^[A-Za-z ]+$/.test(name)){
flag = 1;
document.getElementById('error_name').style.display = "none";

}
else{
flag = 0;
document.getElementById('error_name').innerHTML = "Please enter only alphabate and space.";
document.getElementById('error_name').style.display= "block";	
}

if(email == ""){
flag = 0;
document.getElementById('error_email').innerHTML = "Please enter email id.";
document.getElementById('error_email').style.display= "block";
}
else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
flag = 1;
document.getElementById('error_email').style.display= "none";	
}
else{
flag = 0;
document.getElementById('error_email').innerHTML = "Please enter vaild email id.";
document.getElementById('error_email').style.display= "block";
}

if(mobile == ""){
flag = 0;
document.getElementById('error_mobile').innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('error_mobile').style.display= "block";	
}
else if(/^\d{10}$/.test(mobile)){
flag = 1;
document.getElementById('error_mobile').style.display= "none";	
}
else{
flag = 0;
document.getElementById('error_mobile').innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('error_mobile').style.display= "block";
}
if(flag === 1){
dataString = $("#info_form").serialize();
$.post("process.php", dataString, function(data) {
var jdata= JSON.parse(data);
document.getElementById('book_name').value = "";
document.getElementById('book_email').value = "";
document.getElementById('book_mobile').value = "";
document.getElementById('book_message').value = "";
document.getElementById('success').innerHTML = jdata['success'];

return false;	
});	
}else{
return false;
}
}
</script>
<script type = "text/javascript">
function validate_enquiry(){
$.post("process.php", $("#contactus").serialize(), function(data) {
document.getElementById('contactussuccess').innerHTML = data;
document.getElementById('contactussuccess').style.display = "block";
 });	
}

function validate_feedback(){
$.post("process.php", $("#astrofeedback").serialize(), function(data) {
document.getElementById('feedback-success').innerHTML = data;
document.getElementById('feedback-success').style.display = "block";	
    });	
}

function validate_review(){
var name = document.getElementById('rate_name').value;
var email = document.getElementById('rate_email').value;
var mobile = document.getElementById('rate_mobile').value;
var letters = "/^[A-Za-z ]+$/";
var numbers = "/^[0-9]{10}+$/";
var emailcheck = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
var flag = 0;
if(name == ""){	
document.getElementById('rate_name-error').innerHTML = "Please enter your name.";
document.getElementById('rate_name-error').style.display="block";
}
else if(/^[A-Za-z ]+$/.test(name)){
flag = 1;
document.getElementById('rate_name-error').style.display = "none";

}
else{
flag = 0;
document.getElementById('rate_name-error').innerHTML = "Please enter only alphabate and space.";
document.getElementById('rate_name-error').style.display= "block";	
}

if(email == ""){
flag = 0;
document.getElementById('rate_email-error').innerHTML = "Please enter email id.";
document.getElementById('rate_email-error').style.display= "block";
}
else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
flag = 1;
document.getElementById('rate_email-error').style.display= "none";	
}
else{
flag = 0;
document.getElementById('rate_email-error').innerHTML = "Please enter vaild email id.";
document.getElementById('rate_email-error').style.display= "block";
}

if(mobile == ""){
flag = 0;
document.getElementById('rate_mobile-error').innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('rate_mobile-error').style.display= "block";	
}
else if(/^\d{10}$/.test(mobile)){
flag = 1;
document.getElementById('rate_mobile-error').style.display= "none";	
}
else{
flag = 0;
document.getElementById('rate_mobile-error').innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('rate_mobile-error').style.display= "block";
}

if(flag === 1){
return true;
}
else{
return false;	
}
}
</script>
<script type="text/javascript" src="<?php echo LINK_URL_ADMIN ?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "Letters only please"); 
$(document).ready(function(){ 
$("#contactus").validate({ });
//$("#reviewform").validate({ });
$("#astrofeedback").validate({ });
});
</script>
<script type="text/javascript" src="<?php echo LINK_URL_HOME ?>js/jquery.autocomplete.pack.js"></script>
<script type="text/javascript" src="<?php echo LINK_URL_HOME ?>js/script.js"></script>
<script type="text/javascript">
// Change Your home URL..
home_url = 'http://rgyan.com/astrologer/admin';

/* *
*     fileName - ajax file name to be called by ajax method.
*     data - pass the infromation(like location-id , location-type) via data variable.
*     loadDataToDiv - id of the div to which the ajax responce is to be loaded.
* */
function ajax_call(fileName,data, loadDataToDiv) {
jQuery("#"+loadDataToDiv).html('<option selected="selected">-- -- -- Loding Data -- -- --</option>');

//  If you are changing counrty, make the state and city fields blank
if(loadDataToDiv=='state'){
jQuery('#city').html('');
jQuery('#state').html('');                    
}
//  If you are changing state, make the city fields blank
if(loadDataToDiv=='city'){
jQuery('#city').html('');
}

jQuery.post(home_url + '/' + fileName + '.php', data, function(result) {
jQuery('#' + loadDataToDiv).html(result);
});
}
</script>
</body>
</html>