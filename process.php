<?php
$error = array();
$success = array();
include_once('admin/includes/function.php');
date_default_timezone_set('Asia/Kolkata'); 
/* Astrologer Contact us form process start*/
if(isset($_POST['formflag']) && $_POST['formflag']=='CONTACTUS'){
if(isset($_POST['fname'])){
if(empty($_POST['fname'])){
$error['fname'] = "Please enter first name";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['fname']))){
$error['fname'] = "Enter only alphabates.";	
}
else if(strlen($_POST['fname']) < 3){
$error['fname'] = "First name should be minimum 3 chareacters.";	
}else{
$fname = trim($_POST['fname']);	
}
}

if(isset($_POST['lname'])){
if(empty($_POST['lname'])){
$error['lname'] = "Please enter last name.";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['lname']))){ 
$error['lname'] = "Enter only alphabates.";	
}else if(strlen($_POST['lname']) < 3){
$error['lname'] = "Last name should be minimum 3 chareacters.";	
}else{
$lname = trim($_POST['lname']);	
}	
}

if(isset($_POST['email'])){
if(empty($_POST['email'])){
$error['email'] = "Please enter email address.";	
}
else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === true){
$error['email'] = "Please enter valid email address.";	
}
else{
$email	= $_POST['email'];
}	
}

if(isset($_POST['mobile'])){
if(empty($_POST['mobile'])){
$error['mobile'] = "Please enter 10-digits mobile number.";
}
else if(!ctype_digit($_POST['mobile'])){
$error['mobile'] = "Please enter digits only.";	
}
else if(strlen($_POST['mobile']) < 10 ){
$error['mobile'] = "Please enter 10-digits mobile number.";	
}
else{
$mobile = $_POST['mobile'];	
}	
}

if(isset($_POST['message'])){
if(empty($_POST['message'])){
$error['message'] = "Please enter message.";	
}
else{
$message = $_POST['message'];	
}	
}
$astrologer_id = $_POST['astrologer_id'];
$date = date('y-m-d');
$name = $fname."&nbsp;".$lname;
if(empty($error)){
$insertcontquery = $db->prepare("INSERT INTO astrologerenquiry(astrologer_id, name, email, mobile, message, date) VALUE(:astrologerid, :name, :email, :mobile, :message, :date)");
$insertcontquery->bindParam(':astrologerid', $astrologer_id);
$insertcontquery->bindParam(':name', $name);
$insertcontquery->bindParam(':email', $email);
$insertcontquery->bindParam(':mobile', $mobile);	
$insertcontquery->bindParam(':message', $message);
$insertcontquery->bindParam(':date', $date);
$insertcontquery->execute();
$flag = $db->LastInsertId();
if($flag){
$AstroEmailQuery = $db->prepare("SELECT concat(fname, lname) AS name, email FROM astrologers WHERE id = ".$astrologer_id."");
$AstroEmailQuery->execute();
$emailRes = $AstroEmailQuery->fetch();	
//print_r($emailRes); exit;
if(!empty($emailRes['email'])){	
$to=$emailRes['email'];
}
else{
$to="support@rgyan.com";	
}
//$to="trisha@amicusinfotech.com";
$subject="Enquiry for Astro Services";
$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$emailRes['name'].', <br />
 Find the customer detail which is interested in you.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';

$subjectclient="Enquiry for Astro Services";
$bodyclient='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$name.', <br />
 Thanks for choosing Us. You have done enquiry of '.$emailRes['name'].' for astro service. <br />
 Your dtail is given below. Please check.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';
$headers = "From:" .$email. "\r\n";
$headers.= "Cc:prabhat@rgyan.com \r\n";
$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headersclient = "From:" .$to. "\r\n";
$headersclient.= "Cc:prabhat@rgyan.com \r\n";
$headersclient .= "Reply-To: ". strip_tags($to) . "\r\n";
$headersclient .= "MIME-Version: 1.0\r\n";
$headersclient .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $body, $headers);
mail($email, $subjectclient, $bodyclient, $headersclient);	
$success = "Your enquiry has been sent successfully.";	
echo $success;
}
}
else{
print_r(json_encode($error));
}
}
/* Astrologer Feedback form process start*/
else if(isset($_POST['formflag']) && $_POST['formflag']=='FEEDBACK'){
if(isset($_POST['fname'])){
if(empty($_POST['fname'])){
$error['fname'] = "Please enter first name";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['fname']))){
$error['fname'] = "Enter only alphabates.";	
}
else if(strlen($_POST['fname']) < 3){
$error['fname'] = "First name should be minimum 3 chareacters.";	
}else{
$fname = trim($_POST['fname']);	
}
}

if(isset($_POST['lname'])){
if(empty($_POST['lname'])){
$error['lname'] = "Please enter last name.";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['lname']))){ 
$error['lname'] = "Enter only alphabates.";	
}else if(strlen($_POST['lname']) < 3){
$error['lname'] = "Last name should be minimum 3 chareacters.";	
}else{
$lname = trim($_POST['lname']);	
}	
}

if(isset($_POST['email'])){
if(empty($_POST['email'])){
$error['email'] = "Please enter email address.";	
}
else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === true){
$error['email'] = "Please enter valid email address.";	
}
else{
$email	= $_POST['email'];
}	
}

if(isset($_POST['mobile'])){
if(empty($_POST['mobile'])){
$error['mobile'] = "Please enter 10-digits mobile number.";
}
else if(!ctype_digit($_POST['mobile'])){
$error['mobile'] = "Please enter digits only.";	
}
else if(strlen($_POST['mobile']) < 10 ){
$error['mobile'] = "Please enter 10-digits mobile number.";	
}
else{
$mobile = $_POST['mobile'];	
}	
}

if(isset($_POST['message'])){
if(empty($_POST['message'])){
$error['message'] = "Please enter message.";	
}
else{
$message = $_POST['message'];	
}	
}
$astrologer_id = $_POST['astrologer_id'];
$date = date('y-m-d');
$name = $fname.'&nbsp;'.$lname;
if(empty($error)){
$insertcontquery = $db->prepare("INSERT INTO astrologerfeedback(astrologer_id, name, email, mobile, message, date) VALUE(:astrologerid, :name, :email, :mobile, :message, :date)");
$insertcontquery->bindParam(':astrologerid', $astrologer_id);
$insertcontquery->bindParam(':name', $name);
$insertcontquery->bindParam(':email', $email);
$insertcontquery->bindParam(':mobile', $mobile);	
$insertcontquery->bindParam(':message', $message);
$insertcontquery->bindParam(':date', $date);
$insertcontquery->execute();
$flag = $db->LastInsertId();
if($flag){
$AstroEmailQuery = $db->prepare("SELECT concat(fname, lname) AS name, email FROM astrologers WHERE id = ".$astrologer_id."");
$AstroEmailQuery->execute();
$emailRes = $AstroEmailQuery->fetch();	
if(!empty($emailRes['email'])){	
$to=$emailRes['email'];
}
else{
$to="support@rgyan.com";	
}
$subject="Feedback for Astro Services";
$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$emailRes['name'].', <br />
 Find the customer detail which is interested in you.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';

$subjectclient="Enquiry for Astro Services";
$bodyclient='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$name.', <br />
 Thanks for choosing Us. You have done enquiry of '.$emailRes['name'].' for astro service. <br />
 Your dtail is given below. Please check.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';
$headers = "From:" .$email. "\r\n";
$headers.= "Cc:prabhat@rgyan.com \r\n";
$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headersclient = "From:" .$to. "\r\n";
$headersclient.= "Cc:prabhat@rgyan.com \r\n";
$headersclient .= "Reply-To: ". strip_tags($to) . "\r\n";
$headersclient .= "MIME-Version: 1.0\r\n";
$headersclient .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $body, $headers);
mail($email, $subjectclient, $bodyclient, $headersclient);	
$success = "Your enquiry has been sent successfully.";	
echo $success;

}
}
else{
print_r(json_encode($error));
}	
}
/* Astrologer Review form process start*/
else if(isset($_POST['formflag']) && $_POST['formflag']=='REVIEW'){
//print_r()
//print_r($_POST); exit;
if(isset($_POST['name'])){
if(empty($_POST['name'])){
$error['name'] = "Please enter first name";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['name']))){
$error['name'] = "Enter only alphabates.";	
}
else if(strlen($_POST['name']) < 3){
$error['name'] = "Name should be minimum 3 chareacters.";	
}else{
$fname = trim($_POST['name']);	
}
}

if(isset($_POST['email'])){
if(empty($_POST['email'])){
$error['email'] = "Please enter email address.";	
}
else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === true){
$error['email'] = "Please enter valid email address.";	
}
else{
$email	= $_POST['email'];
}	
}

if(isset($_POST['mobile'])){
if(empty($_POST['mobile'])){
$error['mobile'] = "Please enter 10-digits mobile number.";
}
else if(!ctype_digit($_POST['mobile'])){
$error['mobile'] = "Please enter digits only.";	
}
else if(strlen($_POST['mobile']) < 10 ){
$error['mobile'] = "Please enter 10-digits mobile number.";	
}
else{
$mobile = $_POST['mobile'];	
}	
}

if(isset($_POST['description'])){
if(empty($_POST['description'])){
$error['message'] = "Please enter message.";	
}
else{
$message = $_POST['description'];	
}	
}
$astrologer_id = $_POST['rid'];
$date = date('y-m-d');
$name = $_POST['name'];
$rate = $_POST['rating'];
$path = 'upload/review/';
if(empty($_FILES['file_upload']['name'])){
$message = "Please Upload Profile Picture."; 
$filename = "astro.png";
}
else{
$fileext = explode('.',$_FILES['file_upload']['name']); 
if(!in_array($_FILES['file_upload']['type'],array('image/jpg','image/jpeg','image/png'))){
$message = "Please upload .";
$filename = "astro.png";
}
else
{
$filename = uniqid().'_'.time().'_'.$_POST['name'].'.'.  strtolower(end($fileext));
$dest = $path.$filename;
move_uploaded_file($_FILES['file_upload']['tmp_name'], $dest);
}
}        
	
if(empty($error)){
$insertcontquery = $db->prepare("INSERT INTO astrologerrating(astrologer_id, name, pic, email, mobile, message, rate, date) VALUE(:astrologerid, :name, :pic, :email, :mobile, :message, :rate, :date)");
$insertcontquery->bindParam(':astrologerid', $astrologer_id);
$insertcontquery->bindParam(':name', $name);
$insertcontquery->bindParam(':pic', $filename);
$insertcontquery->bindParam(':email', $email);
$insertcontquery->bindParam(':mobile', $mobile);	
$insertcontquery->bindParam(':message', $message);
$insertcontquery->bindParam(':rate', $rate);
$insertcontquery->bindParam(':date', $date);
$insertcontquery->execute();
$flag = $db->LastInsertId();
if($flag){
$AstroEmailQuery = $db->prepare("SELECT concat(fname, lname) AS name, email FROM astrologers WHERE id = ".$astrologer_id."");
$AstroEmailQuery->execute();
$emailRes = $AstroEmailQuery->fetch();	
if(!empty($emailRes['email'])){	
$to=$emailRes['email'];
}
else{
$to="support@rgyan.com";	
}
$subject="Rating for Astro Services";
$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Rating for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$emailRes['name'].', <br />
 Find the customer detail which is interested in you.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
  <tr>
    <td>Rated</td>
    <td>:</td>
    <td>'.$rate.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';

$subjectclient="Enquiry for Astro Services";
$bodyclient='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Rating for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$name.', <br />
 Thanks for choosing Us. You have rated me for astro service. <br />
 Your dtail is given below. Please check.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Rated</td>
    <td>:</td>
    <td>'.$rate.'</td>
  </tr>
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';
$headers = "From:" .$email. "\r\n";
$headers.= "Cc:prabhat@rgyan.com \r\n";
$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headersclient = "From:" .$to. "\r\n";
$headersclient.= "Cc:prabhat@rgyan.com \r\n";
$headersclient .= "Reply-To: ". strip_tags($to) . "\r\n";
$headersclient .= "MIME-Version: 1.0\r\n";
$headersclient .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $body, $headers);
mail($email, $subjectclient, $bodyclient, $headersclient);
$success['success'] = "Your rating has been sent successfully.";	
//print_r(json_encode($success));	
header('location:detail.php?id='.$astrologer_id.'&success=1');
}
}
else{
$encodeerror = json_encode($error);
//print_r($encodeerror);
header('location:detail.php?id='.$astrologer_id.'&error='.$encodeerror);
}	
	
}
/* Astrologer Book Appointment form process start*/
else if(isset($_POST['formflag']) && $_POST['formflag']=='BOOKAPPOINTMENT'){
if(isset($_POST['name'])){
if(empty($_POST['name'])){
$error['name'] = "Please enter name";	
}	
else if(!ctype_alpha(str_replace(' ', '', $_POST['name']))){
$error['name'] = "Enter only alphabates.";	
}
else if(strlen($_POST['name']) < 3){
$error['name'] = "First name should be minimum 3 chareacters.";	
}else{
$name = trim($_POST['name']);	
}
}

if(isset($_POST['email'])){
if(empty($_POST['email'])){
$error['email'] = "Please enter email address.";	
}
else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === true){
$error['email'] = "Please enter valid email address.";	
}
else{
$email	= $_POST['email'];
}	
}

if(isset($_POST['mobile'])){
if(empty($_POST['mobile'])){
$error['mobile'] = "Please enter 10-digits mobile number.";
}
else if(!ctype_digit($_POST['mobile'])){
$error['mobile'] = "Please enter digits only.";	
}
else if(strlen($_POST['mobile']) < 10 ){
$error['mobile'] = "Please enter 10-digits mobile number.";	
}
else{
$mobile = $_POST['mobile'];	
}	
}

if(isset($_POST['message'])){
if(empty($_POST['message'])){
$error['message'] = "Please enter message.";	
}
else{
$message = $_POST['message'];	
}	
}
$astrologer_id = $_POST['astrologer_id'];
$date = date('y-m-d');
if(empty($error)){
$insertcontquery = $db->prepare("INSERT INTO astrologerenquiry(astrologer_id, name, email, mobile, message, date) VALUE(:astrologerid, :name, :email, :mobile, :message, :date)");
$insertcontquery->bindParam(':astrologerid', $astrologer_id);
$insertcontquery->bindParam(':name', $name);
$insertcontquery->bindParam(':email', $email);
$insertcontquery->bindParam(':mobile', $mobile);	
$insertcontquery->bindParam(':message', $message);
$insertcontquery->bindParam(':date', $date);
$insertcontquery->execute();
$flag = $db->LastInsertId();
if($flag){
$AstroEmailQuery = $db->prepare("SELECT concat(fname, lname) AS name, email FROM astrologers WHERE id = ".$astrologer_id."");
$AstroEmailQuery->execute();
$emailRes = $AstroEmailQuery->fetch();	
//print_r($emailRes); exit;
if(!empty($emailRes['email'])){	
$to=$emailRes['email'];
}
else{
$to="support@rgyan.com";	
}
//$to="trisha@amicusinfotech.com";
$subject="Enquiry for Astro Services";
$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$emailRes['name'].', <br />
 Find the customer detail which is interested in you.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';

$subjectclient="Enquiry for Astro Services";
$bodyclient='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enquiry for Astro Services.</title>
</head>
 <body>
 <p>
 Dear '.$name.', <br />
 Thanks for choosing Us. You have done enquiry of '.$emailRes['name'].' for astro service. <br />
 Your dtail is given below. Please check.
 </p>
<table width="65%" border="1">
  <tr>
    <td>Name</td>
    <td>:</td>
    <td>'.$name.'</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td>'.$email.'</td>
  </tr>
   <tr>
    <td>Mobile</td>
    <td>:</td>
    <td>'.$mobile.'</td>
  </tr>
  <tr>
    <td>Message</td>
    <td>:</td>
    <td>'.$message.'</td>
  </tr>
 
 <tr>
    <td>Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  </table>
  <p>
  Thanks & Rgards<br />
  Team Rgyan
  </p>
</body>
</html>';
$headers = "From:" .$email. "\r\n";
$headers.= "Cc:prabhat@rgyan.com \r\n";
$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headersclient = "From:" .$to. "\r\n";
$headersclient.= "Cc:prabhat@rgyan.com \r\n";
$headersclient .= "Reply-To: ". strip_tags($to) . "\r\n";
$headersclient .= "MIME-Version: 1.0\r\n";
$headersclient .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $body, $headers);
mail($email, $subjectclient, $bodyclient, $headersclient);	
$success['success'] = "Your enquiry has been sent successfully.";	
print_r(json_encode($success));
}
}
else{
$success['success']="Security issue has occurred.";
print_r(json_encode($error));
}	
}
?>