<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <!--  Start Header Section-->
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Thank You</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Thank You</li>
      </ul>
    </div>
  </div>
  <div class="container content __web-inspector-hide-shortcut__">
			<div class="row">
				<div class="col-md-12">
					<center><h1 style = "color:green;">Thank You!</h1></center>
                        <p class="text-justify text-center" style="color:#fff;font-size:18px" >Your submission is received and we will contact you soon.</p>
				</div>
               
               
				
      
			</div><!--/row-fluid-->
		</div>
  
    <!--about us section-->
  
 
  
  <!--footer section start-->
  <div class="clearfix"></div>
  <?php include_once('pages/footer.php'); ?>