<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?=$headerTitle;?></title>
    <link rel="shortcut icon" href="">
    <link href="<?php echo SITE_PATH; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>css/sb-admin.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>css/style-ui.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800' rel='stylesheet' type='text/css'>
</head>

<body style="background-color: #fff;">

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                        
						<a class="navbar-brand" href="http://rgyan.com/"><span class="logo"><img src="http://rgyan.com/horoscope/logo.png" style="height:55px; width:70px;"></span></a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-nav navbar-right top-nav">
            <li class="dropdown">
                <a href="<?php echo SITE_PATH; ?>index.php">Back To Form</a>
            </li>

            <!--Place your links-->

            <!--<li class="dropdown">
                <a href=""></a>
            </li>
            <li class="dropdown">
                <a href=""></a>
            </li>
            <li class="dropdown">
                <a href="" class="nav-btn"></a>
            </li>-->

        </ul>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav horoscope-nave">
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'index.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>horoscope/index.php">Daily Prediction</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'basic_details.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>horoscope/basic_details.php"> Basic Details</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'horo_chart.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>horoscope/horo_chart.php"> Horoscope Charts</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'planets.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>horoscope/planets.php"> Planetary Details</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#dasha"> Dasha </a>
                    <ul id="dasha" class="collapse">
                        <li >
                            <a href="<?php echo SITE_PATH; ?>horoscope/vim_vdasha.php" <?php if (strpos($_SERVER['PHP_SELF'], 'vim_vdasha.php')) echo 'class="active"';?>>Vimshottari Dasha</a>
                        </li>
                        <li>
                            <a  href="<?php echo SITE_PATH; ?>horoscope/yogini_dasha.php" <?php if (strpos($_SERVER['PHP_SELF'], 'yogini_dasha.php')) echo 'class="active"';?>>Yogini Dasha</a>
                        </li>
                        <li <?php if (strpos($_SERVER['PHP_SELF'], 'char_dasha.php')) echo 'class="active"';?>>
                            <a  href="<?php echo SITE_PATH; ?>horoscope/char_dasha.php">Char Dasha</a>
                        </li>
                    </ul>
                </li>
                <li >
                    <a href="javascript:;" data-toggle="collapse" data-target="#numerology"> Numerology Details </a>
                    <ul id="numerology" class="collapse">
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/numero_table.php" <?php if (strpos($_SERVER['PHP_SELF'], 'numero_table.php')) echo 'class="active"';?>>Numerology Table</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/numero_report.php" <?php if (strpos($_SERVER['PHP_SELF'], 'numero_report.php')) echo 'class="active"';?>>Numerology Report</a>
                        </li>
                    </ul>
                </li>
                <li >
                    <a href="javascript:;" data-toggle="collapse" data-target="#dosha"> Dosha Reports </a>
                    <ul id="dosha" class="collapse">
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/kalsarpa_details.php" <?php if (strpos($_SERVER['PHP_SELF'], 'kalsarpa_details.php')) echo 'class="active"';?>>Kalsarpa Report</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/manglik.php" <?php if (strpos($_SERVER['PHP_SELF'], 'manglik.php')) echo 'class="active"';?>>Manglik Report </a>
                        </li>
                        <li>
                            <a  href="<?php echo SITE_PATH; ?>horoscope/sadhesati_current_status.php"  <?php if (strpos($_SERVER['PHP_SELF'], 'sadhesati_current_status.php')) echo 'class="active"';?>>Sadhesati Report</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/pitra_dosha_report.php" <?php if (strpos($_SERVER['PHP_SELF'], 'pitra_dosha_report.php')) echo 'class="active"';?>>Pitra Dosha Report</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#remedies"> Remedies </a>
                    <ul id="remedies" class="collapse">
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/basic_gem_suggestion.php" <?php if (strpos($_SERVER['PHP_SELF'], 'basic_gem_suggestion.php')) echo 'class="active"';?>>Gemstone Suggestion</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/rudraksha_suggestion.php" <?php if (strpos($_SERVER['PHP_SELF'], 'rudraksha_suggestion.php')) echo 'class="active"';?>>Rudraksha Suggestion </a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/sadhesati_remedies.php" <?php if (strpos($_SERVER['PHP_SELF'], 'sadhesati_remedies.php')) echo 'class="active"';?>>Sadhesati Remedies</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#reports">Life Reports </a>
                    <ul id="reports" class="collapse">
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/general_house_report.php" <?php if (strpos($_SERVER['PHP_SELF'], 'general_house_report.php')) echo 'class="active"';?>>Planet House Report</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/general_rashi_report.php" <?php if (strpos($_SERVER['PHP_SELF'], 'general_rashi_report.php')) echo 'class="active"';?>>Planet Sign Report </a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/ascendant_report.php" <?php if (strpos($_SERVER['PHP_SELF'], 'ascendant_report.php')) echo 'class="active"';?>>Ascendant Report</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#ashtakVarga"> Ashtak varga </a>
                    <ul id="ashtakVarga" class="collapse">
                        <li>
                            <a  href="<?php echo SITE_PATH; ?>horoscope/planet_ashtak.php"  <?php if (strpos($_SERVER['PHP_SELF'], 'planet_ashtak.php')) echo 'class="active"';?>>Bhinnashtak Varga</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/sarvashtak.php" <?php if (strpos($_SERVER['PHP_SELF'], 'sarvashtak.php')) echo 'class="active"';?>>Sarvashtak</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#lalkitab"> Lalkitab</a>
                    <ul id="lalkitab" class="collapse">
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/lalkitab_debts.php" <?php if (strpos($_SERVER['PHP_SELF'], 'lalkitab_debts.php')) echo 'class="active"';?>>Lalkitab Debts</a>
                        </li>
                        <li >
                            <a  href="<?php echo SITE_PATH; ?>horoscope/lalkitab_remedies.php" <?php if (strpos($_SERVER['PHP_SELF'], 'lalkitab_remedies.php')) echo 'class="active"';?>>Lalkitab Remedies</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a  href="<?php echo SITE_PATH; ?>horoscope/biorhythm.php"  <?php if (strpos($_SERVER['PHP_SELF'], 'biorhythm.php')) echo 'class="active"';?>> Biorhythm</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">
            <div class="se-pre-con display-none"></div>

            <div class="row">
                <div class="col-lg-12">