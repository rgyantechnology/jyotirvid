<?php
require_once 'SessionData.php';
require_once 'VedicRishiClient.php';

class ApiCall{

     private $userId = "5788";
    private $apiKey = 'dfd595651ada6437b441cf2a94160d41';

    public function horoscopeApiCall($resourceName)
    {
        $data = (SessionData::getHoroscopeRequestData());
        if($data['status'])
        {
            // instantiate VedicRishiClient class
             $vedicRishi = new VedicRishiClient($this->userId, $this->apiKey);

            //call horoscope apis
            $responseData = $vedicRishi->call($resourceName, $data['day'], $data['month'], $data['year'], $data['hour'], $data['minute'], $data['latitude'], $data['longitude'], $data['timezone']);

            //print response data
            return json_decode(($responseData),true);
        }
        else{
            return false;
        }
    }
    public function numerologyApiCall($resourceName)
    {
        $data = (SessionData::getHoroscopeRequestData());

        if($data['status'])
        {
            // instantiate VedicRishiClient class
            $vedicRishi = new VedicRishiClient($this->userId, $this->apiKey);

            //call horoscope apis
            $responseData = $vedicRishi->numeroCall($resourceName, $data['day'], $data['month'], $data['year'],$data['name']);

            //print response data
            return json_decode(($responseData),true);
        }
        else{
            return false;
        }
    }

    public function panchangApiCall($resourceName)
    {
        $data = (SessionData::getPanchangRequestData());

        if($data['status'])
        {
            // instantiate VedicRishiClient class
            $vedicRishi = new VedicRishiClient($this->userId, $this->apiKey);

            //call horoscope apis
            $responseData = $vedicRishi->panchangCall($resourceName, $data['day'], $data['month'], $data['year'],$data['latitude'],$data['longitude'],$data['timezone']);

            //print response data
            return json_decode(($responseData),true);
        }
        else{
            return false;
        }
    }

    public function matchingApiCall($resourceName)
    {
        $data = (SessionData::getMatchingRequestData());

        if($data['status'])
        {
            // instantiate VedicRishiClient class
            $vedicRishi = new VedicRishiClient($this->userId, $this->apiKey);

            //call horoscope apis
            $responseData = $vedicRishi->matchMakingCall($resourceName,$data['male'],$data['female']);

            //print response data
            return json_decode(($responseData),true);
        }
        else{
            return false;
        }
    }



}

$userId = ""; $apiKey = "";