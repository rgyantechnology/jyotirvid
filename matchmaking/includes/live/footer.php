  
    <footer class="footer wow fadeInUp" data-wow-duration="2s">
      <div class="container">
        <div class="footer-inner">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title" style="margin-left:4px;">Address</h3>
                <div class="footer-info"></div>
                <div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
    			  <? $selct="select * from main_menu where status='1' AND id='30'"; 
				  $que=mysql_query($selct);
				  $row=mysql_fetch_array($que);
				  echo $row['short_desc'];?>
                 
                  </address>
                </div>
				<?
$obj_db_admin=new DB();
$obj_db_admin->open();

$sql_admin = "select * from admin where adminID='1'";
$rs_admin = $obj_db_admin->query($sql_admin);
$row_admin = $obj_db_admin->fetchArray($rs_admin);
?>
                <div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us <?=$row_admin['phone'];?></span> </div>

                <div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner">Call us <?=$row_admin['mobile'];?></span> </div>
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:Info@academica.com"><?=$row_admin['adminEmail'];?></a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">USEFUL LINKS</h3>
                <ul class="footer-list list-unstyled">
				<? $select="select * from main_menu where status='1' AND dis_on_footer='1'";
				$que=mysql_query($select); 
				while($row=mysql_fetch_array($que)){?>
                  <li class="footer-list__item"><a class="footer-list__link" href="<?=SITE_PATH?>innerPage.php?pageT=<? echo $row['name']; ?>&page_id=<? echo $row['id'];?>"><? echo $row['name'];?></a></li>
                 <? } ?>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3" style="display:none;">
              <section class="footer-section">
                <h3 class="footer-title">LATEST TWEETS</h3>
                <div class="tweets">
                  <div class="tweets__text">What is the enemy of creativity?</div>
                  
                  <span class="tweets__time">9 hours ago</span> </div>
                <div class="tweets">
                  <div class="tweets__text">An agile framework can produce the type of lean marketing essential for the digital age </div>
                  <span class="tweets__time">9 hours ago</span> </div>
                </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">QUICK CONTACT</h3>
                <form class="form" action="thanks2.php" method="post">
                  <div class="form-group" style="width: 80%">
                    <input name="name" class="form-control" type="text" placeholder="Your Name" required>
                    <input name="mail" class="form-control" type="text" placeholder="Email address" required>
                    <textarea name="msg" class="form-control" rows="7" required placeholder="Message" style="height:110px;"></textarea>
                    <input type="submit" name="submitFromFooterContact" class="btn btn-primary btn-effect" value="SEND MESSSAGE">
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
			
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        <div class="row">
			<div class="col-lg-4 logo-footor">
				<a href="http://rgyan.com/"><img class="footer-logo img-responsive" src="assets/img/logo.png" alt="Logo"></a>
			</div>
		</div>
        <div class="row">
          <div class="col-xs-12 footer-socail">
            <div class="footer-bottom">
              <div class="copyright" style="display:none">Copyright &copy; 2016 <a href="javascript:void(0);">Rgyan</a>,   |  Created by <a href="http://nofikkr.com/">nofikkr</a></div>
              <ul class="social-links list-unstyled">
			  <?
$obj_db_admin=new DB();
$obj_db_admin->open();

$sql_admin = "select * from admin where adminID='1'";
$rs_admin = $obj_db_admin->query($sql_admin);
$row_admin = $obj_db_admin->fetchArray($rs_admin);
?>
			<ul class="footer-socail">
                <li><a class="icon fa fa-facebook" href="<?=$row_admin['facebook']; ?>" target="_blank" style="margin-left:360px;"></a></li>
                <li><a class="icon fa fa-twitter" href="<?=$row_admin['twitter']; ?>" target="_blank"></a></li>
                <li><a class="icon fa fa-google-plus" href="<?=$row_admin['google']; ?>" target="_blank"></a></li>
               
                <li><a class="icon fa fa-linkedin" href="<?=$row_admin['linkedin']; ?>" target="_blank"></a></li>
            </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
  <!-- end wrapper --> 
<!-- end layout-theme --> 

<!-- SCRIPTS --> 
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="assets/js/modernizr.custom.js"></script> 
<script src="assets/js/waypoints.min.js"></script> 
<script src="../../cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!--THEME--> 
<script  src="assets/plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="assets/plugins/isotope/jquery.isotope.min.js"></script> 
<script src="assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="assets/plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="assets/plugins/jelect/jquery.jelect.js"></script> 
<script src="assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="assets/js/cssua.min.js"></script> 
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/custom.min.js"></script>

<!--COLOR SWITCHER --> 
<script src="assets/plugins/switcher/js/bootstrap-select.js"></script> 
<script src="assets/plugins/switcher/js/dmss.js"></script>
</body>


</html>
