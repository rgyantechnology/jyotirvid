<!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <!--<div class="navbar-inverse">
            <form class="navbar-search visible-phone">
               <input type="text" class="search-query" placeholder="Search" />
            </form>
         </div>-->
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
         <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu">
          
            <?
			if($file_name=='dashboard.php' || $file_name==''){
			$tabcls = 'class="sub-menu active"';
			} else {
			$tabcls = 'class="sub-menu"';
			}
			?>
              <li <?=$tabcls?>>
                  <a class="" href="dashboard.php">
                      <i class="icon-dashboard"></i>
                      <span>Dashboard</span>
                  </a>
              </li>
              
              <? if($_SESSION['adminType']=='superadmin'){ ?> <!--// super admin option-->
              <?
                if($file_name=='add_edit_admin.php' || $file_name=='manage_admin.php'){
                $tabcls = 'class="sub-menu active"';
                } else {
                $tabcls = 'class="sub-menu"';
                }
                ?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-group"></i>
                      <span>Administrators</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_admin.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a href="add_edit_admin.php" class="">Add Admin</a></li>
                      <? if($file_name=='manage_admin.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a href="manage_admin.php" class="">Manage Admin</a></li>
                  </ul>
              </li>
              <? } // super admin option ?>
              
              
                <?
				if($file_name=='add_edit_menu.php' || $file_name=='manage_menu.php' || $file_name=='manage_contents.php' || $file_name=='manage_contents_order.php' || $file_name=='edit_menu_content.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>Navigations</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_menu.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="add_edit_menu.php">Add Menu</a></li>
                      <? if($file_name=='manage_menu.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_menu.php">Manage Menu</a></li>
                      <? if($file_name=='manage_contents.php' || $file_name=='edit_menu_content.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_contents.php">Manage Content</a></li>
                      <? if($file_name=='manage_contents_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_contents_order.php">Display Order</a></li>
                  </ul>
              </li>
              
                <?
				if($file_name=='add_edit_banner.php' || $file_name=='manage_banner.php' || $file_name=='manage_banner_order.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-picture"></i>
                      <span>Banners</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_banner.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_banner.php" class="">Add Banner</a></li>
                      	<? if($file_name=='manage_banner.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_banner.php" class="">Manage Banner</a></li>
                        <? if($file_name=='manage_banner_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_banner_order.php" class="">Display Order</a></li>
                  </ul>
              </li>
              
              
            
             
                <?
				if($file_name=='add_edit_category.php' || $file_name=='manage_category.php' || $file_name=='manage_category_contents.php' || $file_name=='manage_category_order.php' || $file_name=='edit_menu_category.php'  || $file_name=='edit_category_content.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>Category</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_category.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="add_edit_category.php">Add Category</a></li>
                      <? if($file_name=='manage_category.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_category.php">Manage Category</a></li>
					  <? if($file_name=='manage_category_contents.php' || $file_name=='edit_category_content.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_category_contents.php">Manage Content</a></li>
                      <? if($file_name=='manage_category_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_category_order.php">Display Order</a></li>
                  </ul>
              </li>
			  
			     <?
				if($file_name=='add_edit_gallery.php' || $file_name=='manage_gallery.php' || $file_name=='manage_gallery_order.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-picture"></i>
                      <span>Gallery</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_gallery.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_gallery.php" class="">Add Gallery</a></li>
                      	<? if($file_name=='manage_gallery.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_gallery.php" class="">Manage Gallery</a></li>
                        <? if($file_name=='manage_gallery_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_gallery_order.php" class="">Display Order</a></li>
                  </ul>
              </li>
              
			  
              
                <?
				if($file_name=='add_edit_product.php' || $file_name=='manage_product.php' || $file_name=='manage_product_order.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Products</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_product.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_product.php" class="">Add Product</a></li>
                      	<? if($file_name=='manage_product.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_product.php" class="">Manage Product</a></li>
                        <? if($file_name=='manage_product_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_product_order.php" class="">Display Order</a></li>
                  </ul>
              </li>
                <?
				if($file_name=='add_edit_dream.php' || $file_name=='manage_dream.php' || $file_name=='manage_dream_order.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Dream</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_dream.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_dream.php" class="">Add Dream</a></li>
                      	<? if($file_name=='manage_dream.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_dream.php" class="">Manage Dream</a></li>
                        <? if($file_name=='manage_dream_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_dream_order.php" class="">Display Dream Order</a></li>
                  </ul>
              </li>
                <?
				if($file_name=='add_edit_dream.php' || $file_name=='manage_dream.php' || $file_name=='manage_dream_order.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Pooja Shop</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_pooja.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_pooja.php" class="">Add Pooja</a></li>
                      	<? if($file_name=='manage_pooja.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_pooja.php" class="">Manage Pooja</a></li>
                  </ul>
              </li>
              
              
              
                <?
				if($file_name=='add_edit_temple.php' || $file_name=='manage_temple.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Temple</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_temple.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_temple.php" class="">Add Temple</a></li>
                      	<? if($file_name=='manage_dream.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_temple.php" class="">Manage Temple</a></li>
                  </ul>
              </li>
              
              
                <?
				if($file_name=='add_edit_astrologer.php' || $file_name=='manage_astrologer.php' || 'add_edit_horoscope.php' || 'manage_horoscope.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Astrologer</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_astrologer.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_astrologer.php">Add Astrologer</a></li>
                      	<? if($file_name=='manage_astrologer.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_astrologer.php">Manage Astrologer</a></li>
                        <? if($file_name=='add_edit_horoscope.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_horoscope.php">Add Horoscope</a></li>
                        <? if($file_name=='manage_horoscope.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_horoscope.php">Manage Horoscope</a></li>                        
                  </ul>
              </li>
              
              
                <?
				if($file_name=='add_edit_pandit.php' || $file_name=='manage_pandit.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-camera-retro"></i>
                      <span>Pandits</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                        <? if($file_name=='add_edit_pandit.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="add_edit_pandit.php" class="">Add Pandit</a></li>
                      	<? if($file_name=='manage_pandit.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				        <li <?=$tabcls2?>><a href="manage_pandit.php" class="">Manage Pandit</a></li>
                  </ul>
              </li>
              
			  
			   <?
				if($file_name=='add_edit_team.php' || $file_name=='manage_team.php' || $file_name=='manage_team.php' || $file_name=='manage_team_order.php' || $file_name=='edit_menu_team.php' || $file_name=='manage_team_contents.php' || $file_name=='edit_team_content.php' ){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>Our Team</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_team.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="add_edit_team.php">Add Team</a></li>
                      <? if($file_name=='manage_team.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_team.php">Manage Team</a></li>
					   <? if($file_name=='manage_team_contents.php' || $file_name=='edit_team_content.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_team_contents.php">Manage Content</a></li>
                      <? if($file_name=='manage_team_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_team_order.php">Display Order</a></li>
                  </ul>
              </li>
               <?
				if($file_name=='add_edit_event.php' || $file_name=='manage_event.php' || $file_name=='manage_event.php' || $file_name=='manage_event_order.php' || $file_name=='edit_menu_event.php' || $file_name=='edit_event_content.php' || $file_name=='manage_event_contents.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>PPT Upload</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_event.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="add_edit_event.php">Add PPT</a></li>
                      <? if($file_name=='manage_event.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_event.php">Manage PPT</a></li>
					  <? if($file_name=='manage_event_contents.php' || $file_name=='edit_event_content.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
					   <li <?=$tabcls2?>><a class="" href="manage_event_contents.php">Manage Content</a></li>
                      <? if($file_name=='manage_event_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_event_order.php">Display Order</a></li>
                  </ul>
              </li>
			  
			  
			  
			    <?
				if($file_name=='add_edit_deity.php' || $file_name=='manage_deity.php' || $file_name=='manage_deity.php' || $file_name=='manage_deity_order.php'|| $file_name='manage_deity_contents.php'){
				$tabcls = 'class="sub-menu active"';
				} else {
				$tabcls = 'class="sub-menu"';
				}
				?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>Deity Song Upload</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_deity.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="add_edit_deity.php">Add Deity</a></li>
                      <? if($file_name=='manage_deity.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_deity.php">Manage Deity</a></li>
					  <? if($file_name=='manage_deity_contents.php' || $file_name=='edit_event_content.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
					   <li <?=$tabcls2?>><a class="" href="manage_deity_contents.php">Manage Content</a></li>
                      <? if($file_name=='manage_deity_order.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a class="" href="manage_deity_order.php">Display Order</a></li>
                  </ul>
              </li>
			  
			  
              
                  <?
                if($file_name=='newsletter_subscriptions.php'){
                $tabcls = 'class="active"';
                } else {
                $tabcls = '';
                }
                ?>
				  <li <?=$tabcls?>>
                  <a class="" href="newsletter_subscriptions.php">
                    <i class="icon-group"></i>
                    <span>Subscriptions</span>
                  </a>
              </li>
              
              
              <?
                if($file_name=='add_edit_newsletter.php' || $file_name=='manage_newsletter.php' || $file_name=='subscribe_list.php' || $file_name=='send_newsletter.php'){
                $tabcls = 'class="sub-menu active"';
                } else {
                $tabcls = 'class="sub-menu"';
                }
                ?>
				  <li <?=$tabcls?>>
                  <a href="javascript:;" class="">
                      <i class="icon-envelope"></i>
                      <span>Newsletters</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <? if($file_name=='add_edit_newsletter.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a href="add_edit_newsletter.php" class="">Add Newsletter</a></li>
                      <? if($file_name=='manage_newsletter.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a href="manage_newsletter.php" class="">Manage Newsletter</a></li>
                      <? if($file_name=='subscribe_list.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <!--<li <?=$tabcls2?>><a href="subscribe_list.php" class="">Subscribed Users</a></li>-->
                      <? if($file_name=='send_newsletter.php'){ $tabcls2 = 'class="active"'; } else { $tabcls2 = ''; } ?>
				      <li <?=$tabcls2?>><a href="send_newsletter.php" class="">Send Newsletter</a></li>
                  </ul>
              </li>
              
           

			  

              <li>
                  <a class="" href="logout.php">
                    <i class="icon-user"></i>
                    <span>Log Out</span>
                  </a>
              </li>
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
	  
<!-- END SIDEBAR -->