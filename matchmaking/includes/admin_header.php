<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>R Gyan</title>
   <link rel="shortcut icon" href="../images/favicon.png?v=3" >
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="Mosaddek" name="author" />
   <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="css/style.css" rel="stylesheet" />
   <link href="css/style-responsive.css" rel="stylesheet" />
   <link href="css/style-default.css" rel="stylesheet" id="style_color" />
   
   <link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
    
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <div id="header" class="navbar navbar-inverse navbar-fixed-top">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class="navbar-inner">
           <div class="container-fluid">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class="sidebar-toggle-box hidden-phone">
                   <div class="icon-reorder tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class="brand" href="dashboard.php" style="overflow:hidden;"><?=getAdminLogo()?></a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="arrow"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               <div id="top_menu" class="nav notify-row">
                   <h3>Admin Control Panel</h3>
               </div>
               
               
               
               <!-- END  NOTIFICATION -->
               <div class="top-nav ">
                   <ul class="nav pull-right top-menu" >
                   
                   <!-- BEGIN NOTIFICATION DROPDOWN -->
                   
                   
                   <li class="dropdown" id="header_notification_bar">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                               <i class="icon-bell-alt"></i>
                               <span class="badge badge-warning"><?=countNewOrders()?></span>
                           </a>
                           <ul class="dropdown-menu extended notification">
                               <li>
                                   <p>You have <?=countNewOrders()?> new orders</p>
                               </li>
                               
                               
                               <?
								$tr = 0;
								$sql="SELECT * FROM orders WHERE is_viewed='0'";
								$sql .= " ORDER BY date_added DESC";
								$sql .= " LIMIT 0,10";
								$result=$obj_db->query($sql) or die($obj_db->error());
								$numrows=$obj_db->numRows($result);
								if($numrows>0) {
								while($row=$obj_db->fetchArray($result)) { $tr++;
								
								$total = $row['total_price']+$row['shipping_price'];
								?>
                               
                               <li>
                                   <a href="order_details.php?eid=<?=$row['id']?>">
                                       <span class="label label-important"><i class="icon-shopping-cart"></i></span>
                                       <?=date("d/m/Y h:i:s",strtotime($row['date_added']))?> - 
                                       <span class="small italic">Rs. <?=number_format($total,2)?></span>
                                   </a>
                               </li>
                               
                               <? } ?>
                               
                               <? } ?>

                               <li>
                                   <a href="manage_order.php">See all orders</a>
                               </li>
                           </ul>
                       </li>
                       
                   <!-- END NOTIFICATION DROPDOWN -->    
                       
                       
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                               <img src="img/admin.png" alt="">
                               <span class="username"><?=ucfirst($_SESSION['LoginID'])?></span>
                               <b class="caret"></b>
                           </a>
                           <ul class="dropdown-menu extended logout">
                               <li><a href="change_settings.php"><i class="icon-cog"></i> Admin Settings</a></li>
                               <li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
   
      <? include("../includes/left_links.php")?>