<div class="main-header">
                
                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="<?=SITE_PATH?>" title="Home">
                            <?=getWebsiteLogo();?>
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                        
                        <? 
						  $nr = 0;
						  $obj_db_tab=new DB();
						  $obj_db_tab->open();
						  $sql_tab = "SELECT * FROM main_menu WHERE status='1' and parent='0' and dis_on_top='1' ORDER BY position";
						  $result_tab=$obj_db_tab->query($sql_tab) or die($obj_db_tab->error());
						  $numRows = $obj_db_tab->numRows($result_tab);
						  while($row_tab=$obj_db_tab->fetchArray($result_tab)){ $nr++;
						  $active = '';
						  $hassub = '';
						  $last = '';
						  
						  ?>
						  
						  <? 
						  $nmrc = getNumChild($row_tab['id']); // count number of childs of root menu for # link
						  ?>
						  
						  <?
							if(trim(strtolower($row_tab['name']))=="home") {
							  $pageLink = SITE_PATH;
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="contact us" || trim(strtolower($row_tab['name']))=="contact") {
							  $pageLink = SITE_PATH.'contact.php?pageT='.$row_tab['name'].'&page_id='.$row_tab['id'].'';
							  $target = '';
							  
							 } elseif(trim(strtolower($row_tab['name']))=="packages") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="india") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_type=india';
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="international") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_type=international';
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="hotels") {
							  $pageLink = SITE_PATH.'hotel_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							} elseif($row_tab['external_link']!="") {
							  $pageLink = showText($row_tab['external_link']);
							  $target = 'target="_blank"';
							} elseif($nmrc>=1) {
							  $pageLink = 'innerPage.php?pageT='.showText($row_tab['name']).'&page_id='.getFirstChild($row_tab[id]).'';
							} else {
							  $pageLink = SITE_PATH.'innerPage.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							}
						  
						  
						 
						  
						  $pid = getParentMenu($_REQUEST['page_id']); 
						  $ppid = getParentMenu($pid); 
						  
						  
					 
						  if($_REQUEST['page_id']==$row_tab['id'] || $row_tab['id'] == $pid || $row_tab['id'] == $ppid){ 
							$active = 'active';
							if($nmrc>='1'){ $hassub = 'menu-item-has-children';}
							if($nr==$numRows){ $last = 'last';}
						  } else { 
							$active = '';
							if($nmrc>='1'){ $hassub = 'menu-item-has-children';}
							if($nr==$numRows){ $last = 'last';}
						  } 
						  
						  
						  // define home select class
							  if(trim(strtolower($row_tab['name']))=="home" && (trim($_REQUEST['pageT']=='' || trim($_REQUEST['pageT']=='Home')))) 
							  $active = 'active';
						  //=============================
						  
						  if(showText($row_tab['name'])=='Packages' || showText($row_tab['name'])=='India' || showText($row_tab['name'])=='International' || showText($row_tab['name'])=='Hotels'){
							$hassub = 'menu-item-has-children';  
						  }
						  ?>
                        
                            <li class="<?=$active?> <?=$hassub?> <?=$last?>">
                                <a href="<?=$pageLink?>" <?=$target?> ><?=showText($row_tab['name'])?></a>
                             
                            <? if(showText($row_tab['name'])=='Packages'){ // IF MENU  NAME PACKAGES?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_package_type WHERE 1='1' ORDER BY title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&package_type='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // PACKAGE LAYER CONDITION ?>
                            
                            
                            <? } elseif(showText($row_tab['name'])=='India'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_destinations WHERE status='1' AND type='india' ORDER BY position,title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_id='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // INDIA LAYER CONDITION ?>
                                
                           
                           <? } elseif(showText($row_tab['name'])=='International'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_destinations WHERE status='1' AND type='international' ORDER BY position,title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_id='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // INTERNATIONAL LAYER CONDITION ?>
                                
                           
                           <? } elseif(showText($row_tab['name'])=='Hotels'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_hotel_type WHERE 1='1' ORDER BY title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'hotel_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&hotel_type='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // HOTEL LAYER CONDITION ?>
                            
                            
                            <? } else { // IF MENU NAME !PACKAGE !INDIA !INTERNATIONAL !HOTEL ?> 
                               
                               <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM main_menu WHERE status='1' and parent='".$row_tab['id']."' and parent!='1' ORDER BY position";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $numChild2 = getNumChild($row_tab2['id']); // count number of childs of sub menu for # link
								  
									if(trim(strtolower($row_tab2['name']))=="home") {
									  $pageLink2 = SITE_PATH;
									  $target2 = '';
									} elseif($row_tab2['external_link']!="") {
									  $pageLink2 = showText($row_tab2['external_link']);
									  $target2 = 'target="_blank"';
									} elseif($numChild2>=1) {
									  $pageLink2 = "#";
									} else {
									  $pageLink2 = SITE_PATH.'innerPage.php?pageT='.showText($row_tab2['name']).'&page_id='.$row_tab2['id'];
									  $target2 = '';
									}
								  ?>
								  <? // apply # link if menu contains submenu
								  if($numChild2>=1){
								  $hassub2 = 'menu-item-has-children';
								  } else {
								  $hassub2 = '';
								  }
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['name'])?></a>
                                        <? 
										  $obj_db_tab3=new DB();
										  $obj_db_tab3->open();
										  $sql_tab3 = "SELECT * FROM main_menu WHERE status='1' and parent='".$row_tab2['id']."' and parent!='1' ORDER BY position";
										  $result_tab3=$obj_db_tab3->query($sql_tab3) or die($obj_db_tab3->error());
										  $nmr3 = $obj_db_tab3->numRows($sql_tab3);
										  if($nmr3>=1){
										  ?>
                                        <ul>
                                            <? 
											  while($row_tab3=$obj_db_tab3->fetchArray($result_tab3)){
											  
											  $numChild3 = getNumChild($row_tab3['id']); // count number of childs of sub menu for # link
											  
												if(trim(strtolower($row_tab3['name']))=="home") {
												  $pageLink3 = SITE_PATH;
												  $target3 = '';
												} elseif($row_tab3['external_link']!="") {
												  $pageLink3 = showText($row_tab3['external_link']);
												  $target3 = 'target="_blank"';
												} elseif($numChild3>=1) {
												  $pageLink3 = "#";
												} else {
												  $pageLink3 = SITE_PATH.'innerPage.php?pageT='.showText($row_tab3['name']).'&page_id='.$row_tab3['id'];
												  $target3 = '';
												}
											  ?>
                                            <li><a href="<?=$pageLink3?>" <?=$target3?>><?=showText($row_tab3['name'])?></a></li>
                                            <? } // LAYER 3 LOOP ?>
                                        </ul>
                                        <? } // LAYER 3 CONDITION ?>
                                    </li>
                                  <? } // SECOND LAYER LOOP?>
                                </ul>
                                
                                <? } // SECOND LAYER CONDITION ?>
                                
                            <? } // IF MENU NAME PACKAGE ?>     
                            </li>
                            
                          
                          <? } // TOP LAYER LOOP ?>
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                
                
                <ul id="mobile-primary-menu" class="menu">
                        
                        <? 
						  $nr = 0;
						  $obj_db_tab=new DB();
						  $obj_db_tab->open();
						  $sql_tab = "SELECT * FROM main_menu WHERE status='1' and parent='0' and dis_on_top='1' ORDER BY position";
						  $result_tab=$obj_db_tab->query($sql_tab) or die($obj_db_tab->error());
						  $numRows = $obj_db_tab->numRows($result_tab);
						  while($row_tab=$obj_db_tab->fetchArray($result_tab)){ $nr++;
						  $active = '';
						  $hassub = '';
						  $last = '';
						  
						  ?>
						  
						  <? 
						  $nmrc = getNumChild($row_tab['id']); // count number of childs of root menu for # link
						  ?>
						  
						  <?
							if(trim(strtolower($row_tab['name']))=="home") {
							  $pageLink = SITE_PATH;
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="contact us" || trim(strtolower($row_tab['name']))=="contact") {
							  $pageLink = SITE_PATH.'contact.php?pageT='.$row_tab['name'].'&page_id='.$row_tab['id'].'';
							  $target = '';
							  
							 } elseif(trim(strtolower($row_tab['name']))=="packages") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="india") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_type=india';
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="international") {
							  $pageLink = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_type=international';
							  $target = '';
							} elseif(trim(strtolower($row_tab['name']))=="hotels") {
							  $pageLink = SITE_PATH.'hotel_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							} elseif($row_tab['external_link']!="") {
							  $pageLink = showText($row_tab['external_link']);
							  $target = 'target="_blank"';
							} elseif($nmrc>=1) {
							  $pageLink = 'innerPage.php?pageT='.showText($row_tab['name']).'&page_id='.getFirstChild($row_tab[id]).'';
							} else {
							  $pageLink = SITE_PATH.'innerPage.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'];
							  $target = '';
							}
						  
						  
						 
						  
						  $pid = getParentMenu($_REQUEST['page_id']); 
						  $ppid = getParentMenu($pid); 
						  
						  
					 
						  if($_REQUEST['page_id']==$row_tab['id'] || $row_tab['id'] == $pid || $row_tab['id'] == $ppid){ 
							$active = 'active';
							if($nmrc>='1'){ $hassub = 'menu-item-has-children';}
							if($nr==$numRows){ $last = 'last';}
						  } else { 
							$active = '';
							if($nmrc>='1'){ $hassub = 'menu-item-has-children';}
							if($nr==$numRows){ $last = 'last';}
						  } 
						  
						  
						  // define home select class
							  if(trim(strtolower($row_tab['name']))=="home" && (trim($_REQUEST['pageT']=='' || trim($_REQUEST['pageT']=='Home')))) 
							  $active = 'active';
						  //=============================
						  
						  if(showText($row_tab['name'])=='Packages' || showText($row_tab['name'])=='India' || showText($row_tab['name'])=='International' || showText($row_tab['name'])=='Hotels'){
							$hassub = 'menu-item-has-children';  
						  }
						  ?>
                        
                            <li class="<?=$active?> <?=$hassub?> <?=$last?>">
                                <a href="<?=$pageLink?>" <?=$target?> ><?=showText($row_tab['name'])?></a>
                             
                            <? if(showText($row_tab['name'])=='Packages'){ // IF MENU  NAME PACKAGES?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_package_type WHERE 1='1' ORDER BY title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&package_type='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // PACKAGE LAYER CONDITION ?>
                            
                            
                            <? } elseif(showText($row_tab['name'])=='India'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_destinations WHERE status='1' AND type='india' ORDER BY position,title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_id='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // INDIA LAYER CONDITION ?>
                                
                           
                           <? } elseif(showText($row_tab['name'])=='International'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_destinations WHERE status='1' AND type='international' ORDER BY position,title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'package_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&destination_id='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // INTERNATIONAL LAYER CONDITION ?>
                                
                           
                           <? } elseif(showText($row_tab['name'])=='Hotels'){ // IF MENU  NAME INDIA?> 
                            
                            
                            <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM tbl_hotel_type WHERE 1='1' ORDER BY title";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $pageLink2 = SITE_PATH.'hotel_list.php?pageT='.showText($row_tab['name']).'&page_id='.$row_tab['id'].'&hotel_type='.$row_tab2['id'];
								  $target2 = '';
								  
								  $hassub2 = '';
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['title'])?></a>
                                    </li>
                                  <? } // PACKAGE LAYER LOOP?>
                                </ul>
                                
                                <? } // INTERNATIONAL LAYER CONDITION ?>
                            
                            
                            <? } else { // IF MENU NAME !PACKAGE !INDIA !INTERNATIONAL !HOTEL ?> 
                               
                               <? 
								  $obj_db_tab2=new DB();
								  $obj_db_tab2->open();
								  $sql_tab2 = "SELECT * FROM main_menu WHERE status='1' and parent='".$row_tab['id']."' and parent!='1' ORDER BY position";
								  $result_tab2=$obj_db_tab2->query($sql_tab2) or die($obj_db_tab2->error());
								  $nmr = $obj_db_tab2->numRows($sql_tab2);
								  if($nmr>=1){
								  ?>
                               
                                <ul>
                                
                                <? 
								  while($row_tab2=$obj_db_tab2->fetchArray($result_tab2)){
								  
								  $numChild2 = getNumChild($row_tab2['id']); // count number of childs of sub menu for # link
								  
									if(trim(strtolower($row_tab2['name']))=="home") {
									  $pageLink2 = SITE_PATH;
									  $target2 = '';
									} elseif($row_tab2['external_link']!="") {
									  $pageLink2 = showText($row_tab2['external_link']);
									  $target2 = 'target="_blank"';
									} elseif($numChild2>=1) {
									  $pageLink2 = "#";
									} else {
									  $pageLink2 = SITE_PATH.'innerPage.php?pageT='.showText($row_tab2['name']).'&page_id='.$row_tab2['id'];
									  $target2 = '';
									}
								  ?>
								  <? // apply # link if menu contains submenu
								  if($numChild2>=1){
								  $hassub2 = 'menu-item-has-children';
								  } else {
								  $hassub2 = '';
								  }
								  ?>
                                    <li class="<?=$hassub2?>">
                                        <a href="<?=$pageLink2?>" <?=$target2?>><?=showText($row_tab2['name'])?></a>
                                        <? 
										  $obj_db_tab3=new DB();
										  $obj_db_tab3->open();
										  $sql_tab3 = "SELECT * FROM main_menu WHERE status='1' and parent='".$row_tab2['id']."' and parent!='1' ORDER BY position";
										  $result_tab3=$obj_db_tab3->query($sql_tab3) or die($obj_db_tab3->error());
										  $nmr3 = $obj_db_tab3->numRows($sql_tab3);
										  if($nmr3>=1){
										  ?>
                                        <ul>
                                            <? 
											  while($row_tab3=$obj_db_tab3->fetchArray($result_tab3)){
											  
											  $numChild3 = getNumChild($row_tab3['id']); // count number of childs of sub menu for # link
											  
												if(trim(strtolower($row_tab3['name']))=="home") {
												  $pageLink3 = SITE_PATH;
												  $target3 = '';
												} elseif($row_tab3['external_link']!="") {
												  $pageLink3 = showText($row_tab3['external_link']);
												  $target3 = 'target="_blank"';
												} elseif($numChild3>=1) {
												  $pageLink3 = "#";
												} else {
												  $pageLink3 = SITE_PATH.'innerPage.php?pageT='.showText($row_tab3['name']).'&page_id='.$row_tab3['id'];
												  $target3 = '';
												}
											  ?>
                                            <li><a href="<?=$pageLink3?>" <?=$target3?>><?=showText($row_tab3['name'])?></a></li>
                                            <? } // LAYER 3 LOOP ?>
                                        </ul>
                                        <? } // LAYER 3 CONDITION ?>
                                    </li>
                                  <? } // SECOND LAYER LOOP?>
                                </ul>
                                
                                <? } // SECOND LAYER CONDITION ?>
                                
                            <? } // IF MENU NAME PACKAGE ?>     
                            </li>
                            
                          
                          <? } // TOP LAYER LOOP ?>
                            
                        </ul>
                
                
                    
                    <ul class="mobile-topnav container">
                        <li><a href="#">MY ACCOUNT</a></li>
                        <li class="ribbon language menu-color-skin">
                            <a href="#" data-toggle="collapse">ENGLISH</a>
                            <ul class="menu mini">
                                <li><a href="#" title="Dansk">Korean</a></li>
                                <li><a href="#" title="Deutsch">Chinese</a></li>
                            </ul>
                        </li>
                        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency menu-color-skin">
                            <a href="#">USD</a>
                            <ul class="menu mini">
                                <li><a href="#" title="INR">INR</a></li>
                                <li><a href="#" title="WOn">WON</a></li>
                                <li class="active"><a href="#" title="USD">USD</a></li>
                                <li><a href="#" title="EUR">EUR</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                </nav>
            </div>