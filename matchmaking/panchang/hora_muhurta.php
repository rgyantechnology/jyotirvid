<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Panchang Planetary Details";
require_once "../panchang_header.php";

$api = new ApiCall();
$data = $api->panchangApiCall('hora_muhurta');
?>
    <div class="UI-II ng-scope">
        <h2 class="text-center">Hora Muhurta</h2>
        <div class="col-md-6">
            <h4 class="text-center"> DAY HORA</h4><div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr>
                    <th>Hora Name</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for($i=0;$i<count($data['hora']['day']);$i++)
                {
                ?>
                    <tr>
                        <td class="dark-td"><?=$data['hora']['day'][$i]['hora']?></td>
                        <td class="ng-binding"><?=$data['hora']['day'][$i]['time']?></td>
                    </tr>
                <?php
                }
                ?>

                </tbody>
            </table>
        </div>
        </div>
        <div class="col-md-6">
            <h4 class="text-center"> NIGHT HORA</h4><div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr>
                    <th>Hora Name</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for($i=0;$i<count($data['hora']['night']);$i++)
                {
                    ?>
                    <tr>
                        <td class="dark-td"><?=$data['hora']['night'][$i]['hora']?></td>
                        <td class="ng-binding"><?=$data['hora']['night'][$i]['time']?></td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
        </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>