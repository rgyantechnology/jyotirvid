<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Panchang Details";
require_once "../panchang_header.php";

$api = new ApiCall();
$data = $api->panchangApiCall('advanced_panchang/sunrise');

?>
    <div class="row">
        <div class="col-md-12">
            <?php
			if(count($data) > 0) {
			?>
            <div class="row">
                <div class="col-md-12">
                    <div class="UI-II n-box-shadow" style="overflow: hidden;">
                        <!-- <h2 class="header-title m-b-md b-n "> Advanced Panchang Details</h2>-->
                        <div class="panchang-details">
                            <article class="pc-common-elements" >
                                <h2 class="text-center">Panchang For <?=$_SESSION['day']?>-<?=$_SESSION['month']?>-<?=$_SESSION['year']?> </h2>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Sunrise</h4>
                                    <p><?=$data['sunrise']?>

                                    </p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Sunset</h4>
                                    <p><?=$data['sunset']?>

                                    </p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Moonrise</h4>
                                    <p><?=$data['moonrise']?>

                                    </p>
                                </div>
                                <div class="col-md-3 last pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Moonset</h4>
                                    <p><?=$data['moonset']?>

                                    </p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Sun Sign</h4>
                                    <p><?=$data['sun_sign']?></p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Moon Sign</h4>
                                    <p><?=$data['moon_sign']?></p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Ritu</h4>
                                    <p><?=$data['ritu']?></p>
                                </div>

                                <div class="col-md-3 pachang-data-block last panchang-block-width">
                                    <h4 class="meta-post">Ayana</h4>
                                    <p><?=$data['ayana']?></p>
                                </div>

                            </article>
                            <article class="pc-common-elements" >
                                <div class="col-md-6 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post meta_post_title">Inauspicious Period</h4>
                                    <p>
                                        <strong>Rahu Kalam</strong> - <?=$data['rahukaal']['start']?>:<?=$data['rahukaal']['end']?><br>
                                        <strong>Yamaganda Kalam</strong> - <?=$data['yamghant_kaal']['start']?>:<?=$data['yamghant_kaal']['end']?><br>
                                        <strong>Gulika Kalam</strong> - <?=$data['guliKaal']['start']?>:<?=$data['guliKaal']['end']?>
                                    </p>
                                </div>
                                <div class="col-md-6 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post meta_post_title">Lunar Month</h4>
                                    <p>
                                        <strong>Amanta</strong>  - <?=$data['hindu_maah']['amanta']?><br>
                                        <strong>Purnimanta</strong>  - <?=$data['hindu_maah']['purnimanta']?><br>
                                        <strong>Paksha</strong> - <?=$data['paksha']?>
                                    </p>
                                </div>

                            </article>
                            <article class="pc-five-elements p-elements">
                                <div class="col-md-3 text-center pachang-data-block border-color-none">

                                    <h4 class="meta-post">Tithi</h4>

                                    <p><?=$data['tithi']['details']['tithi_name']?> TILL <?=$data['tithi']['end_time']['hour']?>:<?=$data['tithi']['end_time']['minute']?></p>

                                </div>

                                <div class="col-md-3 pachang-data-block border-color-none">

                                    <h4 class="meta-post">Yog</h4>
                                    <p><?=$data['yog']['details']['yog_name']?> TILL <?=$data['yog']['end_time']['hour']?>:<?=$data['yog']['end_time']['minute']?></p>

                                </div>

                                <div class="col-md-3  pachang-data-block border-color-none">
                                    <h4 class="meta-post">Nakshatra</h4>
                                    <p><?=$data['nakshatra']['details']['nak_name']?> TILL <?=$data['nakshatra']['end_time']['hour']?>:<?=$data['karan']['end_time']['minute']?></p>

                                </div>
                                <div class="col-md-3 last pachang-data-block border-color-none">
                                    <h4 class="meta-post">Karan</h4>
                                    <p><?=$data['karan']['details']['karan_name']?> TILL <?=$data['karan']['end_time']['hour']?>:<?=$data['karan']['end_time']['minute']?></p>

                                </div>

                            </article>
                            <article class="pc-hindu-month">
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post meta_post_title">Abhijit Muhurta</h4>
                                    <p><?=$data['abhijit_muhurta']['start']?>:<?=$data['abhijit_muhurta']['end']?></p>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Adhik Mass</h4>
                                    <?php
                                    if($data['hindu_maah']['adhik_status'])
                                    {
                                        ?>
                                        YES
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        NO
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-3 pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Shaka Samvat</h4>
                                    <p><?=$data['shaka_samvat']?> - <?=$data['shaka_samvat_name']?></p>
                                </div>
                                <div class="col-md-3 last pachang-data-block panchang-block-width">
                                    <h4 class="meta-post">Vikram Samvat</h4>
                                    <p><?=$data['vikram_samvat']?> - <?=$data['vkram_samvat_name']?></p>
                                </div>
                            </article>


                        </div>
                    </div>

                </div>

            </div>
            <?php } else { ?>
            No Match Available, Try again later!
            <?php } ?>
        </div>
    </div>
<?php require_once("../footer.php"); ?>