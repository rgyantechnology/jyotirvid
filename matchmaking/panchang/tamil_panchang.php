<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Panchang Details";
require_once "../panchang_header.php";

$api = new ApiCall();
$data = $api->panchangApiCall('tamil_panchang');

?>

    <!--Place holder for the horoscope templates-->
    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="UI-II n-box-shadow" style="overflow: hidden;padding: 12px 0;">

                <div class="panchang-details">
                    <article class="pc-common-elements" >
                        <h2 class="text-center">Panchang For <?=$_SESSION['day']?>-<?=$_SESSION['month']?>-<?=$_SESSION['year']?> </h2>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Sunrise</h4>
                            <p><?=$data['sunrise']?>

                            </p>
                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Sunset</h4>
                            <p><?=$data['sunset']?>

                            </p>
                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Moonrise</h4>
                            <p><?=$data['moonrise']?>

                            </p>
                        </div>
                        <div class="col-md-3 last pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Moonset</h4>
                            <p><?=$data['moonset']?>

                            </p>
                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Sun Sign</h4>
                            <p><?=$data['sun_sign']?></p>
                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Moon Sign</h4>
                            <p><?=$data['moon_sign']?></p>
                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Ritu</h4>
                            <p><?=$data['ritu']?></p>
                        </div>

                        <div class="col-md-3 pachang-data-block last panchang-block-width">
                            <h4 class="meta-post">Ayana</h4>
                            <p><?=$data['ayana']?></p>
                        </div>

                    </article>
                    <article class="pc-common-elements" >
                        <div class="col-md-6 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Inauspicious Period</h4>
                            <p>
                                <strong>Rahu Kalam</strong> - <?=$data['rahu_kaal']['start']?> To <?=$data['rahu_kaal']['end']?><br>
                                <strong>Yamaganda Kalam</strong> - <?=$data['yamghant_kaal']['start']?> To <?=$data['yamghant_kaal']['end']?><br>
                                <strong>Gulika Kalam</strong> - <?=$data['gulik_kaal']['start']?> To <?=$data['gulik_kaal']['end']?>
                            </p>
                        </div>
                        <div class="col-md-6 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Lunar Month</h4>
                            <p>
                                <strong>Amanta</strong>  - <?=$data['hindu_month']['amanta']?><br>
                                <strong>Purnimanta</strong>  - <?=$data['hindu_month']['purnimanta']?><br>
                                <strong>Paksha</strong> - <?=$data['paksha']?>
                            </p>
                        </div>

                    </article>
                    <article class="pc-five-elements p-elements">
                        <div class="col-md-3 text-center pachang-data-block border-color-none">
                            <?php
                            $tithi = $data['tithi'];
                            ?>
                            <h4 class="meta-post">Tithi</h4>

                            <p><?=$tithi['details']?> <br><small>&nbsp;&nbsp;upto&nbsp;&nbsp;</small> <?=$tithi['end_time']?></p>

                        </div>

                        <div class="col-md-3 pachang-data-block border-color-none">
                            <?php
                            $yog = $data['yog'];
                            ?>

                            <h4 class="meta-post">Yog</h4>
                            <p><?=$yog['details']?> <br><small>&nbsp;&nbsp;upto&nbsp;&nbsp;</small> <?=$yog['end_time']?> </p>

                        </div>

                        <div class="col-md-3  pachang-data-block border-color-none">
                            <?php
                            $nakshatra = $data['nakshatra'];
                            ?>

                            <h4 class="meta-post">Nakshatra</h4>
                            <p><?=$nakshatra['details']?> <br><small>&nbsp;&nbsp;upto&nbsp;&nbsp;</small> <?=$nakshatra['end_time']?></p>

                        </div>
                        <div class="col-md-3 last pachang-data-block border-color-none">
                            <?php
                            $karan = $data['karan'];
                            ?>

                            <h4 class="meta-post">Karan</h4>
                            <p><?=$karan['details'];?> <br><small>&nbsp;&nbsp;upto&nbsp;&nbsp;</small> <?=$karan['end_time']?></p>

                        </div>

                    </article>
                    <article class="pc-hindu-month">
                        <div class="col-md-6 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Abhijit Muhurta</h4>
                            <p><?=$data['abhijit_muhurta']['start']?>
                                To
                                <?=$data['abhijit_muhurta']['end']?></p>
                        </div>

                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Shaka Samvat</h4>
                            <p><?=$data['shaka_samvat']?> - <?=$data['shaka_samvat_name']?></p>
                        </div>
                        <div class="col-md-3 last pachang-data-block panchang-block-width">
                            <h4 class="meta-post">Vikram Samvat</h4>
                            <p><?=$data['vikram_samvat']?> - <?=$data['vkram_samvat_name']?></p>
                        </div>
                    </article>

                    <article class="pc-inauspicious-elements">

                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Dur Muhurat</h4>

                            <?php
                            if($data['dur_muhurtha']) {
                                for ($i = 0; $i < count($data['dur_muhurtha']); $i++) {
                                    ?>


                                    <p><?= $data['dur_muhurtha'][$i]['start_time'] ?>
                                        To
                                        <?= $data['dur_muhurtha'][$i]['start_time'] ?></p>
                                    <?php
                                }
                            }
                            else
                            {
                                ?>
                                --
                                <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-3 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Amrit Kaal</h4>

                            <?php
                            if($data['amrit_kaal']) {
                                for ($i = 0; $i < count($data['amrit_kaal']); $i++) {
                                    ?>


                                    <p><?= $data['amrit_kaal'][$i]['start'] ?>
                                        To
                                        <?= $data['amrit_kaal'][$i]['end'] ?></p>
                                    <?php
                                }
                            }
                            else
                            {
                            ?>
                                --
                            <?php
                            }
                            ?>
                        </div>

                        <div class="col-md-6 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Anandadi Yog</h4>
                            <?php

                            if($data['anandadi_yog']) {
                            for($i = 0;$i<count($data['anandadi_yog']);$i++)
                            {
                                ?>
                                <?php
                                if(count($data['anandadi_yog'])-1 == $i)
                                {
                                    ?>
                                    <p><?=$data['anandadi_yog'][$i]['yog_name']?></p>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <p>
                                        <?=$data['anandadi_yog'][$i]['yog_name']?>
                                        upto
                                        <?=$data['anandadi_yog'][$i]['end_time']?>
                                    </p>
                                    <?php

                                }
                                ?>

                            <?php
                                }
                            }
                            else
                            {
                                ?>
                                --
                                <?php
                            }
                            ?>

                        </div>
                    </article>

                    <article class="pc-disha-shool">
                        <div class="col-md-12 pachang-data-block panchang-block-width">
                            <h4 class="meta-post meta_post_title">Varjyam</h4>

                            <?php
                            if($data['varjyam']) {
                                for ($i = 0; $i < count($data['varjyam']); $i++) {
                                    ?>


                                    <p><?= $data['varjyam'][$i]['start'] ?>
                                        To
                                        <?= $data['varjyam'][$i]['end'] ?></p>
                                    <?php
                                }
                            }
                            else
                            {
                                ?>
                                --
                                <?php
                            }
                            ?>
                        </div>

                    </article>

                </div><!-- grid-items -->
            </div>
        </div>

        <div class="col-md-2"></div>

    </div>
<?php require_once("../footer.php"); ?>