<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "match manglik report";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_manglik_report');
?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="UI-II zero-padding">
                        <section id="photos" class="match-m-manglik-overlay">
                            <p style="top:15%">  Male Manglik Details</p>

                            <p style="top:25%"><?=$data['male']['percentage_manglik_after_cancellation']?> % manglik</p>

                        </section>
                            <div class="grid" style="padding: 10px;">

                                <div class="grid-col-12">
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Based on house</h2>
                                            <div class="manglik-points">
                                                <?php
                                                for($i=0;$i<count($data['male']['manglik_present_rule']['based_on_house']);$i++)
                                                {
                                                    ?>
                                                    <p class="text-left ng-scope ng-binding"><?=$data['male']['manglik_present_rule']['based_on_house'][$i]?></p>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="spacer-40 clearfix"></div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Based on Aspects</h2>
                                            <div class="manglik-points">
                                                <?php
                                                for($j=0;$j<count($data['male']['manglik_present_rule']['based_on_aspect']);$j++)
                                                {
                                                ?>
                                                <p class="text-left ng-scope ng-binding">
                                                    <?=$data['male']['manglik_present_rule']['based_on_aspect'][$j]?>
                                                    <?php
                                                    }
                                                    ?>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Manglik Effect</h2>
                                            <div class="manglik-points">
                                                <p class="text-left ng-binding">Manglik dosha is <?=$data['male']['manglik_status']?></p>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Manglik Analysis</h2>
                                            <div class="manglik-points">
                                                <p class="text-left ng-binding"><?=$data['male']['manglik_report']?></p>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="UI-II zero-padding">
                        <section id="photos" class="match-f-manglik-overlay">
                            <p style="top:15%">  Female Manglik Details</p>

                            <p style="top:25%"><?=$data['female']['percentage_manglik_after_cancellation'] ?> % manglik</p>

                        </section>

                            <div class="grid" style="padding: 10px;">

                                <div class="grid-col-12">
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Based on house</h2>
                                            <div class="manglik-points">
                                                <?php
                                                for($i=0;$i<count($data['female']['manglik_present_rule']['based_on_house']);$i++)
                                                {
                                                    ?>
                                                    <p class="text-left ng-scope ng-binding"><?=$data['female']['manglik_present_rule']['based_on_house'][$i]?></p>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Based on Aspects</h2>
                                            <div class="manglik-points">
                                                <?php
                                                for($j=0;$j<count($data['female']['manglik_present_rule']['based_on_aspect']);$j++)
                                                {
                                                ?>
                                                <p class="text-left ng-scope ng-binding">
                                                    <?=$data['female']['manglik_present_rule']['based_on_aspect'][$j]?>
                                                    <?php
                                                    }
                                                    ?>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2> Manglik Effect</h2>
                                            <div class="manglik-points">
                                                <p class="text-left ng-binding">Manglik dosha is <?=$data['female']['manglik_status']?></p>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="grid-col-12">
                                        <div class="blocks">
                                            <h2>Manglik Analysis</h2>
                                            <div class="manglik-points">
                                                <p class="text-left ng-binding"><?=$data['female']['manglik_report']?></p>
                                            </div>
                                            <div class="spacer-40 clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="UI-II dark-I matching-conculsion-status">
                        <h2 style="color: #fff;border-bottom-color: #218C5E;">Conclusion</h2>
                        <div class="ashtakoot-conculsion-report">
                            <p class="text-left ng-binding" style="color: #fff;"><?=$data['conclusion']['report']?></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
<?php require_once("../footer.php"); ?>