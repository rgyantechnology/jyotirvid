<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Report";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_making_report');
?>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="UI-II ng-scope">
                    <div class="match-making-report">
                        <div class="grid">
                            <div class="grid-col-1 ">
                                <div class="blocks ashtakoot">
                                    <h3 class="text-center">Ashtakoot</h3>
                                    <div class="ashtakoot-points">
                                        <div class="ng-scope">
                                            <span class="ng-binding"><?=$data['ashtakoota']['received_points']?>/36</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-col-1">
                                <div class="blocks manglik">
                                    <h3 class="text-center">Manglik Match</h3>
                                    <div class="manglik-pointss">
                                    <?php
                                    if($data['manglik']['status'])
                                    {
                                    ?>
                                        YES
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                        NO
                                        <?php
                                    }
                                    ?>

                                    </div>
                                </div>
                            </div>
                            <div class="grid-col-1">
                                <div class="blocks rajjuu">
                                    <h3 class="text-center">Rajjoo Dosha</h3>
                                    <div class="rajjo-points">
                                        <div class="ng-scope">
                                            <?php
                                            if($data['rajju_dosha']['status'])
                                            {
                                                ?>
                                                YES
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                NO
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-col-1">
                                <div class="blocks vedh">
                                    <h3 class="text-center">Vedha Dosha</h3>
                                    <div class="vedh-points">
                                        <div>
                                            <?php
                                            if($data['vedha_dosha']['status'])
                                            {
                                                ?>
                                                YES
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                NO
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix spacer-40"></div>
                    <div class="matching-conculsion-status">
                        <h2>Conclusion</h2>
                        <div class="ashtakoot-conculsion-report">
                            <p class="ng-binding"><?=$data['conclusion']['match_report']?></p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-2"></div>

        </div>

    </div>
<?php require_once("../footer.php"); ?>