<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Basic Details";
require_once("../matching_header.php");

$api = new ApiCall();

$data1 = $api->matchingApiCall('match_birth_details');
$data = $api->matchingApiCall('match_astro_details');

?>
    <!--Place holder for the horoscope templates-->
    <div class="row">
        <div class="col-md-12">
            <?php
			if(count($data) > 0 || count($data1) > 0) {
			?>
            <div class="row">

                <div class="col-md-6">
                    <div class="UI-II zero-padding">
                        <section id="photos">
                            <img src='<?=$url;?>birth.png' alt='Birth Details' height="175">
                            <div id="overlay">
                                <p> Birth Details</p>
                            </div>
                        </section>
                        <div class="table-bordered table-responsive">
                            <table class="table  zero-margin">
                                <thead>
                                <tr>
                                    <th class="text-center text-primary">Male</th>
                                    <th class="text-center text-muted">Birth Details</th>
                                    <th class="text-center text-danger">Female</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['day']?>:<?=$data1['male_astro_details']['month']?>:<?=$data1['male_astro_details']['year']?></td>
                                    <td class="bold-text text-muted" align="center">Date of birth</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['day']?>:<?=$data1['female_astro_details']['month']?>:<?=$data1['female_astro_details']['year']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['hour']?>:<?=$data1['male_astro_details']['minute']?></td>
                                    <td class="bold-text text-muted" align="center">Birth Time</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['hour']?>:<?=$data1['female_astro_details']['minute']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['latitude']?></td>
                                    <td class="bold-text text-muted" align="center">Latitude</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['latitude']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['longitude']?></td>
                                    <td class="bold-text text-muted" align="center">Longitude</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['longitude']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['timezone']?></td>
                                    <td class="bold-text text-muted" align="center">Time Zone</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['timezone']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['sunrise']?></td>
                                    <td class="bold-text text-muted" align="center">Sunrise</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['sunrise']?></td>
                                </tr>
                                <tr>
                                    <td align="center" class="text-primary"><?=$data1['male_astro_details']['sunset']?></td>
                                    <td class="bold-text text-muted" align="center">Sunset</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['sunset']?></td>

                                </tr>
                                <tr>
                                    <td align="center " class="text-primary"><?=$data1['male_astro_details']['ayanamsha']?></td>
                                    <td class="bold-text text-muted" align="center">Ayanamsha</td>
                                    <td align="center" class="text-danger"><?=$data1['female_astro_details']['ayanamsha']?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="UI-II zero-padding">
                        <section id="photos">
                            <img src='<?=$url;?>astro.jpg' alt='Astro Details' height="175">
                            <div id="overlay">
                                <p> Basic Astro Details</p>
                            </div>
                        </section>
                        <div class="table-responsive table-bordered">
                            <table class="table  zero-margin">
                                <thead>
                                <tr>
                                    <th class="text-center text-primary">MALE</th>
                                    <th class="text-center text-muted">ASTRO DETAILS</th>
                                    <th class="text-center text-danger">FEMALE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td  class="text-center text-primary"><?=$data['male_astro_details']['Varna']?></td>
                                    <td class="text-center text-muted">Varna</td>
                                    <td  class="text-center text-danger"><?=$data['female_astro_details']['Varna']?></td>
                                </tr>
                                <tr>
                                    <td  class="text-center text-primary"><?=$data['male_astro_details']['Vashya']?></td>
                                    <td class="text-center text-muted">Vashya</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Vashya']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Yoni']?></td>
                                    <td class="text-center text-muted">Yoni</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Yoni']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Gan']?></td>
                                    <td  class="text-center text-muted">Gan</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Gan']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Nadi']?></td>
                                    <td class="text-center text-muted">Nadi</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Nadi']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['SignLord']?></td>
                                    <td class="text-center text-muted">SignLord</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['SignLord']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Naksahtra']?></td>
                                    <td class="text-center text-muted">Nakshatra</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Naksahtra']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['NaksahtraLord']?></td>
                                    <td class="text-center text-muted">Nakshatra Lord</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['NaksahtraLord']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Charan']?></td>
                                    <td class="text-center text-muted">Charan</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Charan']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Yog']?></td>
                                    <td class="text-center text-muted">Yog</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Yog']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Karan']?></td>
                                    <td class="text-center text-muted">Karan</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Karan']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['Tithi']?></td>
                                    <td class="text-center text-muted">Tithi</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['Tithi']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['yunja']?></td>
                                    <td class="text-center text-muted">Yunja</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['yunja']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['tatva']?></td>
                                    <td class="text-center text-muted">Tatva</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['tatva']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['name_alphabet']?></td>
                                    <td class="text-center text-muted">Name Alphabet</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['name_alphabet']?></td>
                                </tr>
                                <tr>
                                    <td class="text-center text-primary"><?=$data['male_astro_details']['paya']?></td>
                                    <td class="text-center text-muted">Paya</td>
                                    <td class="text-center text-danger"><?=$data['female_astro_details']['paya']?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <?php } else { ?>
            No Match Available, Try again later!
            <?php } ?>
        </div>
    </div>
<?php require_once("../footer.php"); ?>