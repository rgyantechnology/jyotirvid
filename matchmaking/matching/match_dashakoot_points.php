<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Ashtakoot Points";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_dashakoot_points');

?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <div class="UI-II zero-padding">
                    <section id="photos" class="match-ashtakoot-overlay">
                        <p style="top:15%"> Match Dashakoot Points</p>

                        <p style="top:25%"><?=$data['total']['received_points']?> / <?=$data['total']['total_points']?></p>

                    </section>
                    <div class="table-bordered table-responsive">
                        <table class="table cust-table zero-margin">
                            <thead>
                            <tr>
                                <th>Atribute</th>
                                <th>Male</th>
                                <th>Female</th>
                                <th>Outof</th>
                                <th>Received</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Dina</td>
                                <td class="ng-binding"><?=$data['dina']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['dina']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['dina']['total_points']?></td>
                                <td class="ng-binding"><?=$data['dina']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Gana</td>
                                <td class="ng-binding"><?=$data['gana']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['gana']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['gana']['total_points']?></td>
                                <td class="ng-binding"><?=$data['gana']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Yoni</td>
                                <td class="ng-binding"><?=$data['yoni']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['yoni']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['yoni']['total_points']?></td>
                                <td class="ng-binding"><?=$data['yoni']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Rashi</td>
                                <td class="ng-binding"><?=$data['rashi']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rashi']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rashi']['total_points']?></td>
                                <td class="ng-binding"><?=$data['rashi']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Rasyadhipati</td>
                                <td class="ng-binding"><?=$data['rasyadhipati']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rasyadhipati']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rasyadhipati']['total_points']?></td>
                                <td class="ng-binding"><?=$data['rasyadhipati']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Rajju</td>
                                <td class="ng-binding"><?=$data['rajju']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rajju']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['rajju']['total_points']?></td>
                                <td class="ng-binding"><?=$data['rajju']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Vedha</td>
                                <td class="ng-binding"><?=$data['vedha']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vedha']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vedha']['total_points']?></td>
                                <td class="ng-binding"><?=$data['vedha']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Vashya</td>
                                <td class="ng-binding"><?=$data['vashya']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vashya']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vashya']['total_points']?></td>
                                <td class="ng-binding"><?=$data['vashya']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Mahendra</td>
                                <td class="ng-binding"><?=$data['mahendra']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['mahendra']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['mahendra']['total_points']?></td>
                                <td class="ng-binding"><?=$data['mahendra']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>StreeDeergha</td>
                                <td class="ng-binding"><?=$data['streeDeergha']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['streeDeergha']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['streeDeergha']['total_points']?></td>
                                <td class="ng-binding"><?=$data['streeDeergha']['received_points']?></td>
                            </tr>
                            <tr style="background-color: #FF585A;color: #fff;">
                                <td>Total</td>
                                <td>-</td>
                                <td>-</td>
                                <td class="ng-binding"><?=$data['total']['total_points']?></td>
                                <td class="ng-binding"><?=$data['total']['received_points']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="UI-II zero-padding">
                    <section id="photos" class="match-ashtakoot-overlay">
                        <p style="position: relative;top:10%"> Conclusion</p>
                            <?php
                            if($data['total']['received_points'] >=18)
                            {
                                ?>
                                <i class="fa fa-thumbs-o-up ashtakoot-conclusion-thumb" ></i>
                                <?php
                            }
                            else
                            {
                                ?>
                                <i class="fa fa-thumbs-o-down ashtakoot-conclusion-thumb"></i>
                                <?php
                            }
                            ?>

                    </section>
                    <div class="ashtakoot-conclusion" style="padding: 10px;;">
                        <div class="ashtakoot-conculsion-report">
                            <?php
                            if($data['total']['received_points'] >=18)
                            {
                                ?>
                                The match has scored more than 18 points outs of 36 points.
                                Therefore, as per Dashakoot analysis, this is a good match.
                                <?php
                            }
                            else
                            {
                                ?>
                                The match has scored less than 18 points outs of 36 points.
                                Therefore, as per Dashakoot analysis, this is not a good match.
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php require_once("../footer.php"); ?>