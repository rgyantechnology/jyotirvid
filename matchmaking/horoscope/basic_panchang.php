<div id="panchang"></div>

<script type="text/x-handlebars-template" id="panchangTpl">
<div class="UI-II ng-scope">
    <div class="table-container">
        <table>
            <thead>
            <tr>
                <th class="text-center">Panchang Details</th>
                <th class="text-center">Values</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-center dark-td">Tithi</td>
                <td class="text-center ng-binding">{{tithi}}</td>
            </tr>
            <tr>
                <td class="text-center dark-td">Karan</td>
                <td class="text-center ng-binding">{{karan}}</td>
            </tr>
            <tr>
                <td class="text-center dark-td">Yog</td>
                <td class="text-center ng-binding">{{yog}}</td>
            </tr>
            <tr>
                <td class="text-center dark-td">Nakshatra</td>
                <td class="text-center ng-binding">{{nakshatra}}</td>
            </tr>
            <tr>
                <td class="text-center dark-td">Sunrise</td>
                <td class="text-center ng-binding">{{sunrise}}</td>
            </tr>
            <tr>
                <td class="text-center dark-td">Sunset</td>
                <td class="text-center ng-binding">{{sunset}}</td>
            </tr>

            </tbody>
        </table>
    </div>

</div>
</script>

<script type="text/javascript">

getPanchang();

        </script>
