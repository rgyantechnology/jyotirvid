<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Numerology Reports For You";
require_once("../header.php");

$api = new ApiCall();
$report = $api->numerologyApiCall('numero_report');
$lord = $api->numerologyApiCall('numero_fav_lord');
$vastu = $api->numerologyApiCall('numero_place_vastu');
$mantra = $api->numerologyApiCall('numero_fav_mantra');
$time = $api->numerologyApiCall('numero_fav_time');
$fast = $api->numerologyApiCall('numero_fasts_report');
?>

    <div class="row">
        <div class="col-md-6">
            <div class="UI-II">

                <h2><?=$report['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$report['description']?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II">
                <!--Fav Lord-->
                <h2><?=$lord['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$lord['description']?></p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="UI-II ng-scope">

                <!--Place and vastu-->
                <h2><?=$vastu['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$vastu['description']?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">

                <!--Fav Mantra-->
                <h2><?=$mantra['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$mantra['description']?></p>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">

                <!--Fav Time-->
                <h2><?=$time['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$time['description']?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <!--Fast Report-->
                <h2><?=$fast['title']?></h2>

                <div class="matching-conculsion-status">
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$fast['description']?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php require_once("../footer.php"); ?>