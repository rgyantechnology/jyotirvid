<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Daily Prediction";
require_once("../header.php");
$api = new ApiCall();
$data = $api->horoscopeApiCall('daily_nakshatra_prediction');
?>

<!--Place holder for the horoscope templates-->
<div class="row">
    <?php 
	if(count($data) > 0) {
	?>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2>Prediction Date <?=$data['prediction_date'];?></h2>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-5 text-center">
                        <div class="birth-sign-nak nak-star text-success  ng-binding" style="padding: 40px 0;font-size: 1.3em;"><?=$data['birth_moon_sign']?></div>
                        <p style="color: #DD2323;margin-top: 10px;font-size: 1.3em;">Moon Sign</p>
                    </div>
                    <div class="col-sm-6 col-sm-offset-1 text-center">
                        <div class="birth-sign-nak nak-star text-success ng-binding" style="padding: 40px 0"><?=$data['birth_moon_nakshatra']?></div>
                        <p style="color: #3f87dd;margin-top: 10px;font-size: 1.3em;">Nakshatra</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="UI-II ng-scope">
            <h2>Health</h2>
            <p class="ng-binding"><?=$data['prediction']['health']?></p>
        </div>
        <div class="UI-II ng-scope">
            <h2>Personal Life</h2>
            <p class="ng-binding"><?=$data['prediction']['personal_life']?></p>
        </div>
        <div class="UI-II ng-scope">
            <h2>Travel</h2>
            <p class="ng-binding"><?=$data['prediction']['travel']?></p>
        </div>

    </div>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2>Profession</h2>
            <p class="ng-binding"><?=$data['prediction']['profession']?></p>
        </div>
        <div class="UI-II ng-scope">
            <h2>Luck</h2>
            <p class="ng-binding"><?=$data['prediction']['luck']?></p>
        </div>
        <div class="UI-II ng-scope">
            <h2>Emotions</h2>
            <p class="ng-binding"><?=$data['prediction']['emotions']?></p>
        </div>
    </div>
    <?php } else { ?>
    	<div class="col-md-6">No Record Found, Please try again later!</div>
    <?php } ?>
</div>
<?php require_once("../footer.php"); ?>