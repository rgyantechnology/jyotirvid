<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Lal Kitab Remedies Details";
require_once("../header.php");
$planet = null;
$api = new ApiCall();

if(isset($_POST['planet'])){
    $planet = $_POST['planet'];
    $data = $api->horoscopeApiCall('lalkitab_remedies/'.$planet);
}
else
{
    $planet = "Sun";
    $data = $api->horoscopeApiCall('lalkitab_remedies/Sun');
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="UI-II ng-scope">
            <div class="myForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 col-md-offset-3" >
                            <form action="" method="post">
                                <h3 class="text-center">Select planet</h3>
                                <select id="planetId" name="planet" onchange="this.form.submit()">
                                    <option value="Sun" <?php if($planet == "Sun"){ echo " selected"; }?>>Lal Kitab Remedies For Sun</option>
                                    <option value="Moon" <?php if($planet == "Moon"){ echo " selected"; }?>>Lal Kitab Remedies For Moon</option>
                                    <option value="Mars" <?php if($planet == "Mars"){ echo " selected"; }?>>Lal Kitab Remedies For Mars</option>
                                    <option value="Mercury" <?php if($planet == "Mercury"){ echo " selected"; }?>>Lal Kitab Remedies For Mercury</option>
                                    <option value="Jupiter" <?php if($planet == "Jupiter"){ echo " selected"; }?>>Lal Kitab Remedies For Jupiter</option>
                                    <option value="Venus" <?php if($planet == "Venus"){ echo " selected"; }?>>Lal Kitab Remedies For Venus</option>
                                    <option value="Saturn" <?php if($planet == "Saturn"){ echo " selected"; }?>>Lal Kitab Remedies For Saturn</option>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clearfix spacer-30"></div>

                <h3>The planet <?=$data['planet']?> is situated in the <?=$data['house']?> house.</h3>
                <div class="ashtakoot-conculsion-report">

                    <?php
                    if(is_array($data['lal_kitab_desc']))
                    {

                    ?>
                        <h3>Benefic</h3>
                        <?php
                        for($i=0;$i<count($data['lal_kitab_desc']['Benefic']);$i++)
                        {
                        ?>
                            <p><?=$data['lal_kitab_desc']['Benefic'][$i]?></p>
                        <?php

                        }
                        ?>
                        <h3>Malefic</h3>
                        <?php
                        for($i=0;$i<count($data['lal_kitab_desc']['Malefic']);$i++)
                        {
                            ?>
                            <p><?=$data['lal_kitab_desc']['Malefic'][$i]?></p>
                            <?php

                        }
                        ?>

                    <?php

                    }
                    else
                    {
                    ?>
                        <p class="text-left ng-binding">

                            <?=$data['lal_kitab_desc']?>
                        </p>
                    <?php

                    }
                    ?>

                </div>

                <h3>Remedies</h3>
                <div class="ashtakoot-conculsion-report">
                    <?php
                    for($i=0;$i<count($data['lal_kitab_remedies']);$i++)
                    {
                        ?>
                        <p class="text-left ng-binding">- <?=$data['lal_kitab_remedies'][$i]?></p>
                        <?php
                    }
                    ?>

                </div>
            </div>


        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>