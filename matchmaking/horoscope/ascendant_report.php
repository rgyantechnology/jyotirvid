<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Basic Details";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('general_ascendant_report');

?>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="UI-II col-md-12 ng-scope" >

            <h2> Your Ascendant is <?=$data['asc_report']['ascendant']?></h2>
            <p style="line-height: 2.2em; color: #878787;" class="ng-binding"><?=$data['asc_report']['report']?></p>

        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<?php require_once("../footer.php"); ?>