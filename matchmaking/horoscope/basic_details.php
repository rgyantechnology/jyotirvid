<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Basic Details";
require_once("../header.php");

$api = new ApiCall();
$birth_data = $api->horoscopeApiCall('birth_details');
$astro_data = $api->horoscopeApiCall('astro_details');
$panchang_data = $api->horoscopeApiCall('basic_panchang/sunrise');
?>

<div class="row">
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2>Birth Details</h2>
            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Birth Details</th>
                        <th>Values</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="dark-td">Date</td>
                        <td class="ng-binding"><?=$birth_data['day'];?>/<?=$birth_data['month'];?>/<?=$birth_data['year'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Time</td>
                        <td class="ng-binding"><?=$birth_data['hour'];?> : <?=$birth_data['minute'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Latitude</td>
                        <td class="ng-binding"><?=$birth_data['latitude'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Longitude</td>
                        <td class="ng-binding"><?=$birth_data['longitude'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Timezone</td>
                        <td class="ng-binding"><?=$birth_data['timezone'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Sunrise</td>
                        <td class="ng-binding"><?=$birth_data['sunrise'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Sunset</td>
                        <td class="ng-binding"><?=$birth_data['sunset'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Ayanamsha</td>
                        <td class="ng-binding"><?=$birth_data['ayanamsha'];?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="UI-II ng-scope">
            <h2> Basic Panchang Details</h2>

            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Panchang Details</th>
                        <th>Values</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="dark-td">Tithi</td>
                        <td class="ng-binding"><?=$astro_data['Tithi'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Karan</td>
                        <td class="ng-binding"><?=$panchang_data['karan'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Yog</td>
                        <td class="ng-binding"><?=$panchang_data['yog'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Nakshatra</td>
                        <td class="ng-binding"><?=$panchang_data['nakshatra'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Sunrise</td>
                        <td class="ng-binding"><?=$panchang_data['sunrise'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Sunset</td>
                        <td class="ng-binding"><?=$panchang_data['sunset'];?></td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2 class="header-title m-b-md b-n"> Avakhada Details</h2>
            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Avakhasa Details</th>
                        <th>Values</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="dark-td">Varna</td>
                        <td class="ng-binding"><?=$astro_data['Varna'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Vashya</td>
                        <td class="ng-binding"><?=$astro_data['Vashya'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Yoni</td>
                        <td class="ng-binding"><?=$astro_data['Yoni'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Gan</td>
                        <td class="ng-binding"><?=$astro_data['Gan'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Nadi</td>
                        <td class="ng-binding"><?=$astro_data['Nadi'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Sign</td>
                        <td class="ng-binding"><?=$astro_data['sign'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">SignLord</td>
                        <td class="ng-binding"><?=$astro_data['SignLord'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Naksahtra -Charan</td>
                        <td class="ng-binding"><?=$astro_data['Naksahtra'];?>-<?=$astro_data['Charan'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Yog</td>
                        <td class="ng-binding"><?=$astro_data['Yog'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Karan</td>
                        <td class="ng-binding"><?=$astro_data['Karan'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Tithi</td>
                        <td class="ng-binding"><?=$astro_data['Tithi'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">yunja</td>
                        <td class="ng-binding"><?=$astro_data['yunja'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">tatva</td>
                        <td class="ng-binding"><?=$astro_data['tatva'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Name alphabet</td>
                        <td class="ng-binding"><?=$astro_data['name_alphabet'];?></td>
                    </tr>
                    <tr>
                        <td class="dark-td">Paya</td>
                        <td class="ng-binding"><?=$astro_data['paya'];?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>