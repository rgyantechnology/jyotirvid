<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Pitra Dosha Report";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('pitra_dosha_report');
?>
    <div class="UI-II ng-scope">
        <div class="col-sm-12">
            <div class="col-md-5"></div>
            <div class="kalsarpa-desc col-md-3">
                <h3>Present</h3>
                <?php
                if($data['is_pitri_dosha_present'])
                {
                    ?>
                    <p class="status-false">YES</p>
                    <?php
                }
                else
                {
                    ?>
                    <p class="status-true">NO</p>
                    <?php
                }
                ?>

            </div>
            <div class="col-md-4"></div>
            <div class="clearfix clear"></div>
        </div>

        <div class="matching-conculsion-status">
            <h2>Conclusion</h2>
            <div class="ashtakoot-conculsion-report">
                <p class="text-left ng-binding"><?=$data['conclusion']?></p>
            </div>

            <h2>What is Pitri Dosha ?</h2>
            <div class="ashtakoot-conculsion-report">
                <p class="text-left ng-binding"><?=$data['what_is_pitri_dosha']?></p>
            </div>
            <?php
                if($data['is_pitri_dosha_present'])
                {
            ?>
                    <p class="status-false">YES</p>
            <?php
                }
            ?>
            <?php
            if($data['effects'])
            {
                ?>
                <h2>Effects</h2>
                <div class="ashtakoot-conculsion-report">
                    <?php
                    for($i=0;$i<count($data['effects']);$i++)
                    {
                    ?>
                        <p class="text-left ng-binding"><?=$data['effects'][$i]?></p>
                    <?php
                    }
                    ?>

                </div>
                <?php
            }
            ?>
            <?php
            if($data['effects'])
            {
                ?>
                <h2>Remedies</h2>
                <div class="ashtakoot-conculsion-report">
                    <?php
                    for($i=0;$i<count($data['remedies']);$i++)
                    {
                        ?>
                        <p class="text-left ng-binding"><?=$data['remedies'][$i]?></p>
                        <?php
                    }
                    ?>

                </div>
                <?php
            }
            ?>


        </div>

    </div>
<?php require_once("../footer.php"); ?>