<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Vimshottari Dasha Details";
require_once("../header.php");

$api = new ApiCall();
$current_vdasha = $api->horoscopeApiCall('current_vdasha');
$major_vdasha = $api->horoscopeApiCall('major_vdasha');
?>


<div class="row">
    <div class="col-md-6">
        <div class="UI-II">
            <div class="Crnt-Dasha">
                <h2> Current Vimshottari Dasha</h2>

                <div class="dashaFirstlevelData dashaData">
                    <div class="dashaFirstLevel dashaPlanet pull-left ng-binding"><?=$current_vdasha['major']['planet']?></div>
                    <b>Mahadasha</b>
                    <p class="ng-binding"><?=$current_vdasha['major']['start']?> -- <?=$current_vdasha['major']['end']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaSecondlevelData dashaData">
                    <div class="dashaSecondLevel dashaPlanet ng-binding"><?=$current_vdasha['minor']['planet']?></div>

                    <b>Antar Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['minor']['start']?> -- <?=$current_vdasha['minor']['end']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaThirdlevelData dashaData">
                    <div class="dashaThirdLevel dashaPlanet ng-binding"><?=$current_vdasha['sub_minor']['planet']?></div>

                    <b>Pratyantar Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['sub_minor']['start']?> -- <?=$current_vdasha['sub_minor']['end']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaFourthlevelData dashaData">
                    <div class="dashaFourthLevel dashaPlanet ng-binding"><?=$current_vdasha['sub_sub_minor']['planet']?></div>

                    <b>Sookshma Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['sub_sub_minor']['start']?> -- <?=$current_vdasha['sub_sub_minor']['end']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaFifthlevelData dashaData">
                    <div class="dashaFifthLevel dashaPlanet ng-binding"><?=$current_vdasha['sub_sub_sub_minor']['planet']?></div>

                    <b>Pran Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['sub_sub_sub_minor']['start']?> -- <?=$current_vdasha['sub_sub_sub_minor']['start']?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2> Major Vimshottari Dasha </h2>
            <div class="table-bordered table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Planet</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $retro = " ";
                    for($i = 0; $i < count($major_vdasha); $i++)
                    {

                        echo '<tr>';
                        echo '<td class="bold">'.$major_vdasha[$i]['planet'].'</td>';
                        echo '<td>'.$major_vdasha[$i]['start'].'</td>';
                        echo '<td>'.$major_vdasha[$i]['end'].'</td>';
                        echo '</tr>';
                    }

                    ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>