<div id="majorVdasha"></div>

<script type="text/x-handlebars-template" id="major_vdashaTpl">
<div class="UI-II ng-scope">
    <h2> Major Vimshottari Dasha UI</h2>
    <table class="responstable">
        <tbody><tr>
            <th>Planet</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        {{#each majordasha}}
        <tr class="ng-scope">
            <td class="bold ng-binding">{{this.planet}}</td>
            <td class="ng-binding">{{this.start}}</td>
            <td class="ng-binding">{{this.end}}</td>
        </tr>
        {{/each}}
        </tbody></table>
</div>
</script>
<script type="text/javascript">

    majorVdasha();

</script>