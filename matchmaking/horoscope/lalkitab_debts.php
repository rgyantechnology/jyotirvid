<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Lal Kitab Debts";
require_once("../header.php");
$planet = null;
$api = new ApiCall();

$data = $api->horoscopeApiCall('lalkitab_debts');

?>
    <div class="UI-II ng-scope">
        <div class="matching-conculsion-status">

            <?php
            if($data) {
                for($i=0;$i<count($data);$i++)
                {
                ?>
                    <h2><?=$data[$i]['debt_name']?></h2>
                    <div class="ashtakoot-conculsion-report">
                        <h4><b>Indications</b></h4>
                        <p class="text-left ng-binding"><?=$data[$i]['indications']?></p>
                        <h4><b>Events</b></h4>
                        <p class="text-left ng-binding"><?=$data[$i]['events']?></p>
                    </div>
                <?php

                }
            ?>

            <?php
            }
            else
            {
            ?>
                <h2 class="status-true">Congratulation Your Horoscope Not Containing Any Debts</h2>
            <?php
            }
            ?>


        </div>
    </div>
<?php require_once("../footer.php"); ?>