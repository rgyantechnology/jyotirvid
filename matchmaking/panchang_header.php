<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$headerTitle;?></title>

    <!--Favicon-->
    <link rel="shortcut icon" href="">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo SITE_PATH; ?>css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo SITE_PATH; ?>css/sb-admin.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>css/style-ui.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>css/custom.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo SITE_PATH; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800' rel='stylesheet' type='text/css'>

</head>

<body style="background-color: #fff;">

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><!--<img src="" />-->Your brand Logo</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-nav navbar-right top-nav">
            <li class="dropdown">
                <a href="<?php echo SITE_PATH; ?>index.php">Back To Form</a>
            </li>

            <!--Place your links-->

            <!--<li class="dropdown">
                <a href=""></a>
            </li>
            <li class="dropdown">
                <a href=""></a>
            </li>
            <li class="dropdown">
                <a href="" class="nav-btn"></a>
            </li>-->

        </ul>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav horoscope-nave">
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'index.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>panchang/index.php">Panchang Details</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'tamil_panchang.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>panchang/tamil_panchang.php"> Tamil Panchang</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'planets_positions.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>panchang/planets_positions.php"> Planetary Positions </a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'chaughadiya_muhurta.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>panchang/chaughadiya_muhurta.php"> Chaughadiya Muhurta</a>
                </li>
                <li <?php if (strpos($_SERVER['PHP_SELF'], 'hora_muhurta.php')) echo 'class="active"';?>>
                    <a  href="<?php echo SITE_PATH; ?>panchang/hora_muhurta.php"> Hora Muhurta</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">
            <div class="se-pre-con display-none"></div>

            <div class="row">
                <div class="col-lg-12">