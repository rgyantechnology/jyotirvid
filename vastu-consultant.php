<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Vastu Consultant</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Vastu Consultant</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  

<div class="container content __web-inspector-hide-shortcut__">
    <div class="row">
      <div class="col-md-12">
        <div class="headin_link">
          <div class="row">
            <div class="col-md-12">
              <h3 class="pull-left">Vastu Shastra: Ancient Indian Architectural Science</h3>
              <h4 class="pull-right headin_size" class="pull-right"><a href="search-result.php?category=vastu-shastra">Get In Touch With Vastu Consultants</a></h4>
            </div>
          </div>
        </div>
          <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/vastu-veda.jpg">
                        <span class="p_text">Origin:</span> Vastu shastra is known as world's most sacred and powerful architectural science. This traditional Hindu architectural system is originated in ancient India during the vedic civilization. According to history; vastu shastra was flourished before 6000 BCE. The cities of great Harappa and Mohenjo-daro civilization developed on the ethics of vastu shastra. The art of Vastu originates in the Stapatya Veda, a part of the Atharva Veda.
                        <br><span class="p_text">Concept:</span> In the Vedic period, people built homes, temples and hermitages with a view to have a peaceful and spiritual living. So they developed an architectural technology on basis of four directions and nine grahas. The texts and scriptures of Vastu vidya describe principles of design, layout, measurements, ground preparation and space arrangement. 
                        <br>Vastu shastra is the knowledge of collection of ideas and concepts of an inherent energy of science, with or without the support of layout diagrams. The principles of vastu vidya is to combine all the five elements of nature and balance them with the man and the material. The five elements of nature is known as “Panch mahabhutas” in Hindu scriptures. They are earth, water, air, fire and space. 
                        <br><span class="p_text">Feng Shui:</span> Most popular traditional architectural science of south east Asia (China, Japan, Korea) Feng Shui is originated from concepts of Vastu shastra.
                        <br><span class="p_text">Feng Shui:</span> In the Mahabharata it is said a number of houses were built for the kings who were invited to the city Indraprastha for the Rajasuya Yagna of King Yuddhistira. Sage Vyasa says that these houses were as high as the peaks of Kailasa mountains, perhaps meaning that they stood tall and majestic. The houses were free from obstructions, had compounds with high walls and their doors were of uniform height and inlaid with numerous metal ornaments. It is said that the site plan of Ayodhya, the city of Lord Rama was similar to the plan found in the great architectural text Manasara. References are also to be found in Buddhist literature, of buildings constructed on the basis of Vastu. They contain references to individual buildings. Lord Buddha is said to have delivered discourses on architecture and even told his disciples that supervising the construction of a building was one of the duties of the order. Mention is made of monasteries (Vihar                          as) or temples, buildings which are partly residential and partly religious (Ardhayogas), residential storeyed buildings (Prasadas), multi-storeyed buildings (harmyas) and Guhas or residential buildings for middle class people. 
                        
                        
                        </p>
                        
                        
                         <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/history.jpg">
                        <span class="p_text">Science:</span> Is vastu is relevant to science ? The vastu science is based on laws of nature. The basic source of energy of the whole world is stored at North and South Pole. It flows uninterruptedly from North Pole to South Pole in the form of magnetic waves. The basic source of energy of the whole world is stored at North and South Pole. It flows uninterruptedly from North Pole to South Pole in the form of magnetic waves. Therefore, Southward portion of every building should be higher than the northward portion so that there may not be any hindrance to the flow of magnetic waves. There are lots of examples are there; so when you go with laws of nature; science stands down.
                        <br>Vastu vidya is not completely science prove. Although it never violate any rules defined by science. The science has no answers on “how the longest towers and giant stone temples of ancient India are survived thousands years and still not aged yet?”. So it is better to belief than questioning. 
                        <br><span class="p_text">Benefits:</span> We can’t count the benefits of Vastu; as advantages establishment of the five elements of the nature welcomes the path for enhanced health, wealth, prosperity and happiness for you and your family. It leads you to a successful life and keep you away from God’s wrath and other unnatural events. The positive energy of nature keeps minimizes all the negative energies from you and your belongings. The Vastu Shastra can really help people make best use of space. Vastu vidya is more than modern architecture as it connects you with nature not your money with material. 
                        <br>So next time you want to construct any thing special; do not forget to call a vastu shastra specialist. If you are unable to reach  the best vastu specialist nearby you; then find the best among the best by clicking here.
                        </p>
        </div>
               

      
      </div><!--/row-fluid-->
    </div>
  
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>