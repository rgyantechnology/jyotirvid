<?php
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
$error = array(); 
if(isset($_POST['submit']) && $_POST['submit']=='submit'){
if(isset($_POST['fname'])){
if(empty($_POST['fname'])){
$error['fname'] = "Please enter first name";	
}	
else if(!ctype_alpha($_POST['fname'])){
$error['fname'] = "Enter only alphabates.";	
}
else if(strlen($_POST['fname']) < 3){
$error['fname'] = "First name should be minimum 3 chareacters.";	
}else{
$fname = trim($_POST['fname']);	
}
}

if(isset($_POST['lname'])){
if(empty($_POST['lname'])){
$error['lname'] = "Please enter last name.";	
}	
else if(!ctype_alpha($_POST['lname'])){
$error['lname'] = "Enter only alphabates.";	
}else if(strlen($_POST['lname']) < 3){
$error['lname'] = "Last name should be minimum 3 chareacters.";	
}else{
$lname = trim($_POST['lname']);	
}	
}

if(isset($_POST['email'])){
if(empty($_POST['email'])){
$error['email'] = "Please enter email address.";	
}
else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === true){
$error['email'] = "Please enter valid email address.";	
}
else{
$email	= $_POST['email'];
}	
}

if(isset($_POST['mobile'])){
if(empty($_POST['mobile'])){
$error['mobile'] = "Please enter 10-digits mobile number.";
}
else if(!ctype_digit($_POST['mobile'])){
$error['mobile'] = "Please enter digits only.";	
}
else if(strlen($_POST['mobile']) < 10 ){
$error['mobile'] = "Please enter 10-digits mobile number.";	
}
else{
$mobile = $_POST['mobile'];	
}	
}

if(isset($_POST['message'])){
if(empty($_POST['message'])){
$error['message'] = "Please enter message.";	
}
else{
$message = $_POST['message'];	
}	
}
if(empty($error)){
	@extract($_POST);
	$path = '../upload/';
	if(empty($_FILES['fld_logo']['name'])){
	$message = "Please Upload Profile Picture."; 
	$filename = 'astro.jpg';
	}else{
	$fileext = explode('.',$_FILES['fld_logo']['name']); 
	}        
	if(!in_array($_FILES['fld_logo']['type'],array('image/jpg','image/jpeg','image/png'))){
	$message = "Please upload .";
	}
	else{
	$filename = uniqid().'_'.time().'_Astrologer_'.$_POST['fname'].'.'.  strtolower(end($fileext));
	$dest = $path.$filename;
	move_uploaded_file($_FILES['fld_logo']['tmp_name'], $dest);
	}

	$value = $_POST['fname'].' '.$_POST['lname'];
	$alias = createAlias($value);
	$date = date('y-m-d');
	$is_active = 0;
	$is_delete = 0;
	$is_type = 'web';
	$insert = $db->prepare("INSERT INTO ".TBLASTROLOGERS." (fname, lname, alias, faname, moname, qualification, experience, dob, gender, address1, address2, area, city, state, country, landmark, pincode, email, mobile, skypeid, facebookpage, consultancyfee, websitelink, image, is_active, is_delete, is_type, date ) 
	VALUES (:fname, :lname, :alias, :faname, :moname, :qualifications, :experience, :dob, :gender, :address1, :address2, :area, :city, :state, :country, :landmark, :pincode, :email, :mobile, :skypeid, :facebookpage, :consultancyfee, :websitelink, :image, :is_active, :is_delete, :is_type, :date)");
	$insert->bindParam(':fname', $fname);
	$insert->bindParam(':lname', $lname);
	$insert->bindParam(':alias', $alias);
	$insert->bindParam(':faname', $faname); 
	$insert->bindParam(':moname', $moname);
	$insert->bindParam(':qualifications', $qualification);
	$insert->bindParam(':experience', $experience);
	$insert->bindParam(':dob', $dob);
	$insert->bindParam(':gender', $gender);
	$insert->bindParam(':address1', $address1);
	$insert->bindParam(':address2', $address2);
	$insert->bindParam(':area', $area);
	$insert->bindParam(':city', $city);
	$insert->bindParam(':state', $state);
	$insert->bindParam(':country', $country);
	$insert->bindParam(':landmark', $landmark);
	$insert->bindParam(':pincode', $pincode);
	$insert->bindParam(':email', $email);
	$insert->bindParam(':mobile', $mobile);
	$insert->bindParam(':skypeid', $skypeid);
	$insert->bindParam(':facebookpage', $facebookpage);
	$insert->bindParam(':consultancyfee', $consultancyfee);
	$insert->bindParam(':websitelink', $websitelink);
	$insert->bindParam(':image', $filename);
	$insert->bindParam(':is_active', $is_active);
	$insert->bindParam(':is_delete', $is_delete);
	$insert->bindParam(':is_type', $is_type);
	$insert->bindParam(':date', $date);
	$astrologer = $insert->execute();
     if($astrologer){
		$id = $db->LastInsertId();
                $categoriesPost = $_POST['categoryAstro'];
                $catinfo = array(); 
				
				for($i = 0; $i < count($categoriesPost); $i++){
					if($categoriesPost[$i]== 8){
					 $othercatflag = 1;	
					}
				$insertCatQuery = $db->prepare('INSERT INTO astrologercategories(astrologer_id, category_id)VALUE(:astrologer, :category)');
				$insertCatQuery->bindParam(':astrologer', $id);
				$insertCatQuery->bindParam(':category', $categoriesPost[$i]);
				$insertCatQuery->execute();
               }
				 if($othercatflag == 1){
				 $category = 8;	 
				 $othercategory = $_POST['othercategory'];
				 $insertOtherCatQuery = $db->prepare('INSERT INTO categoryothers(category_id, otherinfo, astrologers_id)VALUE(:category, :otherinfo, :astrologer)');	
				 $insertOtherCatQuery->bindParam(':category', $category);
				 $insertOtherCatQuery->bindParam(':otherinfo', $othercategory);
				 $insertOtherCatQuery->bindParam(':astrologer', $id);
				 $insertOtherCatQuery->execute();		
				 }
				 
			$servicesPost = $_POST['serviceAstro'];
			for($i=0; $i < count($servicesPost); $i++){
			if($servicesPost[$i]== 7){
			$otherserviceflag = 1;	
			}
			$InsertServQuery = $db->prepare('INSERT INTO astrologerservices (astrologer_id, service_id)VALUE(:astrologer, :service)');
			$InsertServQuery->bindParam(':astrologer', $id);
			$InsertServQuery->bindParam(':service', $servicesPost[$i]);
			$InsertServQuery->execute();
            }
			if($otherserviceflag == 1){
			$service = 7;
			$otherinfo = $_POST['otherservice'];	
			$InsertOtherServQuery = $db->prepare('INSERT INTO serviceothers (service_id, otherinfo, astrologers_id)VALUE(:service, :otherinfo, :astrologer)');
			$InsertOtherServQuery->bindParam(':service', $service);
			$InsertOtherServQuery->bindParam(':otherinfo', $otherinfo);
			$InsertOtherServQuery->bindParam(':astrologer', $id);
			$InsertOtherServQuery->execute();	
			}
$to="support@rgyan.com";	
$subject="New Astrologer Registration";
$body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>New Astrologer Registration</title>
</head>
 <body>
 <p>
 Respected Sir / Ma\'am,<br />
 Pranam,<br />
 </p>
 
<p>We are honoured to have you as an associate of Religious Guardian Services Private Limited.</p>
 
<p>Our company Religious Guardian Services Private Limited, as its a knowledge centric organization, hosting one of its kinds of website "rgyan.com", we are providing premium on line information in field of Religious, Ritual, Spirituality, Festivals & Cultural Activities, Astrologers, Guru, Pandit and related information’s in line. Our motto is for spreading knowledge that is useful to all.</p>
 
<p>Rgyan offers the platform by which one can be omnipresent to its clients to offer a quality services.</p>
 
<p>Looking forward for a mutually benefiting relationship for time to come.</p>
 
<p>For any clarification do write to us at support@rgyan.com</p>
<p> 
With warm regards,<br />
Rgyan Team.<br />
</p>
</body>
</html>';

$subjectclient="New Astrologer Registration";
$bodyclient='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>New Astrologer Registration</title>
</head>
<body>
<p>
 Respected Sir / Ma\'am,<br />
 Pranam,<br />
 </p>
<p>We are honoured to have you as an associate of Religious Guardian Services Private Limited.</p>
 
<p>Our company Religious Guardian Services Private Limited, as its a knowledge centric organization, hosting one of its kinds of website "rgyan.com", we are providing premium on line information in field of Religious, Ritual, Spirituality, Festivals & Cultural Activities, Astrologers, Guru, Pandit and related information’s in line. Our motto is for spreading knowledge that is useful to all.</p>
 
<p>Rgyan offers the platform by which one can be omnipresent to its clients to offer a quality services.</p>
 
<p>Looking forward for a mutually benefiting relationship for time to come.</p>
 
<p>For any clarification do write to us at support@rgyan.com</p>
<p> 
With warm regards,<br />
Rgyan Team.<br />
</p>
</body>
</html>';
$headers = "From:" .$email. "\r\n";
$headers.= "Cc:prabhat@rgyan.com \r\n";
$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headersclient = "From:" .$to. "\r\n";
$headersclient.= "Cc:prabhat@rgyan.com \r\n";
$headersclient .= "Reply-To: ". strip_tags($to) . "\r\n";
$headersclient .= "MIME-Version: 1.0\r\n";
$headersclient .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($to, $subject, $body, $headers);
mail($email, $subjectclient, $bodyclient, $headersclient);
header('location:thankyou.php');
}       
}
else{
}
}
include_once('pages/head.php');
?>
<div id="main">
  <!--  Start Header Section-->
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  <!--Search Aera-->
  <div class="container">
  <script>
	function showothercategory(){
	if(document.getElementById("Category_8").checked == true)	{
	document.getElementById('CategoryOthersSpan').style.display='block';	
	}
	else{
	if(document.getElementById("Category_8").checked == false){	
	document.getElementById('CategoryOthersSpan').style.display='none';
	}	
	}
	}
	
	function showotherservice(){
	if(document.getElementById("Service_7").checked == true)	{
	document.getElementById('ServiceOthersSpan').style.display='block';	
	}
	else{
	if(document.getElementById("Service_7").checked == false){	
	document.getElementById('ServiceOthersSpan').style.display='none';
	}	
	}	
	}
  </script>
  
      <div class="form-section form_border">
        <div class="row">
          <form id="suggestSearch" name="suggestSearch" action="search-result.php" method="get" enctype="multipart/form-data" onsubmit="" onKeyPress="" >
            <div class="col-md-2">
              <div class="form-group form-group-padding ">
               <select data-placeholder="Select State" name="state" id="sstate" class="form-control"  style="width:180px; height:35px;" onchange="ajax_call_search('ajaxCall',{location_id:this.value,location_type:2}, 'city')" tabindex="2">
           <?php echo showstate(); ?>
          </select>
              </div>
            </div>
			
			<div class="col-md-2">
              <div class="form-group form-group-padding ">
                <select data-placeholder="Select City" name = "city" id="scity" class="form-control"  style="width:180px; height:35px;" tabindex="2">
            <?php echo showcity($city); ?>
          </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-padding">
                <label class="sr-only" for="location">Location</label>
                <input id="category" name="category" type="text" autocomplete="off" onBlur="if(this.value=='')this.value='Search Astrologer, Pandit and Dharmkathawachak'" onFocus="if(this.value=='Search Astrologer, Pandit and Dharmkathawachak')this.value=''" value="Search Astrologer, Pandit and Dharmkathawachak" class="form-control" />
              </div>
            </div>
            <!--<div class="col-md-2">
              <div class="form-group form-group-padding">
                <input class="form-control" type="datetime-local" value="" id="example-datetime-local-input" placeholder="Date">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-group-padding">
                <label class="sr-only" for="location">Location</label>
                <input class="form-control" type="time" value="13:45:00" id="example-time-input">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-group-padding">
                <input class="form-control" type="text" value="Consultation Fees" id="example-text-input">
              </div>
            </div>-->
            <div class="col-md-1">
              <button type="submit" class="btn btn-default btn-primary">Go</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Join as an Astrologer</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Join as an Astrologer</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  <!--registraion-->
  <div class="registraion_form">
    <div class="container">
    	<div class="well color_well">
        <legend class="text-center">Join as an Astrologer</legend>
    		<div class="row">
        		<div class="col-md-9 col-md-offset-1">
            		<form  id = "JoinAsAstrologer" method="post" action = "" class="form-horizontal">
                    <!--Personal Information Details-->
                        	<legend>Personal Information Details</legend>
                        	<div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="fname" id="fname" placeholder="First Name" class="form-control required lettersonly">
                                <span style = "color : red;"><?php echo $error['fname']?></span>
								</div>
                                <div class="col-md-4">
                                	<input type="text" name="lname" id ="lname"  placeholder="Last Name" class="form-control required lettersonly">
									<span style = "color : red;"><?php echo $error['lname']?></span>	
								</div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="faname" id="faname" placeholder="Father’s/Husband’s name" class="form-control required lettersonly">
									
								</div>
                                <div class="col-md-4">
                                	<input type="text" name="moname" placeholder="moname" class="form-control required lettersonly">
									
								</div>
                            </div>
                				
                                <div class="form-group">
            						<div class="col-md-4">
              							<input type="text" name = "dob"  id="dob" placeholder="Date Of Birth" class="form-control">
										
									</div>
                                    <div class="col-md-4">
              							<select type="text" name = "gender" id="gender" placeholder="Gender" class="form-control required">
               								 <option value="female">Female</option>
                								<option value="male" selected = "selected">Male</option>
              							</select>
            						</div>
          						</div>
                                
                                <div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="experience" id="experience" placeholder="Experience" class="form-control required lettersonly">
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="qualification" id="qualification" placeholder="Qualification(In Relevant Field)" class="form-control">
                                </div>
                            </div>
                                
                    		<!--Personal Information Details-->
                            <!-- Address Section -->
          					<legend>Address Details</legend>
                            	<div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="address1"  id = "address1" placeholder="Address Line 1" class="form-control required">
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="address2"  id = "address2" placeholder="Address Line 2" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="area" id = "area" placeholder="Area" class="form-control required">
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="landmark" id = "landmark" placeholder="Landmark" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
								<?php 
								$SelectCountry = $db->prepare("SELECT location_id, name FROM locations WHERE location_type = 0 ");
								$SelectCountry->execute();
								?>
								<select name="country" id="country" class="form-control" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:1}, 'state')">
								<option value="">Select Country</option>
								<?php 
								while($cres = $SelectCountry->fetch()){
								?>
								<option value="<?php echo $cres['location_id']?>" <?php if($cres['location_id'] == '100'){ ?> selected = "selected" <?php } ?>><?php echo $cres['name']?></option>
								<?php	
								}
								?>
								</select>
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="pincode" id = "pincode" placeholder="Pin Code" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
								<?php 
								$SelectState = $db->prepare("SELECT location_id, name FROM locations WHERE location_type = 1 AND parent_id = 100");
								$SelectState->execute();
								?>
								<select name="state" id="state" class="form-control" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:2}, 'city')" >
								<option value="">Select State</option>
								<?php 
								while($sres = $SelectState->fetch()){
								?>
								<option value="<?php echo $sres['location_id']?>"><?php echo $sres['name']?></option>
								<?php	
								}
								?>
								</select>
                                  </div>
                                <div class="col-md-4">
								<?php 
								$SelectCity = $db->prepare("SELECT location_id, name FROM locations WHERE location_type=2");
								$SelectCity->execute();

								?>
								<select name="city" id="city" class="form-control">
								<option value="">Select City</option>
								<?php 
								while($csres = $SelectCity->fetch()){
								?>
								<option value="<?php echo $csres['location_id']?>"> <?php echo $csres['name']?></option>
								<?php	
								}
								?>
								</select>
                                </div>
                            </div>
                            
                            <!--Contact Information-->
                            <legend>Contact Information</legend>
                            	<div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="email" id = "email" placeholder="email" class="form-control">
									<span style = "color : red;"><?php echo $error['email']?></span>
								</div>
                                <div class="col-md-4">
                                	<input type="text" name="mobile" id = "email" placeholder="Phone or Mobile" class="form-control">
                                    <span style = "color : red;"><?php echo $error['mobile']?></span>
								</div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="skypeid" placeholder="Skype Id" class="form-control">
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="websitelink"  id = "websitelink" placeholder="Website" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                            	<div class="col-md-4">
                                	<input type="text" name="consultancyfee" id = "consultancyfee" placeholder="Consultancy Fee" class="form-control">
                                </div>
                                <div class="col-md-4">
                                	<input type="text" name="facebookpage" id = "facebookpage" placeholder="Facebook Page" class="form-control">
                                </div>
                            </div>
                            <!--personal details-->
                            
                            
                            <legend>Contact Information</legend>
                            <div class="form-group">
                            	<div class="col-md-4">
                                <img class="img-responsive" src="img/person-icon.jpg">
                                	<label class="control-label cont_size" style = "color: #151414;">Uplod Photo</label>
									<input id="fld_logo" name="fld_logo" type="file" class="file cont_size1">
                                </div>
                                <div class="col-md-4">
                                	
                                </div>
                            </div>
                            
                             <legend>Select Categories</legend>
                            <div class="form-group">
                            	<div class="col-md-12">
								<?php 
								$celement = "";
								$selcatquery = $db->prepare("SELECT id, name FROM categories");
								$selcatquery->execute();
								while($catres = $selcatquery->fetch()){
								if($catres['id'] == 8){ $celement = 'showothercategory()';}else{ $celement = ""; }
								?>
								<label class="col_chang" style = "color: #151414;"><input type="checkbox" name="categoryAstro[]" , id="Category_<?php echo $catres['id']?>" value="<?php echo $catres['id'] ?>" onclick="<?php echo $celement ?>" ><?php echo $catres['name']?></label>
                                <?php 
								}
								?>
								</div>
                            </div>
                            
                            
                            <div class="form-group" id="CategoryOthersSpan" style="display:none;">
                            	<div class="col-md-6">
                                	<input type = "text" name="othercategory" id="othercategory" class="form-control">
                                </div>
                            </div>
                            
                            
                            <legend>Your Specializations</legend>
                            <div class="form-group">
                            	<div class="col-md-12">
								<?php 
								$selements = "";
								$selcasevquery = $db->prepare("SELECT id, name FROM services");
								$selcasevquery->execute();
								$servicequery = $db->prepare("SELECT service_id FROM astrologerservices WHERE astrologer_id = :astrologer") ;
								$servicequery->bindParam(':astrologer', $alias);
								$servicequery->execute();
								$servRes = $servicequery->fetchAll();
								$serviceotherquery = $db->prepare(" SELECT otherinfo, service_id FROM serviceothers WHERE astrologers_id = :astrologer");
								$serviceotherquery->bindParam(':astrologer', $alias); 
								$serviceotherquery->execute();
								$servotherres = $serviceotherquery->fetch();
								while($services = $selcasevquery->fetch()){
								if($services['id'] == 7){ $celement = 'showotherservice()';}else{ $celement = ""; }
								?>
                                <label class="col_chang" style = "color: #151414;"><input type="checkbox" name="serviceAstro[]" , id="Service_<?php echo $services['id']?>" value="<?php echo $services['id'] ?>" onclick="<?php echo $celement ?>" ><?php echo $services['name']?></label>
								<?php } ?>
								</div>
                            </div>
                            
                            
                            <div class="form-group" id="ServiceOthersSpan" style="display:none;">
                            	<div class="col-md-6">
                                	<input type="text" name="otherservice" id="otherservice" class="form-control">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                            	<div class="col-md-12">
               						<button type="submit" name= "submit" value = "submit" class="btn  btn-lg">Submit</button>
              					</div>
                            </div>
            		</form>
            	</div>
        	</div>
  		</div>
  	</div>
  </div>
  <!--end of registraion--> 
  <!--footer section start-->
 <?php include_once('pages/footer.php'); ?>