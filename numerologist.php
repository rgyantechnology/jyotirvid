<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Numerologist</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Numerologist</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  

<div class="container content __web-inspector-hide-shortcut__">
    <div class="row">
      <div class="col-md-12">
        <div class="headin_link">
          <div class="row">
            <div class="col-md-12">
              <h3 class="pull-left">Numerology&nbsp;:&nbsp;Science Of Number</h3>
              <h4 class="pull-right headin_size" class="pull-right"><a href="http://localhost/newastrology/search-result.php?category=numerology">Get In Touch With Numerologists</a></h4>
            </div>
          </div>
        </div>
        <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/planetary-deity.jpg"> <span class="p_text">Numerology:</span> is the science of exploring the powers of numbers and their effect on human beings. It is a belief in the divine, mystical relationship among the planets, numbers and alphabets. It is said that Planets, Numbers, and Alphabets emit Vibrations and these help in drawing pseudo scientific inferences from words, names and ideas. It is not an exact science but depends upon logical methods and relational interpretations based upon the information available in the scriptures about the planetary bodies. <br>
          <br>
          People who practice numerology are referred to as Numerologists. You can analyse almost anything in your life starting from human behaviour, destiny, temperament, intelligence, sexuality, spirituality and much more.<br>
          <br>
        </p>
        <p>It is said that every number has a corresponding planetary deity associated with it. These deities may work in harmony to create peace or happiness or work against each other to create conflict and suffering. Given below are a Table of Planets, Numbers, and Alphabets.</p>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>PLANET</th>
              <th>NUMBER</th>
              <th>ALPHABET</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Sun</td>
              <td>1</td>
              <td>A I J Q Y</td>
            </tr>
            <tr>
              <td>Moon</td>
              <td>2</td>
              <td>B K R</td>
            </tr>
            <tr>
              <td>Jupiter</td>
              <td>3</td>
              <td>C G L S</td>
            </tr>
            <tr>
              <td>Rahu Urnaus</td>
              <td>4</td>
              <td>D M T</td>
            </tr>
            <tr>
              <td>Mercury</td>
              <td>5</td>
              <td>E H N X</td>
            </tr>
            <tr>
              <td>Venus</td>
              <td>6</td>
              <td>U V W</td>
            </tr>
            <tr>
              <td>Ketu Neptune</td>
              <td>7</td>
              <td>O Z</td>
            </tr>
            <tr>
              <td>Saturn</td>
              <td>8</td>
              <td>P F </td>
            </tr>
            <tr>
              <td>Mars</td>
              <td>9</td>
              <td>No Alphabet</td>
            </tr>
          </tbody>
        </table>
        <p class="text-justify">According to Indian Numerology, each of us has three numbers that are the most relevant:<br>
          <br>
          <b>• Psychic number:</b> It defines what you really want to be and what defines your basic character. It is determined by considering the birth date of the person.<br>
          <b>• Destiny number:</b> This shows how you are viewed by the world depending on the vibration patterns of your past life. It is obtained by adding up the date, year and month of birth.<br>
          <b>• Name number:</b> This signifies your relationship with other people and you can have more than one name number if people call you with different names. </p>
        <p>According to the planetary deity associated, the general quality that may manifest in a human are:-</p>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>PLANET</th>
              <th>NUMBER</th>
              <th>QUALITIES</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Sun</td>
              <td>1</td>
              <td>Strong individuality, likes to stay in control,  authoritative,<br>
                freedom loving,   bright, intelligence, prefers luxuries and comforts of life.</td>
            </tr>
            <tr>
              <td>Moon</td>
              <td>2</td>
              <td>Peace loving, soft, gentle, supportive, emotional,<br>
                fluctuating, good looking, intuitive, attractive. </td>
            </tr>
            <tr>
              <td>Jupiter</td>
              <td>3</td>
              <td>Healthy, energetic, strong, rational, ambitious,<br>
                spiritual, scholarly, enlightened, disciplined.</td>
            </tr>
            <tr>
              <td>Rahu Urnaus</td>
              <td>4</td>
              <td>Rebellious, unpredictable, stubborn, aggressive,<br>
                moody, short tempered, secretive, impulsive.</td>
            </tr>
            <tr>
              <td>Mercury</td>
              <td>5</td>
              <td>Childish, flexible, free spirited, progressive, logical,<br>
                playful, adaptable, princely, shrewd, sensitive, and bright.</td>
            </tr>
            <tr>
              <td>Venus</td>
              <td>6</td>
              <td>Artistic, creative, sensuous, romantic,<br>
                soft spoken, gentle, inventive, friendly, organized.</td>
            </tr>
            <tr>
              <td>Ketu Neptune</td>
              <td>7</td>
              <td>Indecisive, disruptive, social, artistic, intuitive,<br>
                mystical, religious, insightful, creative, dreamy, nature loving.</td>
            </tr>
            <tr>
              <td>Saturn</td>
              <td>8</td>
              <td>Introverted, lonely, thoughtful, wise, harmful,<br>
                depressive, strong willed, caring, suppressive, protective, and radical. </td>
            </tr>
            <tr>
              <td>Mars</td>
              <td>9</td>
              <td>Egoistic, short tempered, war like, aggressive,<br>
                strong, rough, fighting, strong leadership, hard outside but soft inside.</td>
            </tr>
          </tbody>
        </table>
        <p><u>Conclusion</u><br>
          It is important to note that any conclusion drawn from the numerological practices are possibilities not certainties. Reason being, life is not just sum of numbers but there are many other factors which influence this.  One should always keep an open mind about the application and importance of numerology but should never depend upon it or succumb to superstition or delusion.</p>
      </div>
    </div>
    <!--/row-fluid--> 
  </div>
  <!--footer section start-->
   <?php include_once('pages/footer.php'); ?>