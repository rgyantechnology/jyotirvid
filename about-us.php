<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">About us</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">About us</li>
      </ul>
    </div>
  </div>
  
  <!--about us section-->
  <!--<div class="container"><div class="breadcrumbs-v3 img-v2 text-center">
			<div class="container">
				<h1 style="color:rgba(0, 0, 0, 0.58);">About Us</h1>
			</div>
		</div>
  </div>-->
  
  
  
  <div class="container content __web-inspector-hide-shortcut__">
			<div class="row">
				<div class="col-md-12">
					<h4>What is Rgyan.com?</h4>
                        <p class="text-justify" style="color:#fff">Rgyan.com is India's leading online information search and services provider website. 
                        We provide information on the Ritual, Religion, Astrologers, Guru, Pandit and other related 
                        useful information used in day to day life and a happy experience. We aggregate, search, 
                        link and provide information on Astrology, Daily Horoscope, mantra, Meaning etc 10000+ 
                        temples details to provide the most trustworthy information at one place.Rgyan.com is the brainchild 
                        of Debjit Patra - Founder, who launched it in 2016 and form the heart and soul of the company. 
                        Our team comprises of a passionate bunch of techies, product folks, content curators, 
                        marketing folks and designers. What binds us, you ask? Our love for 
                        Indian Religion and creating the best products in the Ritual and Religion Space.</p><br>
				</div>
               
               
				<div class="col-md-12">
					<h4>What it is NOT?</h4>
                        <p class="text-justify" style="color:#fff">We are not an seller of Books, Puja Material, Information, We 
                        don't sell you any material. In fact we don't sell anything at all. Since we don't sell anything we don't have a call centre either. 
                        If you have questions about any content need to contact us directly by the Link Contact us.</p><br>
				</div>
            
                
               
                <div class="col-md-12">
					<h4>What is "You must Know"?</h4>
                        <p class="text-justify" style="color:#fff">Religion is very vast in India and have different way of practice in different parts of World. We provide information on the religious, Ritual and Belief. 
                        We provide wide information on the Practice, Custom and Way of living.</p><br>
				</div>
              
                
                
                <div class="col-md-12">
					<h4>How is Rgyan.com different from other Religious portals or Puja, Spiritual Sites?</h4>
                        <p class="text-justify" style="color:#fff">Rgyan.com is portal for providing information, it neither sell any material nor it offer to sell any information. It is just the informative site to provide vide knowledge.
                        It is most comprehensive online research for Religious + Ritual + Belief + History</p><br>
				</div>
      
			</div><!--/row-fluid-->
		</div>
  
    <!--about us section-->
  
 
  
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>