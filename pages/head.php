<!doctype html>
<head>
<?php if($_SERVER['REQUEST_URI'] == '/astrologer/palmist.php'){ ?>
<title>The Art of Palm Reading and its Basics</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="The Art of palm reading characterizes a person’s personalities, fortune and future by analyzing his/her hands. It is known as palmistry or chiromancy." />
<meta name="keywords" content="astrologers, palmists, numerologists, numerology, online astrologers, best astrologers, online palmists, best palmists, top palmists, astrological services, palm readers online, palm predictors, appointment with a palmist, palm reading expert, palm reading services, palmist’s number, palmists by location, palmists in my city, palm reading experts" />
<meta property="og:title" content="Worried about your future? Make an expert read your palm lines" />
<meta name="og:description" content="Find the best and accurate palmist for yourself and book an appointment by one click . Get detailed and customized advice about your future or any other prediction related queries." />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php 
}else if($_SERVER['REQUEST_URI'] == '/astrologer/astrologer.php'){
	
?>
<title>Astrology – The science of Planets</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Astrology is a very minute study of the celestial bodies and their positioning. They provide information about personality, human affairs, and other terrestrial matters." />
<meta name="keywords" content="astrologers, palmists, numerologists, numerology, top astrologers, best astrologers, astrology experts,astrologers by location, online astrologers, astrologers in my city, astrologers by mane, astrologers by location, astrological services, best astrological services, online astrological services, frequent astrological services, astrological services by phone, online astrologer’s appointment, book appointment with an astrologer, search astrologers, astrologers directory, future predictors, astrologer’s number, astrologer’s address, astrologers list, best astrologer for me, palmists, numerologists, numerology" />
<meta property="og:title" content="Book an Appointment Now for Free" />
<meta name="og:description" content="Find an expert astrologer and get personalized predictions instantly to solve all your problems in your life." />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php	
}
else if($_SERVER['REQUEST_URI'] == '/astrologer/numerologist.php'){
?>
<title>Numerology Science Of Number</title>
<meta name="description" content="People who practice numerology are referred to as Numerologists." />
<meta name="keywords" content="astrologers, palmists, numerologists, numerology, online astrologers, best astrologers, astrology, astrological services, find numerologist online, numerology expert, best numerologist, appointment with astrologer, predictors, appointment with numerologist, numerologist by location, numerologist by name" />
<meta property="og:title" content="Find A Numerologist Online and book an appointment" />
<meta name="og:description" content="Know all about the numbers game in your life by just one click; join us today and have an appointment with the best numerologist at your location" />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php	
}
else if($_SERVER['REQUEST_URI'] == '/astrologer/tarot-card-reader.php'){
?>
<title>Sacred Indian Tarot Card Reading</title>
<meta name="description" content="The art of predicting future or finding solutions of unnatural events of life by playing tarot cards is well known as tarot card reading." />
<meta name="keywords" content="tarot card reader, online tarot card reader, tarot card reader by name, tarot card reader by location, astrologers, palmists, numerologists, numerology, online astrologers, best astrologers, astrology, astrological services,Professional tarot card reader" />
<meta property="og:title" content="Looking for Tort Card Reader & Online astrologer" />
<meta name="og:description" content="Book an appointment now for the most famous tarot card readers. Book these experts and get the accurate answers to your questions." />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php	
}
else if($_SERVER['REQUEST_URI'] == '/astrologer/vastu-consultant.php'){
?>
<title>Vastu Shastra: Ancient Indian Architectural Science</title>
<meta name="description" content="Vastu shastra is known as world's most sacred and powerful architectural science. This traditional Hindu architectural system is originated in ancient India during the vedic civilization." />
<meta name="keywords" content="vastu sastra, online vastu sastra, vastu sastra for home, vastu sastra for office, vastu sastra consultants, vastu sastra professionals, vastu sastra experts, vastu sastra specialists by name, vastu sastra specialist by location, book appointment with vastu sastra specialist, astrologers, palmists, numerologists, numerology, online astrologers, best astrologers, astrology, astrological services" />
<meta property="og:title" content="Make an appointment with best vastu sastra consultant" />
<meta name="og:description" content="Book a experienced consultant and get rid of any vastu related flaw. Vastu dosha removed by just one click. Hurry and grab the time of the most accurate vastu shastra advisers." />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php	
}else{
?>
<title>Rgyan Astrologer: Book an appointment with top astrologers.</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Rgyan Astrologer; a common directory of astrologers, pandits, numerologists, palmists, vastu specialists, tarot readers, psychic readers and more. Book an astrologer’s appointment online.." />
<meta name="keywords" content="astrologers, palmists, palmists online, appointment with palmist, palmistry services, numerologists, numerology, online numerologists, appointment with numerologist, tarot cared reader, tarot card reader online, top astrologers, best astrologers, astrology experts,astrologers by location, online astrologers, astrologers in my city, astrologers by mane, astrologers by location, astrological services, best astrological services, online astrological services, frequent astrological services, astrological services by phone, online astrologer’s appointment, book appointment with an astrologer, search astrologers, astrologers directory, future predictors, astrologer’s number, astrologer’s address, astrologers list, best astrologer for me" />
<meta property="og:title" content="Book an Appointment Now for Free" />
<meta name="og:description" content="Find an expert astrologer and get personalized predictions instantly to solve all your problems in your life." />
<meta property="og:url" content="https://rgyan.com/astrologer/" />
<meta property="og:image" content="" />
<?php	
}
?>
<meta name="Author" content="Rgyan" />
<meta name='copyright' content='Religious Guardian Services Pvt. Ltd.'>
<meta name="robots" content="index,follow"/>
<meta name="googlebot" content="index, follow" />
<meta name="YahooSeeker" content="index, follow" />
<meta name="msnbot" content="index, follow" />
<meta name="language" content="English">
<meta name='url' content='https://rgyan.com/astrologer/'>
<meta name='identifier-URL' content='https://rgyan.com/astrologer/'>
<meta name="robots" content="ALL"/>
<meta name='author' content='Devendar, support@rgyan.com'>
<meta name='reply-to' content='support@rgyan.com'>
<meta name='distribution' content='Global'>
<link rel="stylesheet" href="http://localhost/jyotirvid/css/bootstrap.min.css">
<link rel="stylesheet" href="http://localhost/jyotirvid/css/style.css">
<link rel="stylesheet" type="text/css" href="css/media.css">
<link rel="stylesheet" id="fontawesome-css" href="css/font-awesome_002.css" type="text/css" media="all">
<link rel="stylesheet" href="http://localhost/jyotirvid/css/font-awesome.min.css">
<link rel="stylesheet" href="http://localhost/jyotirvid/css/header.css">
<link rel="stylesheet" href="http://localhost/jyotirvid/css/footer.css">
<link href="http://localhost/jyotirvid/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://localhost/jyotirvid/chosen/chosen.css">
<link href="https://rgyan.com/img/favicon1.png" type="image/x-icon" width='100px'  rel="shortcut icon">
<style type="text/css" media="all">
/* fix rtl for demo */
.chosen-rtl .chosen-drop { left: -9000px; }
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83603389-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>