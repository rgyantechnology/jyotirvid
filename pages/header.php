<div class="header">
    <div class="container"> 
      <!-- Logo --> 
      <a class="logo" href="http://rgyan.com/astrologer/"> <img class="img-responsive" src="<?php echo LINK_URL_HOME ?>imges/logo.png" alt="Logo"> </a> 
      <!-- End Logo --> 
      
      <!-- Topbar -->
      <div class="topbar">
        <ul class="loginbar pull-right">
          <li><a href="https://www.facebook.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
          <li><a href="https://twitter.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
          <li><a href="https://plus.google.com/107364248809228833539/posts"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
          <li><a href="https://in.pinterest.com/rgyan0288/"><span class="socilsize"><span style="color:#FFF" class="fa fa-pinterest"></span></span></a></li>
          <li><a href="https://www.youtube.com/channel/UC6ttpPdbioJ2I_me6lJe4bg"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></span></a></li>
        </ul>
       <!-- <ul class="loginbar pull-right">
		  <li><a href="http://rgyan.com/"><i class="icon-home"></i></a></li> 
          <li><a href="page_login.html">Log&nbsp;In</a></li>
          <li class="topbar-devider" style="background-color:#ff9900;"></li>
          <li><a href="#">Sign&nbsp;Up</a>&nbsp;</li>
        </ul>-->
      </div>
      <!-- End Topbar --> 
      
      <!-- Toggle get grouped for better mobile display -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"><i class="icon-menu"></i> </span> </button>
      <!-- End Toggle --> 
    </div>
    <!--/end container--> 
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
      <div class="container">
        <ul class="nav navbar-nav">
          <!-- Home -->
          <?php echo navgation();?>
          <li class="active"><a href="<?php echo LINK_URL_HOME ?>join-as-astrologer.php">Join as an Astrologer</a></li>
        </ul>
      </div>
      <!--/end container--> 
    </div>
    <!--/navbar-collapse--> 
  </div>
  <!--end of header Section-->  