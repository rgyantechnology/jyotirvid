<!--footer section start-->
  <div class="footer-v1">
    <div class="footer">
      <div class="container">
        <div class="harimg"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>imges/astrologer2.png" alt=""></div>
        <div class="row"> 
          <!-- About -->
          <div class="col-md-3 media_scr"> <a href="<?php echo LINK_URL_HOME ?>"><img id="logo-footer" class="footer-logo" src="<?php echo LINK_URL_HOME ?>imges/footer_logo.png" alt=""></a>
            <p>Religious Guardian Services Private Limited is a knowledge centric organization, hosting one of its kinds of website "rgyan.com", we are providing premium online information services in field of Religious, Ritual, Spirituality, Festivals & Cultural Activities, Astrologers, Guru, Pandit and related information’s in line. Our motto is for spreading knowledge that is useful to everyone.</p>
          </div>
          <!--/col-md-3--> 
          <!-- End About -->
          <div class="col-md-1 media_scr1 divider_off "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Service List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Quick Links</h2>
            </div>
            <div class="link_font">
              <ul class="list-unstyled link-list">
                <li><a href="about-us.php">-&nbsp;About Us</a></li>
                <li><a href="terms-and-conditions.php">-&nbsp;Terms & Conditions</a></li>
                <li><a href="privacy-policy.php">-&nbsp;Privacy Policy</a></li>
                <li><a href="we-are-hiring.php">-&nbsp;We are hiring</a></li>
                <li><a href="disclaimer.php">-&nbsp;Disclaimer</a></li>
              </ul>
            </div>
          </div>
          <!--/col-md-3--> 
          <!-- End Link List -->
          <div class="col-md-1 media_scr1 divider_off"><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Services List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Services</h2>
            </div>
            <ul class="list-unstyled link-list">
              <li><a href="astrologer.php">-&nbsp;Astrologer</a></li>
              <li><a href="pandit.php">-&nbsp;Pandit</a></li>
              <li><a href="https://rgyan.com/horoscope/">-&nbsp;Online Horoscope</a></li>
             <!-- <li><a href="#">-&nbsp;Vedic Mantras</a></li>-->
            </ul>
          </div>
          <!--/col-md-3--> 
          <!-- End Services Link List -->
          <div class="col-md-1 divider_off media_scr1 "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Address -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Contact Us</h2>
            </div>
            <address class="md-margin-bottom-40">
            Religious Guardian Services Private Limited.<br />
            Plot No. 400P,(Basement) Sector 38,Gurugram(HR) 122001.  <br>
            Near Medanta Medicity <br>
            +91- 124 6588802 <br>
            <a href="#" class="">support@rgyan.com</a>
            </address>
          </div>
          <!--/col-md-3--> 
          <!-- End Address --> 
        </div>
      </div>
    </div>
    <!--/footer-->
    
    <div class="copyright">
      <div class="container">
        <div class="harimg1"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>imges/astrologer2.png" alt=""></div>
        <div class="row">
          <div class="col-md-6">
            <p style="color:#ff9900"> Copyright © <?php echo date('Y');?>, Rgyan. All Rights Reserved. 
              <!--<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>--> 
            </p>
          </div>
          
          <!-- Social Links -->
          <div class="col-md-6">
            <ul class="footer-socials list-inline">
              <li><a href="https://www.facebook.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
              <li><a href="https://twitter.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
              <li><a href="https://plus.google.com/107364248809228833539/posts"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
              <li><a href="https://in.pinterest.com/rgyan0288/"><span class="socilsize"><span style="color:#FFF" class="fa fa-pinterest"></span></span></a></li>
              <li><a href="https://www.youtube.com/channel/UC6ttpPdbioJ2I_me6lJe4bg"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></span></a></li>
            </ul>
          </div>
          <!-- End Social Links --> 
        </div>
      </div>
    </div>
    <!--/copyright--> 
  </div>
</div>
<!--end of main div--> 

<!-- JS Global Compulsory --> 
<script type="text/javascript" src="http://localhost/jyotirvid/js/jquery.min.js"></script>
<script type="text/javascript" src="http://localhost/jyotirvid/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="http://localhost/jyotirvid/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
// Change Your home URL..
home_url = 'http://localhost/jyotirvid/admin';

/* *
*     fileName - ajax file name to be called by ajax method.
*     data - pass the infromation(like location-id , location-type) via data variable.
*     loadDataToDiv - id of the div to which the ajax responce is to be loaded.
* */
function ajax_call(fileName,data, loadDataToDiv) {
jQuery("#"+loadDataToDiv).html('<option selected="selected">-- -- -- Loding Data -- -- --</option>');

//  If you are changing counrty, make the state and city fields blank
if(loadDataToDiv=='state'){
jQuery('#city').html('');
jQuery('#state').html('');                    
}
//  If you are changing state, make the city fields blank
if(loadDataToDiv=='city'){
jQuery('#city').html('');
}

jQuery.post(home_url + '/' + fileName + '.php', data, function(result) {
jQuery('#' + loadDataToDiv).html(result);
});
}

function ajax_call_search(fileName,data, loadDataToDiv) {
jQuery("#"+loadDataToDiv).html('<option selected="selected">-- -- -- Loding Data -- -- --</option>');

//  If you are changing counrty, make the state and city fields blank
if(loadDataToDiv=='state'){
jQuery('#scity').html('');
jQuery('#state').html('');                    
}
//  If you are changing state, make the city fields blank
if(loadDataToDiv=='city'){
jQuery('#scity').html('');
}

jQuery.post(home_url + '/' + fileName + '.php', data, function(result) {
jQuery('#' + loadDataToDiv).html(result);
});
}

function validate_form(id){
var name = document.getElementById('name'+id).value;
var email = document.getElementById('email'+id).value;
var mobile = document.getElementById('mobile'+id).value;
var letters = "/^[A-Za-z ]+$/";
var numbers = "/^[0-9]{10}+$/";
var emailcheck = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
var flag = 0;
if(name == ""){	
document.getElementById('error_name'+id).innerHTML = "Please enter your name.";
document.getElementById('error_name'+id).style.display="block";
}
else if(/^[A-Za-z ]+$/.test(name)){
flag = 1;
document.getElementById('error_name'+id).style.display = "none";

}
else{
flag = 0;
document.getElementById('error_name'+id).innerHTML = "Please enter only alphabate and space.";
document.getElementById('error_name'+id).style.display= "block";	
}

if(email == ""){
flag = 0;
document.getElementById('error_email'+id).innerHTML = "Please enter email id.";
document.getElementById('error_email'+id).style.display= "block";
}
else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
flag = 1;
document.getElementById('error_email'+id).style.display= "none";	
}
else{
flag = 0;
document.getElementById('error_email'+id).innerHTML = "Please enter vaild email id.";
document.getElementById('error_email'+id).style.display= "block";
}

if(mobile == ""){
flag = 0;
document.getElementById('error_mobile'+id).innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('error_mobile'+id).style.display= "block";	
}
else if(/^\d{10}$/.test(mobile)){
flag = 1;
document.getElementById('error_mobile'+id).style.display= "none";	
}
else{
flag = 0;
document.getElementById('error_mobile'+id).innerHTML = "Please enter 10-digits mobile number.";
document.getElementById('error_mobile'+id).style.display= "block";
}
if(flag === 1){
//alert('Prabhat');
dataString = $("#info_form"+id).serialize();
$.post("process.php", dataString, function(data) {
var jdata= JSON.parse(data);
document.getElementById('name'+id).value = "";
document.getElementById('email'+id).value = "";
document.getElementById('mobile'+id).value = "";
document.getElementById('message'+id).value = "";
document.getElementById('success'+id).innerHTML = jdata['success'];
return false;	
});	
}else{
	//alert('Tiwari');
return false;
}
}
</script>
<script type="text/javascript" src="http://localhost/jyotirvid/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="http://localhost/jyotirvid/js/jquery.autocomplete.pack.js"></script>
<script type="text/javascript" src="http://localhost/jyotirvid/js/script.js"></script>
</body>
</html>