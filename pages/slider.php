<!--slider section-->
  <div id="main_slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active"> <img class="" src="img/banner_1.jpg" alt="First slide"> </div>
        <div class="item"> <img class="" src="img/banner-_3.jpg" alt="Second slide"> </div>
        <div class="item"> <img class="img-responsive" src="img/banner-_2.jpg" alt="Third slide"> </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> </div>
    <div class="search">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-section">
              <div class="row">
                <form id="suggestSearch" name="suggestSearch" action="search-result.php" method="get" enctype="multipart/form-data" onsubmit="" onKeyPress="" >
                  <div class="col-xs-8 col-xs-offset-2 media_size">
                    <div class="input-group">
			<div class="input-group-btn search-panel">
            <select data-placeholder="Select State" name="state" id="state" class="form-control"  style="width:180px; height:35px;" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:2}, 'city')" tabindex="2">
           <?php echo showstate($state); ?>
          </select>
        </div>		
             <div class="input-group-btn search-panel">
            <select data-placeholder="Select City" name="city" id="city" class="form-control"  style="width:180px; height:35px;" onchange = "setcityinsession();" tabindex="2">
            <?php echo showcity($city); ?>
          </select>
        </div>
	  <input id="category" name="category" type="text" autocomplete="off" onBlur="if(this.value=='')this.value='Search Astrologer, Pandit and Dharmkathawachak'" onFocus="if(this.value=='Search Astrologer, Pandit and Dharmkathawachak')this.value=''" value="Search Astrologer, Pandit and Dharmkathawachak" class="form-control" size="20" />
	  <span class="input-group-btn">
	  <button class="btn btn-default btn-md btn_color btn1" type="submit"><span class="glyphicon glyphicon-search"></span></button>
	  </span> </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>