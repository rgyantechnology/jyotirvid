<?php 
include_once('admin/includes/function.php');
$condition = " WHERE astro.id = ".$_REQUEST['id']."  AND astro.is_active = 1 AND astro.is_delete = 0 HAVING astro.id";
$search = 'SELECT DISTINCT astro.*, group_concat(DISTINCT ca.name) AS astrocat, 
group_concat(DISTINCT se.name) AS astroserv FROM astrologers  AS astro
LEFT JOIN astrologercategories AS ac ON(ac.astrologer_id = astro.id)
INNER JOIN categories AS ca ON(ca.id = ac.category_id)
LEFT JOIN astrologerservices AS sa ON(sa.astrologer_id = astro.id)
INNER JOIN services AS se ON(se.id = sa.service_id)';
$sql = $search.$condition;
$searchquery = $db->prepare($sql);
$searchquery->execute();
$res = $searchquery->fetch();
?>
<!doctype html>
<head>
<title>Astrologer | <?php echo ucfirst($res['fname']).'&nbsp;'.ucfirst($res['lname']) ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/style.css">
<link href="<?php echo LINK_URL_HOME ?>css/review.css" rel="stylesheet">
<link href="<?php echo LINK_URL_HOME ?>css/mctabs.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo LINK_URL_HOME ?>css/media.css">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/header.css">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/footer.css">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/profile.css">
<link rel="stylesheet" id="fontawesome-css" href="<?php echo LINK_URL_HOME ?>css/font-awesome_002.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo LINK_URL_HOME ?>css/font-awesome.min.css">
<link href="<?php echo LINK_URL_HOME ?>css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="chosen/chosen.css">
<style type="text/css" media="all">
/* fix rtl for demo */
.chosen-rtl .chosen-drop { left: -9000px; }
</style>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<style>
.map {
	min-width: 300px;
	min-height: 300px;
	width: 100%;
	height: 100%;
}
.error{
	color:red;
}
</style>
</head>
<body>
<div id="main">
<?php include('pages/header.php')?>
  <!--end of header Section--> 
   <!--Search Aera-->
   <div class="container">
      <div class="form-section form_border">
        <div class="row">
          <form id="suggestSearch" name="suggestSearch" action="search-result.php" method="get" enctype="multipart/form-data" onsubmit="" onKeyPress="" >
            <div class="col-md-2">
              <div class="form-group form-group-padding ">
                <select data-placeholder="Select City" name = "city" class="chosen-select"  style="width:180px; height:35px;" tabindex="2">
            <?php echo showcity($city); ?>
          </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-padding">
                <label class="sr-only" for="location">Location</label>
                <input id="category" name="category" type="text" autocomplete="off" onBlur="if(this.value=='')this.value='Search Astrologer, Pandit and Dharmkathawachak'" onFocus="if(this.value=='Search Astrologer, Pandit and Dharmkathawachak')this.value=''" value="Search Astrologer, Pandit and Dharmkathawachak" class="form-control" />
              </div>
            </div>
            <!--<div class="col-md-2">
              <div class="form-group form-group-padding">
                <input class="form-control" type="datetime-local" value="" id="example-datetime-local-input" placeholder="Date">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-group-padding">
                <label class="sr-only" for="location">Location</label>
                <input class="form-control" type="time" value="13:45:00" id="example-time-input">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-group-padding">
                <input class="form-control" type="text" value="Consultation Fees" id="example-text-input">
              </div>
            </div>-->
            <div class="col-md-1">
              <button type="submit" class="btn btn-default btn-primary">Go</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <!--end of Search Aera--> 
  
  <!--breadcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Search Astrologer, Pandit and Dharmkathawachak</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="search-result.php">Astrologer</a></li>
        <li class="active"><?php echo ucfirst($res['fname']).'&nbsp;'.ucfirst($res['lname'])?></li>
      </ul>
    </div>
  </div>
  <!--end of bradcome--> 
  
  <!--profile secion-->
  
  <div class="container">
    <div class="box2">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2"><a href=""><?php if($res['image']){
				$path = 'upload/'.$res['image']; 
				if(file_exists($path)){ ?>
				<img class="img-thumbnail img-responsive" src="<?php echo LINK_URL_HOME ?>upload/<?php echo $res['image'] ?>" alt = "<?php echo $res['fname'].'&nbsp;'. $res['fname'];?>">
				<?php }  else{ ?><img class="img-thumbnail img-responsive" src="<?php echo LINK_URL_HOME ?>img/person-icon.jpg" alt = "<?php echo $res['fname'].'&nbsp;'. $res['fname'];?>"><?php }} ?></a> <br>
            <div class="btn_review"> <a class="btn-danger btn_padding" data-toggle="modal" data-target="#enquirypopup" href="#enquirypopup">Send Enquiry</a> <a class="btn-danger btn_padding" href="#WriteReviewHere">Write Review</a> </div>
          </div>
          <div class="col-md-7">
            <p><a href=""><span class="span_color"><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?>&nbsp;|&nbsp;Astrologer&nbsp;</span></a></p>
            <p><a href=""><span class="span_color_exp">Exp:&nbsp;<?php echo $res['experience']?>&nbsp;Years</span></a></p>
            <p><a href=""><span class="service_color">Services:&nbsp;</span><?php echo $res['astroserv'] ?></a></p>
            <p>
            <figcaption class="ratings">
              <p><span class="service_color">Ratings: </span> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> <a href="#"> <span class="fa fa-star-o"></span> </a> </p>
            </figcaption>
            <ul class="list-inline social_margin">
              <li><a href="#"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
              <li><a href="#"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
              <li><a href="#"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
              <!--<li><a href="#"><span class="socilsize"><i class="fa fa-pinterest-p" aria-hidden="true"></i></span></a></li>
                                          <li><a href="#"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></i></span></a></li>-->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel-footer">
          <div class="tabbable-panel">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active"><a href="#tab_default_1" data-toggle="tab"> About Me</a> </li>
                <!--<li> <a href="#tab_default_2" data-toggle="tab"> Best Deal</a> </li>-->
                <li> <a href="#tab_default_5" data-toggle="tab"> Contact Us</a> </li>
                <li> <a href="#tab_default_3" data-toggle="tab"> Reviews</a> </li>
                <li> <a href="#tab_default_4" data-toggle="tab"> Feed back</a> </li>
                
                <li> 
                  <!-- Trigger the modal with a button --> 
                  
                  <!-- Modal -->
                  <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog"> 
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Write new news feed</h4>
                        </div>
                        <div class="modal-body">
                          <p>Some text in the modal.</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btnbtn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1" active> <br>
                  <div class="row">
                    <div class="col-md-6">
                    <h4 class="col_heading4"><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></h4>
                      <?php echo str_replace('\r\n', '<br>', strip_tags($res['aboutus'], '<p>, <strong>, <span>, <br />'))?>
                      <h4 class="col_heading4">Services</h4>
                      <p><?php echo $res['astroserv'] ?></p>
                      
                       <h4 class="col_heading4">Timing & Availability</h4>
					   <?php if(!empty($res['timing'])){
						   ?>
                      
							<p><table width="55%" cellspacing="0" cellpadding="0" border="0" style="padding-left:5px;">
		  <?php 
		  
		  $Timing= explode('<br>', $res['timing']);
		  $Length=count($Timing);
		  for($i=0; $i<$Length;$i++)
		  {
		  	$Day=explode(':', $Timing[$i]);
			$DayName=$Day[0];
			?>
			<tr>
			<td><?php echo $DayName?></td>
			<td>:</td>
			<td><?php 
			$DayClosed=array("</b> MonClosed", "</b> TueClosed", "</b> WedClosed", "</b> ThrusClosed", "</b> FriClosed", "</b> SatClosed", "</b> SunClosed" );
			if($Day[1]=="</b> MonClosed" or $Day[1]=="</b> TueClosed" or $Day[1]=="</b> WedClosed" or $Day[1]=="</b> ThursClosed" or $Day[1]=="</b> FriClosed" or $Day[1]=="</b> SatClosed" or $Day[1]=="</b> SunClosed")
			{
				echo "Closed";
				
			}
			else{
					
				$time=explode('to', $Day[1]);
				if($time[0]=="</b> Open 24 Hours")
				{
					echo $time[0];
				}
				else{
				if($time[0]=="</b> ")
				{
					echo "Time is not specified by the Business Owner";
				}
				else{
				echo $time[0]."&nbsp;&nbsp;To&nbsp;&nbsp;".$time[1];
				}
				}
				
			}
			 ?></td>
			</tr>
		<?php
		 }
		  
		 ?></table>
		  <?php }?>
		</p>
                      
                      
                     <h4 class="col_heading4" id = 'WriteReviewHere'>Write Review Here</h4>
                      <div class="horizontal-row-1">
                        <div class="clinic-container">
                          <div id="review-bg-left">
                            <form name="reviewform" id = "reviewform" action="/princeac" method="post" enctype="multipart/form-data" onsubmit="return validateForm1();">
							<input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>>
							<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "REVIEW">
                              <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tbody>
                                  <tr>
                                    <td width="38%"><div class="category-left-heading">Write a Review</div></td>
                                    <td colspan="2" align="center"><div class="blue-txt" id="rateresult">Rate&nbsp;<strong><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></strong></div></td>
                                  </tr>
                                  <tr>
                                    <td valign="top" width="90%">Write a review for <strong><?php echo ucfirst(tep_db_output($res['fname'])).'&nbsp;'. ucfirst(tep_db_output($res['lname']));?></strong></td>
                                    <td width="19%" valign="top" style="text-align:right; color:#ff9900; font-size:18px; padding-right:8px; font-weight:bold;">Your Rating:</td>
                                    <td width="5%" align="right"><div id="rate-container-width">
                                        <div class="input select rating-c">
                                          <select id="example-c" name="rating" style="display: none;" class = "required">
                                            <option value=""></option>
                                            <option value="1"></option>
                                            <option value="2"></option>
                                            <option value="3"></option>
                                            <option value="4"></option>
                                            <option value="5"></option>
                                          </select>
                                        </div>
                                      </div></td>
                                  </tr>
                                </tbody>
                              </table>
                              <table width="100%" border="0" align="left">
                                <tbody>
                                  <tr>
                                    <td width="19%" align="left" valign="top" style="font-size:13px; font-weight:bold;">Description <span class="orange">*</span></td>
                                    <td width="81%"><textarea name="Description" cols="" rows="" class="review-description"></textarea></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2" align="left" valign="top" style="font-size:13px; font-weight:bold;"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tbody>
                                          <tr>
                                            <td width="19%" align="left" valign="top" style="font-size:13px; font-weight:bold;">Name <span class="orange">*</span></td>
                                            <td width="49%" height="20" valign="middle"><input name="Name" type="text" class="review-name required lettersonly"></td>
                                            <td width="33%" rowspan="3" valign="top"><table width="38%" border="0" align="center">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" valign="middle"><div id="user-profile"><img id="uploadPreview" src="imges/user-photo.jpg" width="88" height="80"></div></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table width="100%" border="0" align="left">
                                                <tbody>
                                                  <tr>
                                                    <td align="left"><div class="custom_file_upload"> 
                                                        <!--<input type="text" class="file" name="file_info">-->
                                                        <div class="file_upload">
                                                          <input type="file" id="file_upload" name="file_upload" onchange="PreviewImage();">
                                                        </div>
                                                      </div></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" style="font-size:13px; font-weight:bold;">Mobile <span class="orange">*</span></td>
                                            <td height="25" valign="middle"><input name="Mobile" type="text" class="review-name required number"></td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" style="font-size:13px; font-weight:bold;">Email <span class="orange">*</span></td>
                                            <td height="25" valign="middle"><input name="Email" type="text" class="review-name required email"></td>
                                          </tr>
                                          <tr>
                                            <td width="18%" height="45" valign="top">&nbsp;</td>
                                            <td width="49%" height="45" valign="middle">&nbsp;</td>
                                            <td width="33%" height="45" align="center" valign="bottom"><input type="hidden" name="rid" value="997"><br />
                                   
                                              <input name="Submit_Review" type="submit" value="Submit" class=" btn-sm  btn_size btn-primary center-block"></td>
                                              
                                          </tr>
                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                            </form>
                          </div>
						<script type="text/javascript">
                        function PreviewImage() {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("file_upload").files[0]);
                        
                        oFReader.onload = function (oFREvent) {
                        document.getElementById("uploadPreview").src = oFREvent.target.result;
                        };
                        };
                        </script> 
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="tab-pane" id="tab_default_2"> <br>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <div class="tab-pane" id="tab_default_3"> <br>
                  
                  
                  
                  					<div class="row">
<div class="col-sm-12">
<h3>User  Reviews</h3>
</div><!-- /col-sm-12 -->
</div><!-- /row -->
<div class="row">
<div class="col-sm-1">
<div class="thumbnail">
<img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /thumbnail -->
</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>S Ganesh</strong> <span class="text-muted pull-right">Posted On- 25-01-2015</span>
</div>
<div class="panel-body">
Panel content
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->


<div class="col-sm-1">
<div class="thumbnail">
<img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /thumbnail -->
</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>S Ganesh</strong> <span class="text-muted pull-right">Posted On- 25-01-2015</span>
</div>
<div class="panel-body">
Panel content
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->


<div class="col-sm-1">
<div class="thumbnail">
<img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /thumbnail -->
</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>S Ganesh</strong> <span class="text-muted pull-right">Posted On- 25-01-2015</span>
</div>
<div class="panel-body">
Panel content
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->

<div class="col-sm-1">
<div class="thumbnail">
<img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /thumbnail -->
</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>S Ganesh</strong> <span class="text-muted pull-right">Posted On- 25-01-2015</span>
</div>
<div class="panel-body">
Panel content
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->

</div>
</div>
                <div class="tab-pane" id="tab_default_5"> <br>
                  <div class="row">
                    <div class="col-md-6">
                      <div style="background-color: #201300;" class="well well-sm">
					  <p id = "contactussuccess" style = "display:none;"></p>
                        <form class="form-horizontal" method="post" id = "contactus" onsubmit = "return false;">
						<input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>>
						<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "CONTACTUS">
                          <fieldset>
                            <legend style="background:#999" class="text-center headerr">Contact us</legend>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="fname" name="fname" type="text" placeholder="First Name" label="true" class="form-control tab_form required lettersonly">
                                <span id = "fnameError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control tab_form required lettersonly">
                               <span id = "lnameError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control tab_form required email">
                              <span id = "emailError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="mobile" name="mobile" type="text" placeholder="Phone" class="form-control tab_form required number">
                              <span id = "mobileError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <textarea class="form-control tab_form required" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
								<span id = "messageError"></span>
							  </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12 text-center">
                                <button style="margin-bottom:47px;" type="submit"  onclick ="validate_enquiry();" class="btn btn-primary btn-lg">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div>
                        <div class="panel panel-default header_pannel">
                          <div class="text-center header_pad">Our Office</div>
                          <div class="panel-body text-center">
                            <h4>Address</h4>
                            <div><?php echo $res['address1']?>, <?php echo $res['address2']?><br />
                             <?php echo $res['area']?>, <?php echo $city = getcityname($res['city']);?><br />
							 <?php echo $state =  getstatename($res['state']); ?>- <?php echo $res['pincode'] ?>, <?php //echo $country =  getcountryname(100); ?> India <br />
							 </div>
                            <hr />
                            <div id="map1" class="map">
							<p class="img-thumbnail img-responsive" >
							<?php echo $res['mapembeddedcode']?>
							</p>
						</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="tab_default_4"> 
                  
                  <div class="Feed_back">
                  <div class="row">
                    <div class="col-md-6">
                      <div style="background-color: #201300;"class="well well-sm">
                        <form class="form-horizontal" method="post" id = "astrofeedback">
						<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "FEDDBACK">
                         <input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>> 
						  <fieldset>
                            <legend class="text-center headerr">Feed back</legend>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="fname" name="name" type="text" placeholder="First Name" class="form-control tab_form required lettersonly">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="lname" name="name" type="text" placeholder="Last Name" class="form-control tab_form required lettersonly">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control tab_form required email">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control tab_form required number">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10 col-md-offset-1">
                                <textarea class="form-control tab_form required" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                    </div>
                  </div>
                  
                  
                  
                  
                  <!--comment-->
                  <div class="row">
                    <div class="col-md-8"> <br>
                      <section class="comment-list"> 
                        <!-- First Comment -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow left">
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Second Comment Reply -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-9 col-sm-9">
                            <div class="panel panel-default arrow left">
                              <div class="panel-heading right">Reply</div>
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Third Comment -->
                        <article class="row">
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow right">
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                        <!-- Fourth Comment -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            <div class="panel panel-default arrow left">
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Fifth Comment -->
                        <article class="row">
                          <div class="col-md-10 col-sm-10">
                            <div class="panel panel-default arrow right">
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                        <!-- Sixth Comment Reply -->
                        <article class="row">
                          <div class="col-md-9 col-sm-9 col-md-offset-1 col-md-pull-1 col-sm-offset-0">
                            <div class="panel panel-default arrow right">
                              <div class="panel-heading">Reply</div>
                              <div class="panel-body">
                                <header class="text-right">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> Dec 16, 2014</time>
                                </header>
                                <div class="comment-post">
                                  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                                <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-md-pull-1 hidden-xs">
                            <figure class="thumbnail"> <img class="img-responsive" src="img/thumb.png">
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                        </article>
                      </section>
                    </div>
                  </div>
                  
                  <!--comment--> 
                  
                </div>
                <div class="tab-pane" id="tab_default_6">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
                <div class="tab-pane" id="tab_default_7">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
                <div class="tab-pane" id="tab_default_8">
                  <h1>ENTER TITLE CONTENT HERE</h1>
                  <p>Enter your images / paragraphhs here </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--more Astrologer-->
  <div class="more_astrologer">
    <div class="container">
      <div class="profile-body margin-bottom-20"> 
        <!--Profile Blog-->
        <div class="row margin-bottom-20">
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Mikel Andrews</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">12 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">54 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Natasha Kolnikova</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">37 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">46 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Natasha Kolnikova</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">37 Notifications&nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">46 Followers&nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share&nbsp;</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--/end row--> 
        <!--End Profile Blog--> 
        
        <!--Profile Blog-->
        <div class="row margin-bottom-20">
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Sasha Elli</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">3 Notifications&nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">25 Followers&nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share&nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Frank Heller</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">7 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">77 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 sm-margin-bottom-20">
            <div class="profile-blog"> <img class="rounded-x" src="img/thumb.png" alt="">
              <div class="name-location"> <strong>Frank Heller</strong> <span><i class="fa fa-map-marker"></i><a href="#">Gurugram,</a> <a href="#">Haryana</a></span> </div>
              <div class="clearfix margin-bottom-20"></div>
              <p>Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
              <hr>
              <ul class="list-inline share-list">
                <li><i class="fa fa-bell"></i><a href="#">7 Notifications &nbsp;</a></li>
                <li><i class="fa fa-group"></i><a href="#">77 Followers &nbsp;</a></li>
                <li><i class="fa fa-share"></i><a href="#">Share &nbsp;</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--/end row--> 
        <!--End Profile Blog-->
        <button type="button" class="btn-u btn-u-default btn-block text-center">Load More</button>
        <!--End Profile Blog--> 
      </div>
    </div>
  </div>
  <!--more Astrologer--> 
  
  <!--footer section start-->
  <div class="clearfix"></div>
  <div class="footer-v1">
    <div class="footer">
      <div class="container">
        <div class="harimg"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>imges/astrologer2.png" alt=""></div>
        <div class="row"> 
          <!-- About -->
          <div class="col-md-3 media_scr"> <a href="<?php echo LINK_URL_HOME ?>"><img id="logo-footer" class="footer-logo" src="<?php echo LINK_URL_HOME ?>imges/footer_logo.png" alt=""></a>
            <p>Rgyan.com is India's leading online information search and services provider website.
              We provide information on the Ritual, Religion, Astrologers, Guru, Pandit and other related useful information used in day to day life. 
          </div>
          <!--/col-md-3--> 
          <!-- End About -->
          <div class="col-md-1 media_scr1 divider_off "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Service List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Quick Links</h2>
            </div>
            <div class="link_font">
              <ul class="list-unstyled link-list">
                <li><a href="https://rgyan.com/page/about">-&nbsp;About Us</a></li>
                <li><a href="https://rgyan.com/page/innercontent/Terms-and-Coditions">-&nbsp;Terms & Conditions</a></li>
                <li><a href="https://rgyan.com/page/innercontent/Privacy-Policy">-&nbsp;Privacy Policy</a></li>
                <li><a href="https://rgyan.com/page/innercontent/We-are-Hiring">-&nbsp;We are hiring</a></li>
                <li><a href="https://rgyan.com/page/innercontent/Disclaimer">-&nbsp;Disclaimer</a></li>
              </ul>
            </div>
          </div>
          <!--/col-md-3--> 
          <!-- End Link List -->
          <div class="col-md-1 media_scr1 divider_off"><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Services List -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Services</h2>
            </div>
            <ul class="list-unstyled link-list">
              <li><a href="#">-&nbsp;Astrologer</a></li>
              <li><a href="#">-&nbsp;Pandit</a></li>
              <li><a href="#">-&nbsp;Online Horoscope</a></li>
              <li><a href="#">-&nbsp;Vedic Mantras</a></li>
            </ul>
          </div>
          <!--/col-md-3--> 
          <!-- End Services Link List -->
          <div class="col-md-1 divider_off media_scr1 "><img src="<?php echo LINK_URL_HOME ?>imges/astrologer.png" height="180" alt=""></div>
          <hr class="hr_size1">
          <!-- Address -->
          <div class="col-md-2 media_scr">
            <div class="headline">
              <h2>Contact Us</h2>
            </div>
            <address class="md-margin-bottom-40">
            Plot No. 4OOP, Sector 38, <br>
            Near Medanta, Gurugram(HR) 122001. <br>
            <br>
            +91- 124 2381700 <br>
            <a href="#" class="">support@rgyan.com</a>
            </address>
          </div>
          <!--/col-md-3--> 
          <!-- End Address --> 
        </div>
      </div>
    </div>
    <!--/footer-->
    
    <div class="copyright">
      <div class="container">
        <div class="harimg1"><img class="img-responsive" src="imges/astrologer2.png" alt=""></div>
        <div class="row">
          <div class="col-md-6">
            <p style="color:#ff9900"> Copyright © 2016, Rgyan. All Rights Reserved. 
              <!--<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>--> 
            </p>
          </div>
          
          <!-- Social Links -->
          <div class="col-md-6">
            <ul class="footer-socials list-inline">
              <li><a href="https://www.facebook.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
              <li><a href="https://twitter.com/RgyanIndia/"><span class="socilsize"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
              <li><a href="https://plus.google.com/107364248809228833539/posts"><span class="socilsize"><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
              <li><a href="https://in.pinterest.com/rgyan0288/"><span class="socilsize"><span style="color:#FFF" class="fa fa-pinterest"></span></span></a></li>
              <li><a href="https://www.youtube.com/channel/UC6ttpPdbioJ2I_me6lJe4bg"><span class="socilsize"><i class="fa fa-youtube" aria-hidden="true"></i></span></a></li>
            </ul>
          </div>
          <!-- End Social Links --> 
        </div>
      </div>
    </div>
    <!--/copyright--> 
  </div>
</div>

<!--end of profile secion--> 

<div id="enquirypopup" class="modal fade in" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content row">
				<div class="modal-header custom-modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Book an Appointment</h4>
				</div>
				<div class="modal-body">
					<form name="info_form" id="info_form" class="form-inline" action="#" method="post">
					<input id="astrologer_id" name="astrologer_id" type="hidden" placeholder="Phone" class="form-control tab_form" value = <?php echo $res['id']?>>
					<input id="formflag" name="formflag" type="hidden" placeholder="Phone" class="form-control tab_form" value = "BOOKAPPOINTMENT">
						<div class="form-group col-sm-12 col-xs-12">
							<input type="text" class="form-control required lettersonly" name="name" id="name" placeholder="Enter Name">
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<input type="email" class="form-control required email" name="email" id="email" placeholder="Enter Email">
						</div>
                        
						<div class="form-group col-sm-12 col-xs-12">
							<input type="text" class="form-control required number" name="mobile" id="phone" placeholder="Enter Phone">
						</div>
						
						<div class="form-group col-sm-12 col-xs-12">
							<textarea class="form-control required" id="message" name="message" rows="4" placeholder="Enter Message"></textarea>
						</div>
                        
                        <div class="form-group col-sm-12 col-xs-12">
                  			<input id="filebutton" name="filebutton" class="input-file" type="file">
                  		</div>
                    
                  
           
                        
						<div class="form-group col-sm-12 ">
							<button type="submit" class="btn btn-default pull-right">Submit</button>
						</div>
					</form>
				</div>
				
			</div>
			
		</div>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo LINK_URL_HOME ?>chosen/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
var config = {
'.chosen-select'           : {},
'.chosen-select-deselect'  : {allow_single_deselect:true},
'.chosen-select-no-single' : {disable_search_threshold:10},
'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
'.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
$(selector).chosen(config[selector]);
}
</script>

<script type = "text/javascript">
function validate_enquiry(){
$.post("process.php", $("#contactus").serialize(), function(data) {
	document.getElementById('contsuccess').innerHTML = data;
 });	
}

function validate_bookappointment(){
$.post("process.php", $("#info_form").serialize(), function(data) {
	var jdata= JSON.parse(data);
       document.getElementById('contsuccess').innerHTML = data;
		
    });	
}

function validate_rating(){
$.post("process.php", $("#reviewform").serialize(), function(data) {
document.getElementById('ratingsuccess').innerHTML = data;	
});	
}

function validate_feedback(){
$.post("process.php", $("#astrofeedback").serialize(), function(data) {
document.getElementById('feedbacksuccess').innerHTML = data;	
    });	
}
</script>

<script type="text/javascript" src="<?php echo LINK_URL_ADMIN ?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "Letters only please"); 
$(document).ready(function(){ 
$("#info_form").validate({ });
$("#contactus").validate({ });
$("#reviewform").validate({ });
$("#astrofeedback").validate({ });
});
</script>
<script type="text/javascript" src="<?php echo LINK_URL_HOME ?>js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="<?php echo LINK_URL_HOME ?>js/jquery.autocomplete.pack.js"></script>
<script type="text/javascript" src="<?php echo LINK_URL_HOME ?>js/script.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript">
        $(function () {
           
                

                $('#example-b').barrating('show', {
                    readonly:true
                });

                $('#example-c, #example-d').barrating('show', {
                    showValues:true,
                    showSelectedRating:false, onSelect:function(value, text) {
                        if(value==1)
						{
						document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#ff9900'>Very Bad</font></strong>";
						}
						else{
								if(value==2)
								{
								document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#ff9900'>Average</font></strong>";	
								}
								else{
								
										if(value==3)
										{
										document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#ff9900'>Good</font></strong>";
										}
										else{
												if(value==4)
												{
													
												document.getElementById('rateresult').innerHTML="You are Rating this as  <strong><font color='#ff9900'>Great</font></strong>";
												}
												else{
														if(value==5)
														{
															document.getElementById('rateresult').innerHTML="You are Rating this as <strong><font color='#ff9900'>Excellent</font></strong>";	
														}
														else{
														
														
														}
												}
										
										}
								
								}
						
						}
                    }
                });

                $('#example-e').barrating('show', {
                    initialRating:'A',                    
                    showValues:true,
                    showSelectedRating:false,
                    onSelect:function(value, text) {
                        alert('Selected rating: ' + value);
                    }
                });

               $('#example-f').barrating('show', {showSelectedRating:false});

                $('#example-g').barrating('show', {
                    showSelectedRating:true,
                    reverse:true
                });

  });
</script> 
<script src="js/jquery.barrating.js"></script>
</body>
</html>