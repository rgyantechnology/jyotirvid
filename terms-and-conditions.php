<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 

  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Terms & Conditions</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Terms & Conditions</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  <!--Terms & Conditions section-->
 <!-- <div class="container"><div class="breadcrumbs-v3 img-v1 text-center">
			<div class="container">
				<h1 style="color:rgba(0, 0, 0, 0.58);">Terms & Conditions</h1>
			</div>
		</div>
  </div>-->
  
  
  
  <div class="container content __web-inspector-hide-shortcut__">
			<div class="row">
				<div class="col-md-12">
					<h4> This is a Contract</h4>
                        <p class="text-justify" style="color:#fff">This (including any modification/alteration/change/deletion in this Agreement from time to time by Rgyan.com) is a Contract between you and Rgyan.com</p>
				</div>
               
               
				<div class="col-md-12">
					<h4> Free Consent</h4>
                        <p class="text-justify" style="color:#fff">You state that the very fact that you are visiting / have visited the Rgyan.com is a complete unqualified acceptance of yours relating to various terms and conditions detailed in the website as also in the disclaimer section of this Agreement. You fully declare and undertake with a sound mind without any undue force, pressure, influence or coercion on you, that you shall be fully bound by the terms of the website as also by the terms of the disclaimer. You further undertake that in case any dispute arises out of and relating to the non-performances of any obligations as a visitor of this Rgyan.com website or as a seeker or querist of opinion, advice or consultancy from the services of the Rgyan.com website or on account of non-payment of any dues in respect of the services rendered under this Agreement then the exclusive jurisdiction to file law suits against you, being the visitor or querist, shall be in the courts at New Delhi only.</p>
				</div>
            
                
               
                <div class="col-md-12">
					<h4>Eligibility</h4>
                        <p class="text-justify" style="color:#fff">Membership to the Site is void where prohibited. For using this site you must be eligible to enter into contract as per the applicable laws of India and country of your residence. Your use of this Rgyan.com represents and warrants that you have the right, authority, and capacity to enter into this Agreement and to abide by all of the terms and conditions of this Agreement.<br>
You agree to inform Rgyan.com your subsequent disability to enter into any contract.
Subsequent disability shall entitle Rgyan.com to restrict/change/terminate this agreement at its own discretion.</p>
				</div>
              
                
                
                <div class="col-md-12">
					<h4>Term &amp; Termination</h4>
                        <p class="text-justify" style="color:#fff">This Agreement will remain in full force and effect while you use the Site and/or are a Member of Rgyan.com. You may terminate your membership at any time, for any reason by informing Rgyan.com in writing to terminate your Membership. In the event you terminate your membership, you will not be entitled to a refund of any unutilized subscription fees or access to digital content or reports purchased through Rgyan.com. Rgyan.com may terminate your access to the Site and/or your membership for any reason which shall be effective upon sending notice of termination to you at the email address you provide in your application for membership or such other email address as you may later provide to Rgyan.com. If Rgyan.com terminates your membership because of your breaching the Agreement, you will not be entitled to any refund of any unused Subscription fees or access to digital content or reports purchased by you. Even after this Agreement is terminated, certain provisions will remain in effect including sections 5-12, inclusive, of this Agreement.<br>
Rgyan.com reserves its right to change the Service or delete features at any time and for any reason and to cancel or suspend your Service at any time. Such cancellation or suspension may be without cause and/or without notice. In case of service cancellation, your right to use the Service stops right away. Once the Service is cancelled or suspended, any data you may have stored on the Service may not be retrieved later.
If we cancel the Service in its entirety without cause, then we will refund to you, on a pro-rata basis the amount of your payment corresponding to the portion of your Service remaining right before such cancellation.</p>
				</div>

				<div class="col-md-12">
					<h4>Non-Commercial Use by Members</h4>
                        <p class="text-justify" style="color:#fff">The Rgyan.com Site is for the personal use of individual members only, and may not be used in connection with any commercial endeavors unless registered and approved as an 'Affiliate' of the Site. This includes providing links to other websites, whether deemed competitive to Rgyan.com or otherwise. Illegal and/or unauthorized uses of the Site, including unauthorized framing of or linking to the Site will be investigated, and appropriate legal action will be taken, including without limitation, civil, criminal, and injunctive redress.Rgyan.com shall be entitled to recover from you, your successors, heirs, assigns etc any profit, revenue, benefit etc which you have gained/received, expected or going to receive with penalty @ 50% of such profit/revenue/benefit by unauthorized use of this site.</p>
				</div>


				<div class="col-md-12">
					<h4>Other Terms of Use by Members</h4>
                        <p class="text-justify" style="color:#fff">You may not engage in advertising to, or solicitation of, other Members to buy or sell any products or services through the Service unless accepted as an Expert or as an Authorized Vendor by the Site. You will not transmit any chain letters or junk email to other Rgyan.com Members. Rgyan.com reserves the right to screen messages that you may send to Experts.
You may not use any automated processes, including IRC Bots, EXE's, CGI or any other programs/scripts to view content on or communicate/contact/respond/interact with Rgyan.com and/or its Members.</p>
				</div>


				<div class="col-md-12">
					<h4>Content Posted on the Site</h4>
                        <p class="text-justify" style="color:#fff">Rgyan.com owns and retains all proprietary rights, including without limitation, all intellectual property rights in the Rgyan.com Site and the Rgyan.com Service. The Site contains the copyrighted material, trademarks, and other proprietary information of Rgyan.com, and its licensors. Except for that information which is in the public domain or for which you have been given express permission by Rgyan.com, you may not copy, modify, publish, transmit, distribute, perform, display, or sell any such proprietary information. All lawful, legal and non-objectionable messages (in the sole discretion of Rgyan.com), content and/or other information, content or material that you post on the forum boards shall become the property of Rgyan.com. Rgyan.com reserves the right to scrutinize all such information, content and/or material posted on the forum boards and shall have the exclusive right to either remove, edit and/or display such information, material and/or content.<br>
You must use the Rgyan.com Service in a manner consistent with any and all applicable local, state, federal laws and regulations.</p>
				</div>


				<div class="col-md-12">
					<h4>Copyright Policy</h4>
                        <p class="text-justify" style="color:#fff">ou can not post, distribute, or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining the prior written consent of the owner of such proprietary rights. Without limiting the foregoing, if you believe that your work has been copied and posted on the Site through the Rgyan.com Service in a way that constitutes copyright infringement, please provide our Copyright Agent with the following information: an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest; a description of the copyrighted work that you claim has been infringed; a description of where the material that you claim is infringing is located on the Site; your address, telephone number, and email address; a written statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and where applicable a copy of the registration certificate proving registration of copyright or any other applicable intellectual property right; a statement by you, made under penalty of perjury, that the above information in your Notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf. Rgyan.com's Copyright Agent for Notice of claims of copyright infringement can be reached by writing to the NEW DELHI address located under the Help/Contact section on the site.</p>
				</div>


				<div class="col-md-12">
					<h4> Disclaimers</h4>
                        <p class="text-justify" style="color:#fff">your use of the rgyan.com service is at your sole risk
please note that this contract stricly limits our liability and rgyan.com does not provide warranties for the service. The contract also limits your remedies.
Astrology is an evolving body of knowledge, with experiences and original research being contributed by astrologers from around the world. Any data, interpretation, prediction or information in any form received through rgyan.com should not be treated as substitute for advice or treatment you would normally receive from medical professionals, financial advisors, legal consultants and other such licensed services. Astroyogi and its subsidiaries, affiliates, officers, employees, agents, partners and licensors expressly disclaim all warranties of any kind, whether express or implied, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement.<br>
Astrological counseling offered through rgyan.com is based on cumulative or individual knowledge, experience and interpretations of astrologers and as such it may wary from one astrologer to another.<br>
Astrologers on the panel of rgyan.com may from time to time make recommendations of using mantras, yantras, gemstones or other astrological remedies to be used by you. Such recommendations are being made in good faith by the astrologers and rgyan.com and its subsidiaries, affiliates, officers, employees, agents, partners and licensors make no warranty that (i) the service will meet your requirements; (ii) the service will be uninterrupted, timely, secure or error-free; (iii) the results that may be obtained from the use of the service will be accurate or reliable; (iv) the quality of any products, services, information or other material purchased or obtained by you through the service will meet your expectations; and (v) any errors in the software will be corrected. You are required to make full disclosure about the emotional, mental and physical state of the person seeking advice from the panel of astrologers of rgyan.com, so that the astrologers make an informed judgment about giving advice.<br>
Any material downloaded or otherwise obtained through the use of the service is accessed at your own discretion and risk, and you will be solely responsible for any damage to your computer system or loss of data that results from the download of any such material. Limitation on liability: you expressly understand and agree that rgyan.com and its subsidiaries, affiliates, officers, employees, agents, partners and licensors shall not be liable to you for any direct, indirect, incidental, special, consequential or exemplary damages, including, but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if rgyan.com has been advised of the possibility of such damages), resulting from: <br>(i) The use or the inability to use the service.<br>(ii) The cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the service.<br> (iii) unauthorized access to or alteration of your transmissions or data.<br>(iv) Statements or conduct of any third party on the service. <br>(v) Any other matter relating to the service.<br>
Notwithstanding anything to the contrary contained herein, rgyan.com liability to you for any cause whatsoever, and regardless of the form of the action, will at all times be limited to the amount paid, if any, by you to rgyan.com, for the service during the term of membership.</p>
				</div>

				<div class="col-md-12">
					<h4>Disputes</h4>
                        <p class="text-justify" style="color:#fff">Any claim in relation to this agreement or in connection therewith shall be filed with opposite party within 24 months from the date when the claim first could be filed.<br>If there is any dispute about or involving the Site and/or the Service, by using the services you further agree that the disputed matter will be governed by the laws of India. You agree to the exclusive jurisdiction of the courts of NEW DELHI, India.</p>
				</div>

				<div class="col-md-12">
					<h4>Assignment</h4>
                        <p class="text-justify" style="color:#fff">Rgyan.com may assign this contract to anybody with or without notice to you. You can not assign this contract to anybody without getting prior written approval/consent of Rgyan.com. Any such transfer/assignment by you without getting approval of Rgyan.com shall be void.</p>
				</div>

				<div class="col-md-12">
					<h4> Indemnity</h4>
                        <p class="text-justify" style="color:#fff">You agree to indemnify and hold Rgyan.com, its subsidiaries, directors, affiliates, officers, agents, and other partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorney's fees, made by any third party due to or arising out of your use of the Service in violation of this Agreement and/or arising from a breach of any of the terms of this Agreement and/or any breach of your representations and warranties set forth above.</p><br>
				</div>

				<div class="col-md-12">
					<h4> Dealings With Advertisers</h4>
                        <p class="text-justify" style="color:#fff">Any of your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Service, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such advertiser. You agree that Rgyan.com shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such advertisers on the Service.</p>
				</div>

				<div class="col-md-12">
					<h4> Other</h4>
                        <p class="text-justify" style="color:#fff">By becoming a Member of the Site / Rgyan.com Service, you agree to receive certain specific emails and SMS from Rgyan.com. This Agreement, accepted upon use of the Site and further affirmed by becoming a Member of the Rgyan.com service, contains the entire agreement between you and Rgyan.com regarding the use of the Site and/or the Service. If any provision of this Agreement is held invalid, the remainder of this Agreement shall continue in full force and effect.
You are under an obligation to report any misuse or abuse of the Site. If you notice any abuse or misuse of the Site or any thing which is in violation of this Agreement, you shall forthwith report such violation to Rgyan.com by writing to Customer Care. On receipt of such complaint, Rgyan.com may investigate such complaint and if necessary may terminate the membership of the Member responsible for such violation abuse or misuse without any refund of subscription fee. Any false complaint made by a Member shall make such Member liable for termination of his / her membership without any refund of the subscription fee.
Rgyan.com is a trademark of Netway India Pvt. Ltd. By joining Rgyan.com, you confirm that you have read the above provisions and agree to abide by them.
Images: Some images used in the site have been taken from the internet and if, any of these belong to you, please let us know. Send us a notification and we will promptly remove these post verification.<br>Product images may vary from the actual product as some of these products are hand crafted and images are digitally enhanced for the web.
Should you need any clarification in respect of this website, you may contact us.</p>
				</div>

				<div class="col-md-12">
					<h4>Refund &amp; Cancellation</h4>
                        <p class="text-justify" style="color:#fff">Refunds cannot be issued on the order of any reports under any circumstances, once it reaches the “processing” (Allocated to astrologer) stage. Customers are requested to order carefully and with full consideration. If you wish to cancel an order, please e-mail customer care team at customercare@rgyan.com within 1 hour from making the payment. The timelines displayed on the site are an approximate. We try to adhere to them to a very large extent. However, as these reports are individual and manually generated by our astrologers, some delays are possible. We will try on a best case to deliver as soon as possible. Delays will not be considered as biases for refunds.<br>We do not consider refunds for in-correct data provided by the customer. We request you to please re-check all your data when entered. Rgyan.com will not consider refund requests resulting from the incorrect data provided by you. However, if you e-mail us at customercare@rgyan.com within an hour of ordering the report, we will consider the changes to be made.<br>Any delay in the activation of subscription services will be dealt with immediately and compensated on pro-rata biases. No refunds will be issued on the return of damaged products. By ordering products on Rgyan.com, the customer takes full responsibility of any damage caused to the product, post its delivery. In case such an order was made via the ‘cash on delivery’ mode of payment, the customer will be charged the cost of the product, as displayed on Rgyan.com and the shipping/ customs/ courier charges or all three as applicable, if the product is returned.<br>The products and services offered by Rgyan.com are not meant to replace any philosophical, emotional or medical treatment. It is offered here as a service. Rgyan.com holds no responsibility or liability about the reality or reliability of the astrological effects on the human physiology, by the gems represented and sold on the website. On ordering the customer assumes full responsibility upon this matter. Thus, no refunds will be issued on this ground.<br>Any damage caused to the product while in transit, will be dealt with by Rgyan.com and its agencies. While Rgyan.com tries to make sure that the products displayed on the website are delivered to the customer in an as-is condition, the picture may differ slightly from the original product. The customers are advised to exercise discretion in this matter. No refunds will be issued on this bias.<br>Phone Advise: Please ensure that your contact number is in full coverage area and that you answer the phone when it rings. Any call once connected, will not be refunded.</p>
				</div>

				<div class="col-md-12">
					<h4> Refund Policy</h4>
                        <p class="text-justify" style="color:#fff">All refunds will be made after deducting the transaction charges levied by the bank and / or the payment gateway, the cost of shipping or courier charges (back and forth), customs duty and / or any other charges that may have been borne by Rgyan.com in processing and / or delivering the service, as applicable. Multiple payments against the same order will be refunded in full (without deducting transaction charges on the duplicate payments), after retaining the cost of a singular order.</p>
				</div>


      


      
			</div><!--/row-fluid-->
		</div>
  
    <!--Terms & Conditions section-->
  
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>