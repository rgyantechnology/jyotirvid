<?php 
/*Example(s): 
For a table of firstName and lastName: 
John Smith 
Mark Smith 
Jack Johnson 
Bob Johnson */
//single query, single result 
$query = "SELECT * FROM names WHERE firstName=? AND lastName=?"; 
$params = array("Bob","Johnson"); 

mysqli_prepared_query($link,$query,"ss",$params) 
/* 
returns array( 
0=> array('firstName' => 'Bob', 'lastName' => 'Johnson') 
) 
*/ 

//single query, multiple results 
$query = "SELECT * FROM names WHERE lastName=?"; 
$params = array("Smith"); 

mysqli_prepared_query($link,$query,"s",$params) 
/* 
returns array( 
0=> array('firstName' => 'John', 'lastName' => 'Smith') 
1=> array('firstName' => 'Mark', 'lastName' => 'Smith') 
) 
*/ 

//multiple query, multiple results 
$query = "SELECT * FROM names WHERE lastName=?"; 
$params = array(array("Smith"),array("Johnson")); 

mysqli_prepared_query($link,$query,"s",$params) 
/* 
returns array( 
0=> 
array( 
0=> array('firstName' => 'John', 'lastName' => 'Smith') 
1=> array('firstName' => 'Mark', 'lastName' => 'Smith') 
) 
1=> 
array( 
0=> array('firstName' => 'Jack', 'lastName' => 'Johnson') 
1=> array('firstName' => 'Bob', 'lastName' => 'Johnson') 
) 
) 
*/ 
?>