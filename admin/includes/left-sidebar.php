<div class="sidebar-nav">
<ul>
<li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>Manage Master<i class="fa fa-collapse"></i></a></li>
<li><ul class="dashboard-menu nav nav-list collapse in">
<li <?php if($_SESSION['page']=='manage-category'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-category.php"><span class="fa fa-caret-right"></span> Manage Category</a></li>
<li <?php if($_SESSION['page']=='manage-service'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-service.php"><span class="fa fa-caret-right"></span> Manage Service</a></li>
<li <?php if($_SESSION['page']=='manage-zodaic'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-zodaic.php"><span class="fa fa-caret-right"></span> Manage Zodaic</a></li>
<li <?php if($_SESSION['page']=='manage-user'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-user.php"><span class="fa fa-caret-right"></span> Manage User</a></li>
<li <?php if($_SESSION['page']=='manage-role'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-role.php"><span class="fa fa-caret-right"></span> Manage Role</a></li>
<li <?php if($_SESSION['page']=='manage-zodaics'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>"><span class="fa fa-caret-right"></span> Manage Permission</a></li>
<li <?php if($_SESSION['page']=='manage-location'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-location.php"><span class="fa fa-caret-right"></span> Manage Location</a></li>
<li <?php if($_SESSION['page']=='manage-astrologer'){?> class="active" <?php } ?> ><a href="<?php echo LINK_URL_ADMIN ?>manage-astrologer.php"><span class="fa fa-caret-right"></span> Manage Astrologers</a></li>
</ul>
</li>
</ul>
</div>