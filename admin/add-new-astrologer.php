<?php 
error_reporting(1);
include('includes/function.php');
if($_SESSION['username']=='' and $_SESSION['id']==0)
{
header('location:index.php');	
}
else{
$_SESSION['page'] = 'manage-astrologer';
}
if(isset($_POST['action']) && $_POST['action']=='add')
{
	@extract($_POST);
	$path = '../upload/';
	if(empty($_FILES['fld_logo']['name'])){
	$message = "Please Upload Profile Picture."; 
	}else{
	$fileext = explode('.',$_FILES['fld_logo']['name']); 
	}        
	if(!in_array($_FILES['fld_logo']['type'],array('image/jpg','image/jpeg','image/png'))){
	$message = "Please upload .";
	}

	if(intval($_FILES['fld_logo']['size']) > 15728640){ //5MB file size
			   
	}
	else{
	$filename = uniqid().'_'.time().'_Astrologer_'.$_POST['fname'].'.'.  strtolower(end($fileext));
	$dest = $path.$filename;
	move_uploaded_file($_FILES['fld_logo']['tmp_name'], $dest);
	}

	$value = $_POST['fname'].' '.$_POST['lname'];
	$alias = createAlias($value);
	$date = date('y-m-d');
	$is_active = 1;
	$is_delete = 0;
	$is_type = 'web';
	$insert = $db->prepare("INSERT INTO ".TBLASTROLOGERS." (fname, lname, alias, faname, moname, qualification, experience, dob, gender, address1, address2, area, city, latitude, longitude, state, country, landmark, pincode, email, mobile, skypeid, facebookpage, consultancyfee, websitelink, mapembeddedcode, image, is_active, is_delete, is_type, date ) 
	VALUES (:fname, :lname, :alias, :faname, :moname, :qualifications, :experience, :dob, :gender, :address1, :address2, :area, :city, :latitude, :longitude, :state, :country, :landmark, :pincode, :email, :mobile, :skypeid, :facebookpage, :consultancyfee, :websitelink, :mapembeddedcode, :image, :is_active, :is_delete, :is_type, :date)");
	$insert->bindParam(':fname', $fname);
	$insert->bindParam(':lname', $lname);
	$insert->bindParam(':alias', $alias);
	$insert->bindParam(':faname', $faname); 
	$insert->bindParam(':moname', $moname);
	$insert->bindParam(':qualifications', $qualification);
	$insert->bindParam(':experience', $experience);
	$insert->bindParam(':dob', $dob);
	$insert->bindParam(':gender', $gender);
	$insert->bindParam(':address1', $address1);
	$insert->bindParam(':address2', $address2);
	$insert->bindParam(':area', $area);
	$insert->bindParam(':city', $city);
	$insert->bindParam(':latitude', $latitude);
	$insert->bindParam(':longitude', $longitude);
	$insert->bindParam(':state', $state);
	$insert->bindParam(':country', $country);
	$insert->bindParam(':landmark', $landmark);
	$insert->bindParam(':pincode', $pincode);
	$insert->bindParam(':email', $email);
	$insert->bindParam(':mobile', $mobile);
	$insert->bindParam(':skypeid', $skypeid);
	$insert->bindParam(':facebookpage', $facebookpage);
	$insert->bindParam(':consultancyfee', $consultancyfee);
	$insert->bindParam(':websitelink', $websitelink);
	$insert->bindParam(':mapembeddedcode', $mapembeddedcode);
	$insert->bindParam(':image', $filename);
	$insert->bindParam(':is_active', $is_active);
	$insert->bindParam(':is_delete', $is_delete);
	$insert->bindParam(':is_type', $is_type);
	$insert->bindParam(':date', $date);
	$astrologer = $insert->execute();
     if($astrologer){
		$id = $db->LastInsertId();
                $categoriesPost = $_POST['categoryAstro'];
                $catinfo = array(); 
				
				for($i = 0; $i < count($categoriesPost); $i++){
					if($categoriesPost[$i]== 8){
					 $othercatflag = 1;	
					}
				$insertCatQuery = $db->prepare('INSERT INTO astrologercategories(astrologer_id, category_id)VALUE(:astrologer, :category)');
				$insertCatQuery->bindParam(':astrologer', $id);
				$insertCatQuery->bindParam(':category', $categoriesPost[$i]);
				$insertCatQuery->execute();
               }
				 if($othercatflag == 1){
				 $category = 8;	 
				 $othercategory = $_POST['othercategory'];
				 $insertOtherCatQuery = $db->prepare('INSERT INTO categoryothers(category_id, otherinfo, astrologers_id)VALUE(:category, :otherinfo, :astrologer)');	
				 $insertOtherCatQuery->bindParam(':category', $category);
				 $insertOtherCatQuery->bindParam(':otherinfo', $othercategory);
				 $insertOtherCatQuery->bindParam(':astrologer', $id);
				 $insertOtherCatQuery->execute();		
				 }
				 
			$servicesPost = $_POST['serviceAstro'];
			for($i=0; $i < count($servicesPost); $i++){
			if($servicesPost[$i]== 7){
			$otherserviceflag = 1;	
			}
			$InsertServQuery = $db->prepare('INSERT INTO astrologerservices (astrologer_id, service_id)VALUE(:astrologer, :service)');
			$InsertServQuery->bindParam(':astrologer', $id);
			$InsertServQuery->bindParam(':service', $servicesPost[$i]);
			$InsertServQuery->execute();
            }
			if($otherserviceflag == 1){
			$service = 7;
			$otherinfo = $_POST['otherservice'];	
			$InsertOtherServQuery = $db->prepare('INSERT INTO serviceothers (service_id, otherinfo, astrologers_id)VALUE(:service, :otherinfo, :astrologer)');
			$InsertOtherServQuery->bindParam(':service', $service);
			$InsertOtherServQuery->bindParam(':otherinfo', $otherinfo);
			$InsertOtherServQuery->bindParam(':astrologer', $id);
			$InsertOtherServQuery->execute();	
			}
	 }       
	
	header('location:add-moreinfo-astrologer.php?id='.$id);
}
else{
	if(isset($_POST['action'])&& $_POST['action']=='update')
	{
	@extract($_POST);
	$path = '../upload/';
	if(empty($_FILES['fld_logo']['name'])){
		$select = $db->prepare("SELECT * FROM ".TBLASTROLOGERS." WHERE id =:id");
		$select->bindParam(':id', $alias);
		$select->execute();
		$cols = $select->fetch();	
		$filename = $cols['image'];
        }else{
			$fileext = explode('.',$_FILES['fld_logo']['name']); 
			if(!in_array($_FILES['fld_logo']['type'],array('image/jpg','image/jpeg','image/png'))){
			  $message = "Please upload .";
			}
		   else if(intval($_FILES['fld_logo']['size']) > 15728640){ //5MB file size
						   
			}
			else{
			$filename = uniqid().'_'.time().'_Astrologer_'.$_POST['fname'].'.'.  strtolower(end($fileext));
			$dest = $path.$filename;
			move_uploaded_file($_FILES['fld_logo']['tmp_name'], $dest);
			}
			}   
	
	$date = date('y-m-d');
	$is_active = 1;
	$is_delete = 0;
	$is_type = 'web';
	//echo "UPDATE ".TBLASTROLOGERS." SET fname = :fname, lname = :lname, faname = :faname, moname = :moname, qualification = :qualifications, experience = :experience, dob = :dob, gender = :gender, address1 = :address1, address2 = :address2, area = :area, city = :city, latitude = :latitude, longitude = :longitude, state = :state, country = :country, landmark = :landmark, pincode = :pincode, email = :email, mobile = :mobile, skypeid = :skypeid, facebookpage = :facebookpage, consultancyfee = :consultancyfee, websitelink = :websitelink, mapembeddedcode = :mapembeddedcode, image = :image, is_active = :is_active, is_delete = :is_delete, is_type = :is_type, modified = :date  WHERE id = :id ";
	$update = $db->prepare("UPDATE ".TBLASTROLOGERS." SET fname = :fname, lname = :lname, faname = :faname, moname = :moname, qualification = :qualifications, experience = :experience, dob = :dob, gender = :gender, address1 = :address1, address2 = :address2, area = :area, city = :city, latitude = :latitude, longitude = :longitude, state = :state, country = :country, landmark = :landmark, pincode = :pincode, email = :email, mobile = :mobile, skypeid = :skypeid, facebookpage = :facebookpage, consultancyfee = :consultancyfee, websitelink = :websitelink, mapembeddedcode = :mapembeddedcode, image = :image, is_active = :is_active, is_delete = :is_delete, is_type = :is_type, modified = :date  WHERE id = :id ");
	$update->bindParam(':fname', $fname);
	$update->bindParam(':lname', $lname);
	$update->bindParam(':faname', $faname); 
	$update->bindParam(':moname', $moname);
	$update->bindParam(':qualifications', $qualification);
	$update->bindParam(':experience', $experience);
	$update->bindParam(':dob', $dob);
	$update->bindParam(':gender', $gender);
	$update->bindParam(':address1', $address1);
	$update->bindParam(':address2', $address2);
	$update->bindParam(':area', $area);
	$update->bindParam(':city', $city);
	$update->bindParam(':latitude', $latitude);
	$update->bindParam(':longitude', $longitude);
	$update->bindParam(':state', $state);
	$update->bindParam(':country', $country);
	$update->bindParam(':landmark', $landmark);
	$update->bindParam(':pincode', $pincode);
	$update->bindParam(':email', $email);
	$update->bindParam(':mobile', $mobile);
	$update->bindParam(':skypeid', $skypeid);
	$update->bindParam(':facebookpage', $facebookpage);
	$update->bindParam(':consultancyfee', $consultancyfee);
	$update->bindParam(':websitelink', $websitelink);
	$update->bindParam(':mapembeddedcode', $mapembeddedcode);
	$update->bindParam(':image', $filename);
	$update->bindParam(':is_active', $is_active);
	$update->bindParam(':is_delete', $is_delete);
	$update->bindParam(':is_type', $is_type);
	$update->bindParam(':date', $date);
	$update->bindParam(':id', $id);
	$astrologer = $update->execute();
     if($astrologer){
		//$id = $_POST['id'];
		//echo 'DELETE FROM astrologercategories WHERE astrologer_id= '.$id;
		$deletecatquery = $db->prepare('DELETE FROM astrologercategories WHERE astrologer_id= '.$id);
		$deletecatquery->execute();
		$deleteservquery = $db->prepare('DELETE FROM astrologerservices WHERE astrologer_id= '.$id);
		$deleteservquery->execute();
		
                $categoriesPost = $_POST['categoryAstro'];
                $catinfo = array(); 
				
				for($i = 0; $i < count($categoriesPost); $i++){
					if($categoriesPost[$i]== 8){
					 $othercatflag = 1;	
					}
				$insertCatQuery = $db->prepare('INSERT INTO astrologercategories(astrologer_id, category_id)VALUE(:astrologer, :category)');
				$insertCatQuery->bindParam(':astrologer', $id);
				$insertCatQuery->bindParam(':category', $categoriesPost[$i]);
				$insertCatQuery->execute();
               }
				 if($othercatflag == 1){
				 $category = 8;	 
				 $othercategory = $_POST['othercategory'];
				 $insertOtherCatQuery = $db->prepare('INSERT INTO categoryothers(category_id, otherinfo, astrologers_id)VALUE(:category, :otherinfo, :astrologer)');	
				 $insertOtherCatQuery->bindParam(':category', $category);
				 $insertOtherCatQuery->bindParam(':otherinfo', $othercategory);
				 $insertOtherCatQuery->bindParam(':astrologer', $id);
				 $insertOtherCatQuery->execute();		
				 }
				 
			$servicesPost = $_POST['serviceAstro'];
			for($i=0; $i < count($servicesPost); $i++){
			if($servicesPost[$i]== 7){
			$otherserviceflag = 1;	
			}
			$InsertServQuery = $db->prepare('INSERT INTO astrologerservices (astrologer_id, service_id)VALUE(:astrologer, :service)');
			$InsertServQuery->bindParam(':astrologer', $id);
			$InsertServQuery->bindParam(':service', $servicesPost[$i]);
			$InsertServQuery->execute();
            }
			if($otherserviceflag == 1){
			$service = 7;
			$otherinfo = $_POST['otherservice'];	
			$InsertOtherServQuery = $db->prepare('INSERT INTO serviceothers (service_id, otherinfo, astrologers_id)VALUE(:service, :otherinfo, :astrologer)');
			$InsertOtherServQuery->bindParam(':service', $service);
			$InsertOtherServQuery->bindParam(':otherinfo', $otherinfo);
			$InsertOtherServQuery->bindParam(':astrologer', $id);
			$InsertOtherServQuery->execute();	
			}
	 }
	
	header('location:add-moreinfo-astrologer.php?id='.$id);
	}
	
}
if(isset($_REQUEST['edit']))
{		
	$alias = $_REQUEST['edit'];
	$select = $db->prepare("SELECT * FROM ".TBLASTROLOGERS." WHERE id =:id");
	$select->bindParam(':id', $alias);
	$select->execute();
	$cols = $select->fetch();	
}
?>
<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>Online Consultancy | Add New Astrologer</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
<script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
<link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="js/datepicker/datepicker3.css">
<link rel="stylesheet" type="text/css" href="js/daterangepicker/daterangepicker.css">  
<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js"></script>
<script type="text/javascript" language="javascript" src="resources/demo.js"></script>
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('#example').DataTable();
} );
</script>
</head>
<body class=" theme-blue">
<!-- Demo page code -->
<script type="text/javascript">
$(function() {
var match = document.cookie.match(new RegExp('color=([^;]+)'));
if(match) var color = match[1];
if(color) {
$('body').removeClass(function (index, css) {
return (css.match (/\btheme-\S+/g) || []).join(' ')
})
$('body').addClass('theme-' + color);
}

$('[data-popover="true"]').popover({html: true});

});
</script>

<style>
/*table, th, td {
    border: 1px solid black;
}*/
.error{
	color:red;
}
</style>

<script type="text/javascript">
$(function() {
var uls = $('.sidebar-nav > ul > *').clone();
uls.addClass('visible-xs');
$('#main-menu').append(uls.clone());
});
</script>
<script src="js/jQuery.js" type="text/javascript"></script>
<script type="text/javascript">

// Change Your home URL..
home_url = 'http://rgyan.com/astrologer/admin';

/* *
*     fileName - ajax file name to be called by ajax method.
*     data - pass the infromation(like location-id , location-type) via data variable.
*     loadDataToDiv - id of the div to which the ajax responce is to be loaded.
* */
function ajax_call(fileName,data, loadDataToDiv) {
jQuery("#"+loadDataToDiv).html('<option selected="selected">-- -- -- Loding Data -- -- --</option>');

//  If you are changing counrty, make the state and city fields blank
if(loadDataToDiv=='state'){
jQuery('#city').html('');
jQuery('#state').html('');                    
}
//  If you are changing state, make the city fields blank
if(loadDataToDiv=='city'){
jQuery('#city').html('');
}

jQuery.post(home_url + '/' + fileName + '.php', data, function(result) {
jQuery('#' + loadDataToDiv).html(result);
});
}
</script>
<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
/*tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});*/
</script>
<script type="text/javascript">
function PreviewImage() {
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("fld_logo").files[0]);

oFReader.onload = function (oFREvent) {
document.getElementById("uploadPreview").src = oFREvent.target.result;
};
};

function showothercategory(){
if(document.getElementById("Category_8").checked == true)	{
document.getElementById('CategoryOthersSpan').style.display='block';	
}
else{
if(document.getElementById("Category_8").checked == false){	
document.getElementById('CategoryOthersSpan').style.display='none';
}	
}
}

function showotherservice(){
if(document.getElementById("Service_7").checked == true)	{
document.getElementById('ServiceOthersSpan').style.display='block';	
}
else{
if(document.getElementById("Service_7").checked == false){	
document.getElementById('ServiceOthersSpan').style.display='none';
}	
}	
}
</script>
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<div class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="" href="manage-service.php">Manage Astrologer</a></div>
<div class="navbar-collapse collapse" style="height: 1px;">
<ul id="main-menu" class="nav navbar-nav navbar-right">
<li class="dropdown hidden-xs">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo ucfirst($_SESSION['username']);?>
<i class="fa fa-caret-down"></i>
</a>

<ul class="dropdown-menu">
<li class="divider"></li>
<li class="dropdown-header">Super Admin</li>
<li class="divider"></li>
<li><a tabindex="-1" href="index.php">Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<?php include('includes/left-sidebar.php')?>     
<div class="content">
<div class="header">
<h1 class="page-title"><?php if($cols['id']){ ?>Edit Existing Astrologer<?php }else{ ?>Add New <?php } ?></h1>
<ul class="breadcrumb">
<li><a href="manage.php">Home</a> </li>
<li><a href="manage.php">Manage Astrologer</a> </li>
<li class="active"><?php if($cols['id']){ ?>Edit Existing Astrologer<?php }else{?>Add New <?php } ?></li>
</ul>
</div>
<div class="main-content">
<form  name = "addnewform" id ="addnewform" action="" method="post" enctype="multipart/form-data" >
<table width="80%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td colspan="2" valign="top"><p class="btn btn-primary" style="width:100% !important; text-align:left !important; font-size:18px;"><strong>Personal Information</strong></p></td>
</tr>
<tr>
<td colspan="2" valign="top">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td width="10%"><label>First Name</label></td>
<td width="20%"><input type="text" name="fname" id="fname" class="form-control required /^[a-z," "]+$/ " minlength = 2 value="<?php echo tep_db_output($cols['fname'])?>"></td>
<td width="10%"><label>Last Name</label></td>
<td width="20%"><input type="text" name="lname" id="lname" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['lname'])?>"></td>
</tr>

<tr>
<td width="10%"><label>Father's Name</label></td>
<td width="20%"><input type="text" name="faname" id="faname" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['faname'])?>"></td>
<td width="10%"><label>Mother's Name</label></td>
<td width="20%"><input type="text" name="moname" id="moname" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['moname'])?>"></td>
</tr>

<tr>
<td width="10%"><label>Date Of Birth</label></td>
<td width="20%"><input type="text" name="dob" id="dob" class="form-control required date" value="<?php echo tep_db_output($cols['dob'])?>"></td>
<td width="10%"><label>Gender</label></td>
<td width="20%">
<input type="radio" name="gender" id="gendermale"  value="Male" <?php if($cols['gender']=='Male'){?> checked = "checked"<?php } ?>> Male &nbsp; &nbsp;
<input type="radio" name="gender" id="genderfemale"  value="Female" <?php if($cols['gender']=='Female'){ ?>checked = "checked"<?php }else{ if($cols['gender']=='Male'){ ?> <?php }else{ ?> checked="checked"<?php } } ?>> Female

</td>
</tr>

<tr>
<td width="10%"><label>Qualification(In Relevant Field)</label></td>
<td width="20%"><input type="text" name="qualification" id="qualification" class="form-control required " minlength = 2 value="<?php echo tep_db_output($cols['qualification'])?>"></td>
<td width="10%"><label>Experience</label></td>
<td width="20%">
<input type="text" name="experience" id="experience" class="form-control required number" value="<?php echo tep_db_output($cols['experience'])?>">
</td>
</tr>

<tr>
<td width="10%"><label>Address(Line-1)</label></td>
<td width="20%"><input type="text" name="address1" id="address1" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['address1'])?>"></td>
<td width="10%"><label>Address(Line-2)</label></td>
<td width="20%">
<input type="text" name="address2" id="address2" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['address2'])?>">
</td>
</tr>

<tr>
<td width="10%"><label>Landmark</label></td>
<td width="20%"><input type="text" name="landmark" id="landmark" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['landmark'])?>"></td>
<td width="10%"><label>Area</label></td>
<td width="20%">
<input type="text" name="area" id="area" class="form-control required" minlength = 2 value="<?php echo tep_db_output($cols['area'])?>">
</td>
</tr>

<tr>
<td width="10%"><label>Latitude</label></td>
<td width="20%"><input type="text" name="latitude" id="latitude" class="form-control required number" value="<?php echo tep_db_output($cols['latitude'])?>"></td>
<td width="10%"><label>Longitude</label></td>
<td width="20%">
<input type="text" name="longitude" id="longitude" class="form-control required number" value="<?php echo tep_db_output($cols['longitude'])?>">
</td>
</tr>

<tr>
<td width="10%"><label>Pin Code</label></td>
<td width="20%"><input type="text" name="pincode" id="pincode" class="form-control required number" value="<?php echo tep_db_output($cols['pincode'])?>"></td>
<td width="10%"><label>Country</label></td>
<td width="20%">
<?php 
$SelectCountry = $db->prepare("SELECT location_id, name FROM locations WHERE location_type = 0 ");
$SelectCountry->execute();
?>
<select name="country" id="country" class="form-control" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:1}, 'state')">
<option value="">Select Country</option>
<?php 
while($cres = $SelectCountry->fetch()){
?>
<option value="<?php echo $cres['location_id']?>" <?php if($cres['location_id'] == '100'){ ?> selected = "selected" <?php } ?>><?php echo $cres['name']?></option>
<?php	
}
?>
</select>
</td>
</tr>

<tr>
<td width="10%"><label>State</label></td>
<td width="20%"><?php 
$SelectState = $db->prepare("SELECT location_id, name FROM locations WHERE location_type = 1 AND parent_id = 100");
$SelectState->execute();
?>
<select name="state" id="state" class="form-control" onchange="ajax_call('ajaxCall',{location_id:this.value,location_type:2}, 'city')" >
<option value="">Select State</option>
<?php 
while($sres = $SelectState->fetch()){
?>
<option value="<?php echo $sres['location_id']?>" <?php if($sres['location_id'] == $cols['state']){ ?> selected = "selected" <?php } ?>><?php echo $sres['name']?></option>
<?php	
}
?>
</select></td>
<td width="10%"><label>City</label></td>
<td width="20%"><?php 
$SelectCity = $db->prepare("SELECT location_id, name FROM locations WHERE location_type=2");
$SelectCity->execute();

?>
<select name="city" id="city" class="form-control">
<option value="">Select City</option>
<?php 
while($csres = $SelectCity->fetch()){
?>
<option value="<?php echo $csres['location_id']?>" <?php if($csres['location_id']==$cols['city']){ ?> selected = "selected" <?php } ?> > <?php echo $csres['name']?></option>
<?php	
}
?>
</select></td>
</tr>

<tr>
<td width="10%"><label>Email</label></td>
<td width="20%"><input type="text" name="email" id="email" class="form-control required email" value="<?php echo tep_db_output($cols['email'])?>"></td>
<td width="10%"><label>Contact No.</label></td>
<td width="20%"><input type="text" name="mobile" id="mobile" class="form-control required number" value="<?php echo tep_db_output($cols['mobile'])?>"></td>
</tr>

<tr>
<td width="10%"><label>Skype Id</label></td>
<td width="20%"><input type="text" name="skypeid" id="skypeid" class="form-control required" value="<?php echo tep_db_output($cols['skypeid'])?>"></td>
<td width="10%"><label>Cosultancy Fee</label></td>
<td width="20%"><input type="text" name="consultancyfee" id="consultancyfee" class="form-control required number" value="<?php echo tep_db_output($cols['consultancyfee'])?>"></td>
</tr>

<tr>
<td width="10%"><label>Facebook</label></td>
<td width="20%"><input type="text" name="facebookpage" id="facebookpage" class="form-control required url" value="<?php echo tep_db_output($cols['facebookpage'])?>"></td>
<td width="10%"><label>Website</label></td>
<td width="20%"><input type="text" name="websitelink" id="websitelink" class="form-control required url" value="<?php echo tep_db_output($cols['websitelink'])?>"></td>
</tr>
</table>
</td>
</tr>

<tr>
<td colspan="2" valign="top"><p class="btn btn-primary" style="width:100% !important; text-align:left !important; font-size:18px;"><strong>Upload Profile Picture</strong></p></td>
</tr>
<tr>
<td>
<span id="user-profile"><img id="uploadPreview" src="<?php echo LINK_URL_HOME?>upload/<?php echo $cols['image']?>" width="100" height="80" /></span>
<input type="file" name="fld_logo" id="fld_logo" class="form-control" onchange="PreviewImage()">
</td>
</tr>

<tr>
<td colspan="2" valign="top"><p class="btn btn-primary" style="width:100% !important; text-align:left !important; font-size:18px;"><strong>Add Embedded Code of Google Map</strong></p></td>
</tr>
<tr>
<td>
<textarea  name="mapembeddedcode" id="mapembeddedcode" class="form-control"><?php echo $cols['mapembeddedcode']?></textarea>
<?php 
if($cols['id']){
?>
<p>
<br />
<span>
<?php echo $cols['mapembeddedcode']?>
</span>
</p>
<?php } ?>
</td>
</tr>
<tr>
<td colspan="2" valign="top"><p class="btn btn-primary" style="width:100% !important; text-align:left !important; font-size:18px;"><strong>Select Categories</strong></p></td>
</tr>
<tr>
<td>
<?php 
$celement = "";
$selcatquery = $db->prepare("SELECT id, name FROM categories");
$selcatquery->execute();
$categoryquery = $db->prepare("SELECT category_id FROM astrologercategories WHERE astrologer_id = :astrologer") ;
$categoryquery->bindParam(':astrologer', $alias);
$categoryquery->execute();
$catres = $categoryquery->fetchAll();
$catotherquery = $db->prepare("SELECT otherinfo, category_id FROM categoryothers WHERE astrologers_id = :astrologer");
$catotherquery->bindParam(':astrologer', $alias); 
$catotherquery->execute();
$catotherres = $catotherquery->fetch();
while($categories = $selcatquery->fetch()){
if($categories['id'] == 8){ $celement = 'showothercategory()';}else{ $celement = ""; }
?>
<input type="checkbox" name="categoryAstro[]" , id="Category_<?php echo $categories['id']?>" value="<?php echo $categories['id'] ?>" onclick="<?php echo $celement ?>"  <?php foreach($catres as $catrs){ if($catrs['category_id'] == $categories['id']){ ?> checked = "checked" <?php }} ?>><?php echo $categories['name']?><br />
<?php    
}
if($catotherres['category_id'] == 8){
?>
<span id="CategoryOthersSpan" style="display:block;">
<label for="exampleInputEmail1">Other Category Detail</label>
<input type = "text" name="othercategory" id="othercategory" value="<?php echo $catotherres['otherinfo']?>" class="form-control">
</span>
<?php	
}
else{
?>      
<span id="CategoryOthersSpan" style="display:none;">
<label for="exampleInputEmail1">Other Category Detail</label>
<input type = "text" name="othercategory" id="othercategory" value="" class="form-control">
</span>
<?php 
}
?>
</td>
</tr>

<tr>
<td colspan="2" valign="top"><p class="btn btn-primary" style="width:100% !important; text-align:left !important; font-size:18px;"><strong>Select Services</strong></p></td>
</tr>
<tr>
<td>
<?php 
$selements = "";
$selcasevquery = $db->prepare("SELECT id, name FROM services");
$selcasevquery->execute();
$servicequery = $db->prepare("SELECT service_id FROM astrologerservices WHERE astrologer_id = :astrologer") ;
$servicequery->bindParam(':astrologer', $alias);
$servicequery->execute();
$servRes = $servicequery->fetchAll();
$serviceotherquery = $db->prepare(" SELECT otherinfo, service_id FROM serviceothers WHERE astrologers_id = :astrologer");
$serviceotherquery->bindParam(':astrologer', $alias); 
$serviceotherquery->execute();
$servotherres = $serviceotherquery->fetch();
while($services = $selcasevquery->fetch()){
if($services['id'] == 7){ $celement = 'showotherservice()';}else{ $celement = ""; }
?>
<input type="checkbox" name="serviceAstro[]" , id="Service_<?php echo $services['id']?>" value="<?php echo $services['id'] ?>" onclick="<?php echo $celement ?>" <?php foreach($servRes as $selserv){ if($selserv['service_id']==$services['id']){ ?> checked = "checked"<?php }} ?> ><?php echo $services['name']?><br />
<?php    
}
if($servotherres['service_id'] == 7){
?>
<span id="ServiceOthersSpan" style="display:block;">
<label for="exampleInputEmail1">Other Service Detail</label>
<input type = "text" name="otherservice" id="otherservice" value="<?php echo $servotherres['otherinfo']?>" class="form-control">
</span>
<?php	
}
else{
?>      
<span id="ServiceOthersSpan" style="display:none;">
<label for="exampleInputEmail1">Other Service Detail</label>
<input type = "text" name="otherservice" id="otherservice" value="" class="form-control">
</span>
<?php } ?>
</td>
</tr>
</table>
<br />
<div class="btn-toolbar list-toolbar">
<?php if($cols['id']){ ?>
<button class="btn btn-primary">Update</button>	
<input type="hidden" name="action" value="update">
<input type="hidden" name="id" value="<?php echo $cols['id'] ?>"> 
<?php 	
} 
else{
?>
<button class="btn btn-primary">Submit</button>
<input type="hidden" name="action" value="add">
<?php
}
?>
<a href="manage-astrologer.php" class="btn btn-primary" title="Cancel" >Cancel</a>
</div>
</form>

<footer>
<hr>
</footer>
</div>
</div>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#addnewform123").validate({ });
});
</script>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
$("[rel=tooltip]").tooltip();
$(function() {
$('.demo-cancel-click').click(function(){return false;});
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="js/datepicker/bootstrap-datepicker.js"></script>

<!-- datepicker -->

<script>
  $(function () {
   $('#dob').datepicker({
    format: 'yyyy-mm-dd',
        endDate: '+0d',
        autoclose: true
});
  });
  
</script>
</body>
</html>