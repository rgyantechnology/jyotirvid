<?php 
include('includes/function.php');
if($_SESSION['username']=='')
{
header('location:index.php');	
}
else{
$_SESSION['page'] = 'dashboard';
}

?>
<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>Astrologer | Dashboard</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
<script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
<link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
<style type="text/css" class="init">
</style>
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js"></script>
<script type="text/javascript" language="javascript" src="resources/demo.js"></script>
<script type="text/javascript" language="javascript" class="init">
//$('#example').DataTable();
$(document).ready( function () {
$('#example').DataTable({
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] },
			{ "bSortable": false, "aTargets": [ 1 ] },
			{ "bSortable": true, "aTargets": [ 2 ] },
			{ "bSortable": true, "aTargets": [ 3 ] },
			{ "bSortable": false, "aTargets": [ 4 ] },
			{ "bSortable": false, "aTargets": [ 5 ] },
			{ "bSortable": false, "aTargets": [ 5 ] }
			
			
        ],
        
	}
);		
} );
</script>
<script type="text/javascript">
function checkall(objForm)
{
	//alert("prabhat");
	len = objForm.elements.length;
	//alert(len);
	var i=0;
	for( i=0 ; i<len ; i++) 
	{
		if (objForm.elements[i].type=='checkbox') 
		{
			objForm.elements[i].checked=objForm.check_all.checked;
		}
	}
}
function delete_confirm(form)
{
if(form.btn_submit.value == "Delete")
	{	
		if(confirm("Are you sure you want to Delete? "))
		{
			form.submit;
		}
		else
		{
			return false;
		}
	}
}
</script>
</head>
<body class=" theme-blue">
<!-- Demo page code -->
<script type="text/javascript">
$(function() {
var match = document.cookie.match(new RegExp('color=([^;]+)'));
if(match) var color = match[1];
if(color) {
$('body').removeClass(function (index, css) {
return (css.match (/\btheme-\S+/g) || []).join(' ')
})
$('body').addClass('theme-' + color);
}

$('[data-popover="true"]').popover({html: true});

});
</script>
<style type="text/css">
#line-chart {
height:300px;
width:800px;
margin: 0px auto;
margin-top: 1em;
}
.navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
color: #fff;
}
</style>

<script type="text/javascript">
$(function() {
var uls = $('.sidebar-nav > ul > *').clone();
uls.addClass('visible-xs');
$('#main-menu').append(uls.clone());
});
</script>
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<div class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="" href="manage.php">Online Consultancy</a></div>
<div class="navbar-collapse collapse" style="height: 1px;">
<ul id="main-menu" class="nav navbar-nav navbar-right">
<li class="dropdown hidden-xs">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo ucfirst($_SESSION['username']);?>
<i class="fa fa-caret-down"></i>
</a>

<ul class="dropdown-menu">
<li class="divider"></li>
<li class="dropdown-header">Super Admin</li>
<li class="divider"></li>
<li><a tabindex="-1" href="index.php">Logout</a></li>
</ul>
</li>
</ul>

</div>
</div>
</div>
<?php include('includes/left-sidebar.php')?>     
<div class="content">
<div class="header">
<h1 class="page-title">Dashboard</h1>
<ul class="breadcrumb">
<li><a href="dashboard.php">Home</a> </li>
<li class="active">Dashboard</li>
</ul>
</div>

<div class="main-content">
<?php
if(isset($_REQUEST['mesg']))
{
if($_REQUEST['mesg'] == 1)
{
?>
<p style="border: 1px rgb(182, 19, 3) solid; text-align:center;" >Your school has been deleted.</p>
<?php	
}
else{
if($_REQUEST['mesg'] == 2)
{
?>
<p style="border: 1px rgb(182, 19, 3) solid; text-align:center;">Your school has been stored.</p>
<?php			
}
else{
	if($_REQUEST['mesg']==3)
	{
	?>
	<p style="border: 1px rgb(182, 19, 3) solid; text-align:center;">Your school has been modified.</p>	
	<?php
	}
}	
}	
}
?>
<footer>
<hr>
</footer>
</div>
</div>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
$("[rel=tooltip]").tooltip();
$(function() {
$('.demo-cancel-click').click(function(){return false;});
});
</script>
</body>
</html>