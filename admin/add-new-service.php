<?php 
error_reporting(0);
include('includes/function.php');
if($_SESSION['username']=='' and $_SESSION['id']==0)
{
header('location:index.php');	
}
else{
$_SESSION['page'] = 'manage-service';
}
if(isset($_POST['action']) && $_POST['action']=='add')
{
	$name = $_POST['name'];
	$alias = createAlias($name);
	$insert = $db->prepare("INSERT INTO ".TBLSERVICES." (name, alias) 
	VALUES (:name, :alias)");
	$insert->bindParam(':name', $name);
	$insert->bindParam(':alias', $alias);
	$insert->execute();
	header('location:manage-service.php?mesg=2');
}
else{
	if(isset($_POST['action'])&& $_POST['action']=='update')
	{
	$name = $_POST['name'];
	$id = $_POST['id'];
	$update = $db->prepare("UPDATE ".TBLSERVICES." SET name =:name WHERE id=:id");
	$update->bindParam(':name', $name);
	$update->bindParam(':id', $id);
	$update->execute();
	header('location:manage-service.php?mesg=3');
	}
	
}

if(isset($_REQUEST['edit']))
{		
	$alias = $_REQUEST['edit'];
	$select = $db->prepare("SELECT * FROM ".TBLSERVICES." WHERE alias =:alias");
	$select->bindParam(':alias', $alias);
	$select->execute();
	$cols = $select->fetch();
}
?>
<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>Online Consultancy | Add New Service</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
<script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
<link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js"></script>
<script type="text/javascript" language="javascript" src="resources/demo.js"></script>
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('#example').DataTable();
} );
</script>
</head>
<body class=" theme-blue">
<!-- Demo page code -->
<script type="text/javascript">
$(function() {
var match = document.cookie.match(new RegExp('color=([^;]+)'));
if(match) var color = match[1];
if(color) {
$('body').removeClass(function (index, css) {
return (css.match (/\btheme-\S+/g) || []).join(' ')
})
$('body').addClass('theme-' + color);
}

$('[data-popover="true"]').popover({html: true});

});
</script>
<style>
table, th, td {
    border: 1px solid black;
}
</style>

<script type="text/javascript">
$(function() {
var uls = $('.sidebar-nav > ul > *').clone();
uls.addClass('visible-xs');
$('#main-menu').append(uls.clone());
});
</script>
<script src="js/jQuery.js" type="text/javascript"></script>
<script type="text/javascript">

// Change Your home URL..
home_url = 'http://localhost/Online-virtual-learning/admin';

/* *
*     fileName - ajax file name to be called by ajax method.
*     data - pass the infromation(like location-id , location-type) via data variable.
*     loadDataToDiv - id of the div to which the ajax responce is to be loaded.
* */
function ajax_call(fileName,data, loadDataToDiv) {
jQuery("#"+loadDataToDiv).html('<option selected="selected">-- -- -- Loding Data -- -- --</option>');

//  If you are changing counrty, make the state and city fields blank
if(loadDataToDiv=='state'){
jQuery('#city').html('');
jQuery('#state').html('');                    
}
//  If you are changing state, make the city fields blank
if(loadDataToDiv=='city'){
jQuery('#city').html('');
}

jQuery.post(home_url + '/' + fileName + '.php', data, function(result) {
jQuery('#' + loadDataToDiv).html(result);
});
}
</script>
<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
<script type="text/javascript">
function PreviewImage() {
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("fld_logo").files[0]);

oFReader.onload = function (oFREvent) {
document.getElementById("uploadPreview").src = oFREvent.target.result;
};
};
</script>
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<div class="navbar navbar-default" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="" href="manage-service.php">Manage Service</a></div>
<div class="navbar-collapse collapse" style="height: 1px;">
<ul id="main-menu" class="nav navbar-nav navbar-right">
<li class="dropdown hidden-xs">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo ucfirst($_SESSION['username']);?>
<i class="fa fa-caret-down"></i>
</a>

<ul class="dropdown-menu">
<li class="divider"></li>
<li class="dropdown-header">Super Admin</li>
<li class="divider"></li>
<li><a tabindex="-1" href="index.php">Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<?php include('includes/left-sidebar.php')?>     
<div class="content">
<div class="header">
<h1 class="page-title"><?php if($cols['id']){ ?>Edit Existing Service<?php }else{ ?>Add New <?php } ?></h1>
<ul class="breadcrumb">
<li><a href="manage.php">Home</a> </li>
<li><a href="manage.php">Manage Service</a> </li>
<li class="active"><?php if($cols['id']){ ?>Edit Existing Service<?php }else{?>Add New <?php } ?></li>
</ul>
</div>
<div class="main-content">
<form  name = "addnewform" id ="addnewform" action="" method="post" enctype="multipart/form-data" >
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>Name</td>
<td><input type="text" name="name" id="name" value="<?php echo tep_db_output($cols['name'])?>"></td>
</tr>
</table>
<br />
<div class="btn-toolbar list-toolbar">
<?php if($cols['id']){ ?>
<button class="btn btn-primary">Update</button>	
<input type="hidden" name="action" value="update">
<input type="hidden" name="id" value="<?php echo $cols['id'] ?>"> 
<?php 	
} 
else{
?>
<button class="btn btn-primary">Submit</button>
<input type="hidden" name="action" value="add">
<?php
}
?>
<a href="manage-service.php" class="btn btn-primary" title="Cancel" >Cancel</a>
</div>
</form>

<footer>
<hr>
</footer>
</div>
</div>
<script src="js/lib/jquery.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#addnewform").validate({ });
});
</script>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
$("[rel=tooltip]").tooltip();
$(function() {
$('.demo-cancel-click').click(function(){return false;});
});
</script>
</body>
</html>