<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
<?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">We are hiring</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">We are hiring</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  





<div class="container content __web-inspector-hide-shortcut__">
      <div class="row">
        <div class="col-md-12">
          <h4>Join the Rgyan.com Team!</h4>
                        <p class="text-justify" style="color:#fff">At Rgyan.com, we're building an Religious and Ritual startup that is delighting many Devotees across India. 

We provide wide information on theRitual and Religious of India.  We value creativity, hard work, initiative and radical ideas.

If this sounds exciting to you, then come have a chat with us ASAP</p>
        </div>
               
               
        <div class="col-md-12">
          <h4>Open Positions</h4>
                        <p class="text-justify" style="color:#fff">We're also looking for Summer Interns for our Operations and Engineering teams.
Location for Operations Interns: Gurgaon
If you're interested, please mail your resume to Support@Rgyan.com, and mention your preferred location and role
If there are no opportunities that fit your profile but you want to be part of an exciting journey, email us at Support@Rgyan.com with your resume/link to your profile. We're are on the lookout to enlist people into our organisation who are passionate and want to make a difference!
</p><br>
        </div>
            
              
      
      </div><!--/row-fluid-->
    </div>
  
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>