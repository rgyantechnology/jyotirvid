<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Pandit</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Pandit</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
  


<div class="container content __web-inspector-hide-shortcut__">
    <div class="row">
      <div class="col-md-12">
        <div class="headin_link">
          <div class="row">
            <div class="col-md-12">
              <h3 class="pull-left">Pandit</h3>
              <h4 class="pull-right headin_size" class="pull-right"><a href="search-result.php?category=pandit">Get In Touch With Pandits</a></h4>
            </div>
          </div>
        </div>
          <p class="text-justify pull-right gap-top" style="color:#fff;"><img class="img-responsive img-thumbnail pull-left gap-right" src="imges/priest.jpg">
                        Hinduism is a vast culture with variations of knowledge and takeaways. There are numerous sacred texts which are assigned for the Hindu followers by the intellectual ancestors. Reading and understanding them is not at all an easy task to perform. For absorbing them completely and going into the deep, you also need to be spiritually active.<br><br>
There are a group of specific people who always stay involved in religious and spiritual activities. They are known to be the preachers and carriers of religious knowledge. They are the great Pandits.<br><br>
Pandits preach many fields in Hinduism like the Vedic scriptures, dharma, Hindu philosophy and sacred music or mantras. They are spread across the globe and specialize in different categories. They are treated next to God by people. They conduct many auspicious events or rituals like marriages, birth, deaths, Pujas, Yajnyas and many more.
Finding a very experienced and spiritually connected Pandit is quite difficult. We have hence created a platform in which we will connect you to the most relevant Pandits according to your requirements. 

                        
                        
                        </p>
                        
        </div>
               

      
      </div><!--/row-fluid-->
    </div>
  
  <!--footer section start-->
   <?php include_once('pages/footer.php'); ?>