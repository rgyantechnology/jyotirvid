<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  
  <!--bradcome-->
  
  <div class="breadcrumbs breadcrumbs-light">
    <div class="container">
      <h1 class="pull-left">Privacy Policy</h1>
      <ul class="pull-right breadcrumb">
        <li><a href="<?php echo LINK_URL_HOME ?>">Home</a></li>
        <li class="active">Privacy Policy</li>
      </ul>
    </div>
  </div>
  <div class="clearfix"></div>
  <!--end of bradcome--> 
 <!--Terms & Conditions section-->
  
  
  
  <div class="container content __web-inspector-hide-shortcut__">
			<div class="row">
				<div class="col-md-12">
					<h4>PRIVACY POLICY</h4>
                        <p class="text-justify" style="color:#fff">This privacy policy sets out how Rgyan.com uses and protects any information that you give Rgyan.com when you use this website.<br>RGYAN.COM is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. We explain how we use customer information in this privacy policy statement.</p>
				</div>
               
               
				<div class="col-md-12">
					<h4> Note:</h4>
                        <p class="text-justify" style="color:#fff">(1)&nbsp;&nbsp;&nbsp;By visiting this website you agree to be bound by the terms and conditions of this privacy policy. If you do not agree please do not use or access our website.<br>(2)&nbsp;&nbsp;&nbsp;RGYAN.COM may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 1st Day of May, 2016.<br>(3)&nbsp;&nbsp;&nbsp;By mere use of the Website, you expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy. This Privacy Policy is incorporated into and subject to the Terms of Use.</p>
				</div>
            
                
               
                <div class="col-md-12">
					<h4>We may collect and store the following information:</h4>
                    <p class="text-justify" style="color:#fff"><b>.</b>&nbsp;&nbsp;Your personal information / data which is provided by you time to time, including e-mail id, mail address, first name, last name, telephone number, date of birth, physical contact information, and (depending on the service used) sometimes financial information, such as credit card or bank account numbers.<br><b>.</b>Your personal information / data which is provided by you time to time, including e-mail id, mail address, first name, last name, telephone number, date of birth, physical contact information, and (depending on the service used) sometimes financial information, such as credit card or bank account numbers;

<br><b>.</b>&nbsp;&nbsp;transactional information based on your activities on the Site (such as rental activity, Content you generate or that relates to your Account);

<br><b>.</b>&nbsp;&nbsp;community discussions, forum discussions, chats, dispute resolution, 9 sent to us, and communications otherwise transmitted through the Site;

<br><b>.</b>&nbsp;&nbsp;other information from your interaction with our Site, Services, Content and advertising, including computer and connection information, statistics on page views, traffic to and from the Site, IP address and standard web log information;

<br><b>.</b>&nbsp;&nbsp;additional information we ask you to submit to authenticate yourself or if we believe you are violating Site Policies;

<br><b>.</b>&nbsp;&nbsp;Cookies (to improve navigation by remembering information entered, such as passwords) Cookies are generally small temporary files stored on your hard disk and usually remain only for the duration of your session.

<br><b>.</b>&nbsp;&nbsp;Any optional information requested is clearly indicated as such on the online registration form.

All information uploaded by the user becomes public knowledge and RGYAN.COM may at its sole discretion include all or any portion of the listing intimated by a client for display on Website in any other media including the matrimonial sections of other platforms including but not limited to print media, community associations, websites, television shows, DTH/IPTV Services, mobile etc. and RGYAN.COM shall not be held liable for usage/publicity of such information / data.</p>
				</div>
              
                
                
                <div class="col-md-12">
					<h4>What we do with the information we gather</h4>
                        <p class="text-justify" style="color:#fff">You can browse the Website without telling us who you are or revealing any personal information about yourself. Once you give us your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional. You always have the option to not provide information by choosing not to use a particular service or feature on the Website. We may automatically track certain information about you based upon your behaviour on our Website. This information is compiled and analysed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on our Website or not), which URL you next go to (whether this URL is on our Website or not), your computer browser information, and your IP address.<br>We require this information to understand your needs and provide you with a better service, safe, efficient, smooth and in particular for the following reasons:<br><b>.</b>&nbsp;&nbsp;Internal record keeping.<br><b>.</b>&nbsp;&nbsp;We may use the information to improve our products and services.<br><b>.</b>&nbsp;&nbsp;We may periodically send promotional email about new products, special offers or other information which we think you may find interesting using the email address which you have provided. <br><b>.</b>&nbsp;&nbsp;From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.<br><b>.</b>&nbsp;&nbsp;We may use the information to customise the website according to your interests.</p>
				</div>

				<div class="col-md-12">
					<h4>Security</h4>
                        <p class="text-justify" style="color:#fff">We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
				</div>


				<div class="col-md-12">
					<h4>How we use cookies</h4>
                        <p class="text-justify" style="color:#fff">A cookie is a small file which asks permission to be placed on your computer\'s hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.<br><br>

We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.<br><br>

We also use cookies to allow you to enter your password less frequently during a session. Cookies can also help us provide information that is targeted to your interests. Most cookies are "session cookies," meaning that they are automatically deleted from your hard drive at the end of a session. You are always free to decline our cookies if your browser permits, although in that case you may not be able to use certain features on the Website and you may be required to re-enter your password more frequently during a session.<br><br>

Additionally, you may encounter "cookies" or other similar devices on certain pages of the Website that are placed by third parties. We do not control the use of cookies by third parties.<br><br>


Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.<br><br>

You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>
				</div>


				<div class="col-md-12">
					<h4>Links to other websites</h4>
                        <p class="text-justify" style="color:#fff">Our website may contain links to enable you to visit other websites of interest easily. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>
				</div>


				<div class="col-md-12">
					<h4>Controlling your personal information</h4>
                        <p class="text-justify" style="color:#fff">We may share personal information with our other corporate entities and affiliates to help detect and prevent identity theft, fraud and other potentially illegal acts; correlate related or multiple accounts to prevent abuse of our services; and to facilitate joint or co-branded services that you request where such services are provided by more than one corporate entity. Those entities and affiliates may not market to you as a result of such sharing unless you explicitly opt-in.<br><br>

We may disclose personal information if required to do so by law or in the good faith belief that such disclosure is reasonably necessary to respond to subpoenas, court
orders, or other legal process. We may disclose personal information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is reasonably necessary to: enforce our Terms or Privacy Policy; respond to claims that an advertisement, posting or other content violates the rights of a third party; or protect the rights, property or personal safety of our users or the general public.<br><br>

We and our affiliates will share / sell some or all of your personal information with another business entity should we (or our assets) plan to merge with, or be acquired by that business entity, or re-organization, amalgamation, restructuring of business. Should such a transaction occur that other business entity (or the new combined entity) will be required to follow this privacy policy with respect to your personal information.<br><br>

If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>
				</div>


				<div class="col-md-12">
					<h4>Advertisements on RGYAN.COM</h4>
                        <p class="text-justify" style="color:#fff">We use third-party advertising companies to serve ads when you visit our Website. These companies may use information (not including your name, address, email
address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you.</p>
				</div>

				<div class="col-md-12">
					<h4>Your Consent</h4>
                        <p class="text-justify" style="color:#fff">By using the Website and/ or by providing your information, you consent to the collection and use of the information you disclose on the Website in accordance with this Privacy Policy, including but not limited to Your consent for sharing your information as per this privacy policy.<br><br>

If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
				</div>

				<div class="col-md-12">
					<h4>Grievance Officer & Questions</h4>
                        <p class="text-justify" style="color:#fff">In accordance with Information Technology Act 2000 and rules made there under, the name and contact details of the Grievance Officer are provided below:<br><br>Devendar Agarwal<br>0124 2381700<br>Questions regarding this statement should be directed to the following address:<br>Religious Guardian Services Private Limited.<br>Plot No 400P, Sector 38, Near Medanta, Gurugram(HR) 122001.</p>
				</div>

				


      


      
			</div><!--/row-fluid-->
		</div>
  
    <!--Terms & Conditions section-->
  
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>