<?php 
date_default_timezone_set('Asia/Kolkata');
include_once('admin/includes/function.php');
include_once('pages/head.php');
?>
<div id="main">
  <!--  Start Header Section-->
  <?php include_once('pages/header.php');?>
  <!--end of header Section--> 
  
  <!--slider section satrt-->
  <?php include_once('pages/slider.php'); ?>
  <!-- End slider section-->
  
  <div class="clearfix"></div>
  <!--Service Section-->
  <div style="margin-top:0%" class="container">
    <div class="section_margin">
      <div class="row"> 
        
        <!--firstsection-->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails thumbnail-style">
            <div class="caption">
              <h3 class=""><a class="thumbnail_heading hover-effect" href="<?php echo LINK_URL_HOME?>dainikpanchang/index.php?id=panchang">Today's Panchang</a></h3>
            </div>
            <div class="thumbnail-img">
              <div class="overflow-hidden">
				<?php 
				//date_default_timezone_set('Asia/Kolkata');
				$date = date('d/m/y');
				$time = date('h:i:a');
				$url = "http://api.panchang.click/v0.4/panchangapi?date=".$date."&time=".$time."&tz=05:30&userid=dileepk&authcode=ec1889b1456ae21a625730b8995679f1";	
				$data = file_get_contents($url);
				$data = json_decode($data,true);
				$date = date('d-m-y');
				$time = time();
				$latitude = "28.4594965";
				$longitude = "77.02663830000006";
				$sun_info = date_sun_info(time(), $latitude, $longitude);
				foreach ($sun_info as $key => $val) {
				$suntime[] = date("H:i:s", $val);
				}
				//date_sun_info ( int $time , float $latitude , float $longitude );
				?>
				<div class="service-box"> 
				<p style="color:#fff; padding: 4px 10px; font-size:14px" class="text-muted">Gurgaon, Haryana, India <br>
				<?php echo date ('l');?>, <?php echo date ('d F Y');?> <br>
				Sunrise : <?php echo $suntime[0]?> | Sunset : <?php echo $suntime[1]?><br>
				Shaka Samvat : 1938 Durmukha <br>
				| Vikram Samvat : 2073 Saumya<br><br />
				</p>
				</div>
				<a style="margin: 12px 44px 0px 0px;" class="btn btn-group-sm btn-danger pull-right" href="https://rgyan.com/panchang/">See Panchang</a> </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails thumbnail-style">
            <div class="caption">
              <h3><a class="thumbnail_heading hover-effect" href="<?php echo LINK_URL_HOME ?>matchmaking" target = "new">Match Making</a></h3>
            </div>
            <div class="thumbnail-img">
              <div class="overflow-hidden"> <img class="img-responsive text-center" src="<?php echo LINK_URL_HOME ?>img/match-making.jpg"  alt=""> </div>
              <a class="btn-more hover-effect" href="<?php echo LINK_URL_HOME ?>matchmaking">match profile +</a> </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails">
            <div class="thumbnail-img">
              <div class="overflow-hidden"><a href="#myModal" data-toggle="modal"> <img class="img-responsive" src="<?php echo LINK_URL_HOME ?>img/vastu-tips.jpg" width="365" height = "264" alt=""></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--end of firstsection--> 
    
    <!--Start of section 3-->
    <?php echo servicesection(); ?>
    
    <!--end of second section-->
    
    <div class="section_margin">
      <div class="row margin-bottom-20"> 
        <!--Third section-->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails thumbnail-style">
            <div class="caption">
              <h3><a class="thumbnail_heading hover-effect" href="#">Recommended Pujas</a></h3>
            </div>
            <div style="background:#000; border:none;" class="panel panel-default">
              <div style="padding: 33px 10px;margin-top: 1px;" class="panel-body">
                <div class="overflow-hidden">
                  <div class="puja_box"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>img/puja-1.jpg" alt=""></div>
                  <div class="puja_box1">
                    <p style=" ">Sri Saraswathi Amman Sannadhi<br>
                      Puja For Education</p>
                  </div>
                  <div class="puja_box3" style=""> <a href="">
                    <p><i  style="margin:2px" class="fa fa-inr" aria-hidden="true"></i>1151 </p>
                    </a>
                    <div class="puja_box4"><a href="">
                      <p style=""><a href = " http://epuja.co.in/product-details.php?puja_id=796&page=Saraswathi-Amman-Sannadhi-Athitheeshwarar-Temple" style="color:#000;" target = "new">Book</a></p>
                      </a></div>
                  </div>
                </div>
                <hr class="hr_size">
                <div class="overflow-hidden">
                  <div class="puja_box"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>img/puja-2.jpg" alt=""></div>
                  <div class="puja_box1">
                    <p style=" ">Sri Mahalaxmi Temple<br>
                      Puja For Wealth & Prosperity</p>
                  </div>
                  <div class="puja_box3" style=""> <a href="">
                    <p><i  style="margin:2px" class="fa fa-inr" aria-hidden="true"></i>1601 </p>
                    </a>
                    <div class="puja_box4"><a href="">
                      <p style=""><a href = "http://epuja.co.in/product-details.php?puja_id=849&page=Puja-For-Wealth-&-Prosperity" target = "new" style="color:#000;">Book</a></p>
                      </a></div>
                  </div>
                </div>
               <hr class="hr_size">
                <div class="overflow-hidden">
                  <div class="puja_box"><img class="img-responsive" src="<?php echo LINK_URL_HOME ?>img/puja-4.jpg" alt=""></div>
                  <div class="puja_box1">
                    <p style=" ">In Kerala Maruthorvattam<br>
                     Puja In 4 Temples...</p>
                  </div>
                  <div class="puja_box3" style=""> <a href="">
                    <p><i  style="margin:2px" class="fa fa-inr" aria-hidden="true"></i>3751 </p>
                    </a>
                    <div class="puja_box4"><a href="">
                      <p style=""><a href = "http://epuja.co.in/product-details.php?puja_id=864&page=Relief-From-Illness-Health-Related-Problems" target = "new" style="color:#000;">Book</a></p>
                      </a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails thumbnail-style ">
            <div class="caption">
              <h3><a class="thumbnail_heading hover-effect" href="#">Top Astrologers</a></h3>
            </div>
           <?php echo topstrologer(); ?>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnails">
            <div class="thumbnail-img">
              <div class="overflow-hidden"> <a href="https://rgyan.com/blogs/" target = "new"><img style="border:1px solid #f3ddbc" class="img-responsive" src="<?php echo LINK_URL_HOME ?>img/blog.jpg" alt=""> </div>
            </div>
          </div>
        </div>
        <!--end of Third section--> 
      </div>
    </div>
    <!--end of row--> 
  </div>
  <!--Service Section end--> 
  
  <!--Horscope-->
  <!--<section class="asstro">
    <div class="container">
      <div style="margin:30px 0px 0px 0px;"><img class="img-responsive" src="<?php //echo LINK_URL_HOME ?>imges/astrologer2.png" alt=""></div>
      <div class="row">
        <div class="col-md-12">
          <h3 class="head_ing">Daily Horoscope</h3>
        </div>
      </div>
      <?php //echo horoscopesign(); ?>
     </div>
  </section>-->
  <!--Horscope conatiner-->
  
  <div class="clearfix"></div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:red;">Vastu Tips</h4>
        </div>
        <div class="modal-body">
          <p style="color:#000;"><iframe src="https://rgyan.com/astrologer/img/Vaastu_Tips_for_Daily.pdf" height="450" width="550"></iframe></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--footer section start-->
  <?php include_once('pages/footer.php'); ?>