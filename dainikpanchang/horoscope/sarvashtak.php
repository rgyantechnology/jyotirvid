<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Sarvashtak varga Details";
require_once("../header.php");
$planet = null;
$api = new ApiCall();

$data = $api->horoscopeApiCall('sarvashtak');

?>
<div class="UI-II ng-scope">
    <h2> Sarvashtak Varga</h2>
    <div class="table-bordered table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Planet <span class="fa a-long-arrow-right"></span> <br><span class="fa fa-long-arrow-bottom"></span> Zodiac </th>
                <th>Sun</th>
                <th>Moon</th>
                <th>Mars</th>
                <th>Mercury</th>
                <th>Jupiter</th>
                <th>Venus</th>
                <th>Saturn</th>
                <th>Ascendant</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="bold">Aries</td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aries']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Taurus</td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['taurus']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Gemini</td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['gemini']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Cancer</td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['cancer']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Leo</td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['leo']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Virgo</td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['virgo']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Libra</td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['libra']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Scorpio</td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['scorpio']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Sagittarius</td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['sagittarius']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Capricorn</td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['capricorn']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Aquarius</td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['aquarius']['total']?></td>
            </tr>
            <tr>
                <td class="bold">Pisces</td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['sun']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['moon']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['mars']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['mercury']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['jupiter']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['venus']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['saturn']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['ascendant']?></td>
                <td class="ng-binding"><?=$data['ashtak_points']['pisces']['total']?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<?php require_once("../footer.php"); ?>

