<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Sadhesati remedies";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('sadhesati_remedies');

?>
<div class="UI-II ng-scope">

    <div class="matching-conculsion-status">
        <h2>What is SadheSati ?</h2>
        <div class="ashtakoot-conculsion-report">
            <p class="text-left ng-binding"><?=$data['what_is_sadhesati']?></p>
        </div>
        <div class="matching-conculsion-status">
            <h2>Remedies</h2>
            <div class="ashtakoot-conculsion-report">
                <?php
                for($i=0;$i<count($data['remedies']);$i++)
                {
                ?>
                    <p class="text-left ng-binding"><?=$data['remedies'][$i]?></p>
                <?php

                }
                ?>

            </div>
    </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>