<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Rudraksha Suggestion For You";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('rudraksha_suggestion');

?>

<div class="UI-II ng-scope">
    <h2>Rudraksha Suggestion</h2>
    <div class="rudrakshaImageContainer">
        <figure>
            <img src="https://www.vedicrishi.in/<?=$data['img_url']?>" alt="Rudraksha Remedy">
            <figcaption class="figcaption ng-binding"><?=$data['name']?>{{name}}</figcaption>
        </figure>
    </div>
    <div class="spacer-40 center"></div>
    <h3 class="remedies-title ng-binding">You are recommended to wear <?=$data['name']?>.</h3>
    <p style="line-height: 2.2em; color: #878787;" class="ng-binding"><?=$data['detail']?></p>
</div>
<?php require_once("../footer.php"); ?>