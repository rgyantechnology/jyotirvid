<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Manglik Details";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('manglik');
?>
    <div class="row">
        <div class="col-md-6">
            <div class="UI-II ng-scope">

                <h2>Manglik Dosha Analysis</h2>
                <div class="kalsarpa-desc block m-auto">
                    <h3 style="font-size: 1.4em;">Percentage</h3>
                    <p class="status-false"><?=$data['percentage_manglik_after_cancellation']?> %</p>
                </div>
                <div class="clearfix spacer-40"></div>

                <div class="matching-conculsion-status">
                    <h3>Manglik Effect</h3>
                    <p>Manglik dosha is <?=$data['manglik_status']?></p>
                    <h3>Manglik Analysis</h3>
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$data['manglik_report']?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <h2>Based On House</h2>
                <?php
                for($i = 0; $i<count($data['manglik_present_rule']['based_on_house']);$i++)
                {
                ?>
                    <p class="text-left ng-scope ng-binding">- <?=$data['manglik_present_rule']['based_on_house'][$i];?></p>
                <?php
                }
                ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <h2>Based On Aspects</h2>
                <?php
                for($i = 0; $i<count($data['manglik_present_rule']['based_on_aspect']);$i++)
                {
                    ?>
                    <p class="text-left ng-scope ng-binding">- <?=$data['manglik_present_rule']['based_on_aspect'][$i];?></p>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>