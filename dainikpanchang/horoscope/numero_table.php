<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Numerology Number For You";
require_once("../header.php");

$api = new ApiCall();
$data = $api->numerologyApiCall('numero_table');
?>

<div class="UI-II ng-scope">

<div class="row">
    <div class="col-sm-12 ">
        <div class="col-sm-4 text-center">
            <h4>Radical Number</h4>
            <div class="circleshape" style="margin-top:5%">
                <p class="circon1"><?=$data['radical_number']?></p>
            </div>
        </div>

        <div class="col-sm-4 text-center">
            <h4>Destiny Number</h4>
            <div class="circleshape" style="margin-top:5% ; background-color: salmon">
                <p class="text-center circon1"><?=$data['destiny_number']?></p>
            </div>
        </div>

        <div class="col-sm-4 text-center">
            <h4>Name Number</h4>
            <div class="circleshape" style="margin-top:5%; background-color: plum">
                <p class="text-center circon1"><?=$data['name_number']?></p>
            </div>
        </div>
        <div class="clearfix spacer-40"></div>

        <div class="col-sm-12" style="margin-top: 40px">
            <h2 >Numerology Table</h2>
            <div class="table-bordered table-responsive">
                <table class="table">

                    <thead>
                    <tr>
                        <th class=>Numerology Report</th>
                        <th class="">Values</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Your Name</td>
                        <td><?=$data['name']?></td>
                    </tr>
                    <tr>
                        <td>Your Date of Birth</td>
                        <td><?=$data['date']?></td>
                    </tr>
                    <tr>
                        <td>Radical Number</td>
                        <td><?=$data['radical_number']?></td>
                    </tr>
                    <tr>
                        <td>Radical Ruler</td>
                        <td><?=$data['radical_ruler']?></td>
                    </tr>
                    <tr>
                        <td>Friendly Numbers</td>
                        <td><?=$data['friendly_num']?></td>
                    </tr>
                    <tr>
                        <td>Neutral Numbers</td>
                        <td><?=$data['neutral_num']?></td>
                    </tr>
                    <tr>
                        <td>Evil Numbers</td>
                        <td><?=$data['evil_num']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Days</td>
                        <td><?=$data['fav_day']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Stone</td>
                        <td><?=$data['fav_stone']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Sub Stone</td>
                        <td><?=$data['fav_substone']?></td>
                    </tr>
                    <tr>
                        <td>Favourable God</td>
                        <td><?=$data['fav_god']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Metal</td>
                        <td><?=$data['fav_metal']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Color</td>
                        <td><?=$data['fav_color']?></td>
                    </tr>
                    <tr>
                        <td>Favourable Mantra</td>
                        <td><?=$data['fav_mantra']?></td>
                    </tr>
                    </tbody>


                </table>
            </div>

        </div>
        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>