<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Sadhesati current status";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('sadhesati_current_status');
?>
    <div class="row">
        <div class="col-md-6">
            <div class="UI-II ng-scope">

                <h2>Kalsarpa Dosha Analysis</h2>
                <div class="kalsarpa-desc block m-auto">
                    <h3>Present</h3>
                    <?php
                    if($data['sadhesati_status'])
                    {
                        ?>
                        <p class="status-false">YES</p>
                        <?php
                    }
                    else
                    {
                        ?>
                        <p class="status-true">NO</p>
                        <?php
                    }
                    ?>
                    
                </div>
                <div class="clearfix spacer-40"></div>

                <div class="matching-conculsion-status">
                    <h3>Other Details : </h3>
                    <div class="gemestoneRemediesDetails">
                        <span class="ng-binding"><strong>Is Saturn Retrograde? :</strong><?php $data['is_saturn_retrograde']?'YES':'NO';?></span>
                        <span class="ng-binding"><strong>Consideration Date:</strong><?=$data['consideration_date']?></span>
                        <span class="ng-binding"><strong>Moon Sign :</strong><?=$data['moon_sign']?></span>
                        <span class="ng-binding"><strong>Saturn Sign :</strong><?=$data['saturn_sign']?></span>
                    </div>
                    <h3>Conclusion:</h3>
                    <div class="ashtakoot-conculsion-report">
                        <p class="text-left ng-binding"><?=$data['is_undergoing_sadhesati']?></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <h2>What is sadhesati dosha?</h2>
                <p>
                    Sadhe Sati refers to the seven-and-a-half year period in which Saturn moves through three signs, the moon sign, one before the moon and the one after it. Sadhe Sati starts when Saturn (Shani) enters the 12th sign from the birth Moon sign and ends when Saturn leaves 2nd sign from the birth Moon sign. Since Saturn approximately takes around two and half years to transit a sign which is called Shanis dhaiya it takes around seven and half year to transit three signs and that is why it is known as Sadhe Sati. Generally Sade-Sati comes thrice in a horoscope in the life time - first in childhood, second in youth & third in old-age. First Sade-Sati has effect on education & parents. Second Sade-Sati has effect on profession, finance & family. The last one affects health more than anything else.
                </p>
            </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>