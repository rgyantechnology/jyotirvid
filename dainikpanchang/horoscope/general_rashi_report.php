<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Basic Details";
require_once("../header.php");
$planet = null;
$api = new ApiCall();
if(isset($_POST['planet'])){
    $planet = $_POST['planet'];
    $data = $api->horoscopeApiCall('general_rashi_report/'.$planet);
}
else
{
    $planet = "Moon";
    $data = $api->horoscopeApiCall('general_rashi_report/Moon');
}

?>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="UI-II ng-scope">
            <div class="myForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 col-md-offset-3" >
                            <form action="" method="post">
                                <h3 class="text-center">Select planet</h3>
                                <select id="planetId" name="planet" onchange="this.form.submit()">
                                    <option value="Moon" <?php if($planet == "Moon"){ echo " selected"; }?>>Sign Report For Moon</option>
                                    <option value="Mars" <?php if($planet == "Mars"){ echo " selected"; }?>>Sign Report For Mars</option>
                                    <option value="Mercury" <?php if($planet == "Mercury"){ echo " selected"; }?>>Sign Report For Mercury</option>
                                    <option value="Jupiter" <?php if($planet == "Jupiter"){ echo " selected"; }?>>Sign Report For Jupiter</option>
                                    <option value="Venus" <?php if($planet == "Venus"){ echo " selected"; }?>>Sign Report For Venus</option>
                                    <option value="Saturn" <?php if($planet == "Saturn"){ echo " selected"; }?>>Sign Report For Saturn</option>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix spacer-30"></div>

            <h2><?=$data['planet']?> sign report  </h2>

            <p style="line-height: 2.2em; color: #878787;" class="ng-binding"><?=$data['rashi_report']?></p>

        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<?php require_once("../footer.php"); ?>