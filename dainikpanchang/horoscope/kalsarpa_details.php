<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Kalsarpa Dosha Details";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('kalsarpa_details');
?>


<div class="row">
    <div class="col-md-6">
        <div class="UI-II ng-scope">

            <h2>Kalsarpa Dosha Analysis</h2>
            <div class="kalsarpa-desc block m-auto">
                <h3>Present</h3>
                <?php
                if($data['present'])
                {
               ?>
                    <p class="status-false">YES</p>
                <?php
                }
                else
                {
                ?>
                    <p class="status-true">NO</p>
                <?php
                }
                ?>


            </div>
        <div class="clearfix spacer-40"></div>

        <div class="matching-conculsion-status">
            <?php
            if($data['present'])
            {
            ?>
                <h3>Type</h3>
                <p class="status-false ng-binding"><?=$data['type']?></p>
            <?php
            }
            ?>

            <h3>Description</h3>
            <div class="ashtakoot-conculsion-report">
                <p class="text-left ng-binding"><?=$data['one_line']?></p>
            </div>
            <?php
            if($data['present'])
            {
            ?>
                <h3>Report</h3>
                <div class="ashtakoot-conculsion-report">
                    <p class="text-left ng-binding"><?=$data['report']['report']?></p>
                </div>
            <?php
            }
            ?>

        </div>
    </div>
</div>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2>What is kalsarpa dosha?</h2>

            <p>
                If all the 7 planets are situated between Rahu and Ketu then Kaal Sarp Yog is formed.

                According to the situation of Rahu in 12 houses of horoscope there are Kaal Sarp Yogas of 12 types.
                <br>
                <strong>These are :</strong>
                <br>
                <strong>
                    Anant,
                    Kulik,
                    Vasuki,
                    Shankhpal,
                    Padma,
                    Mahapadma,
                    Takshak,
                    Karkotak,
                    Shankhchud,
                    Ghaatak,
                    Vishdhar
                    Sheshnag.
                </strong>
                <br>
                The Kaal Sarp Yog is of two types- Ascending and Descending. If all the 7 planets are eaten away by Rahu's mouth then it is Ascending Kaal Sarp Yog. If all planets are situated in back of Rahu then Descending Kaal Sarp Yog is formed.
            </p>
        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>