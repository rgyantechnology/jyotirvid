<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Char Dasha Details";
require_once("../header.php");

$api = new ApiCall();
$current_vdasha = $api->horoscopeApiCall('current_chardasha');
$major_vdasha = $api->horoscopeApiCall('major_chardasha');
?>


<div class="row">
    <div class="col-md-6">
        <div class="UI-II">
            <div class="Crnt-Dasha">
                <h2> Current Yogini Dasha</h2>

                <div class="dashaFirstlevelData dashaData">
                    <div class="dashaFirstLevel dashaPlanet pull-left ng-binding"><?=$current_vdasha['major_dasha']['sign_name']?></div>
                    <b>Mahadasha</b>
                    <p class="ng-binding"><?=$current_vdasha['major_dasha']['start_date']?> -- <?=$current_vdasha['major_dasha']['end_date']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaSecondlevelData dashaData">
                    <div class="dashaSecondLevel dashaPlanet ng-binding"><?=$current_vdasha['sub_dasha']['sign_name']?></div>

                    <b>Antar Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['sub_dasha']['start_date']?> -- <?=$current_vdasha['sub_dasha']['end_date']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>
                <div class="dashaThirdlevelData dashaData">
                    <div class="dashaThirdLevel dashaPlanet ng-binding"><?=$current_vdasha['sub_sub_dasha']['sign_name']?></div>

                    <b>Pratyantar Dasha</b>
                    <p class="ng-binding"><?=$current_vdasha['sub_sub_dasha']['start_date']?> -- <?=$current_vdasha['sub_sub_dasha']['end_date']?></p>
                </div>
                <div style="clear:both;"></div>
                <div></div>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="UI-II ng-scope">
            <h2> Major Yogini Dasha </h2>
            <div class="table-bordered table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Sign </th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Duration</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $retro = " ";
                    for($i = 0; $i < count($major_vdasha); $i++)
                    {

                        echo '<tr>';
                        echo '<td class="bold">'.$major_vdasha[$i]['sign_name'].'</td>';
                        echo '<td>'.$major_vdasha[$i]['start_date'].'</td>';
                        echo '<td>'.$major_vdasha[$i]['end_date'].'</td>';
                        echo '<td>'.$major_vdasha[$i]['duration'].'</td>';
                        echo '</tr>';
                    }

                    ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<?php require_once("../footer.php"); ?>