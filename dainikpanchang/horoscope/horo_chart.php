<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Kundli";
require_once("../header.php");

$api = new ApiCall();
$sun_chart = json_encode($api->horoscopeApiCall('horo_chart/SUN'));
$moon_chart = json_encode($api->horoscopeApiCall('horo_chart/MOON'));
$chalit = json_encode($api->horoscopeApiCall('horo_chart/chalit'));
$navamansha = json_encode($api->horoscopeApiCall('horo_chart/D9'));
$data = ($api->horoscopeApiCall('horo_chart/D1'));
$data_encoded = json_encode($data);
?>
<div class="row">
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Lagna Chart</h2>
            <div id="northChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Lagna Chart (South Style)</h2>
            <div id="southChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Chalit Chart</h2>
            <div id="chalitChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Navamansha Chart</h2>
            <div id="navamanshaChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Sun Chart</h2>
            <div id="sunChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="UI-II" style="overflow: hidden;">
            <h2>Moon Chart</h2>
            <div id="moonChart" style="width: 95%; margin: 0 auto;"></div>
        </div>
    </div>
</div>
<!--Footer Content-->
<?php require_once("../footer.php"); ?>
<script type="text/javascript">

    var options = {

        lineColor : '#666',
        planetColor : '#000',
        signColor : '#000',
        width : $('.UI-II').width()-22

    };

    drawNorthChart(getPlanetArray(<?php echo $data_encoded;?>), getSignArray(<?php echo $data_encoded;?>), options,'#northChart');
    drawSouthChart(getSignPlanetArray(<?php echo $data_encoded;?>),getSignArray(<?php echo $data[0]['sign'];?>), options,'#southChart');
    drawNorthChart(getPlanetArray(<?php echo $sun_chart;?>), getSignArray(<?php echo $sun_chart;?>), options,'#sunChart');
    drawNorthChart(getPlanetArray(<?php echo $moon_chart;?>), getSignArray(<?php echo $moon_chart;?>), options,'#moonChart');
    drawNorthChart(getPlanetArray(<?php echo $chalit;?>), getSignArray(<?php echo $chalit;?>), options,'#chalitChart');
    drawNorthChart(getPlanetArray(<?php echo $navamansha;?>), getSignArray(<?php echo $navamansha;?>), options,'#navamanshaChart');

</script>