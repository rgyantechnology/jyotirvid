<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Basic Gems Suggestion For You";
require_once("../header.php");

$api = new ApiCall();
$data = $api->horoscopeApiCall('basic_gem_suggestion');

?>

<div class="UI-II ng-scope">
    <h2>Gemstones Remedies</h2>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <figure>
                   <img src="https://vedicrishi.in/img/member/<?=$data['LIFE']['name']?>.jpg" alt="Remedial Astrology" class="gemestoneImg ng-scope">
                </figure>
            </div>
            <div class="col-sm-10 gemestoneRemedies-container">
                <p class="remedies-legends">Life Stone : <span class="figcaption ng-binding"><?=$data['LIFE']['name']?></span></p>
                <div class="gemestoneRemediesDetails">
                    <span class="ng-binding"><strong>Substitute :</strong><?=$data['LIFE']['semi_gem']?></span>
                    <span class="ng-binding"><strong>Finger :</strong><?=$data['LIFE']['wear_finger']?></span>
                    <span class="ng-binding"><strong>Weight :</strong><?=$data['LIFE']['weight_caret']?></span>
                    <span class="ng-binding"><strong>Day : </strong><?=$data['LIFE']['wear_day']?></span>
                    <span class="ng-binding"><strong>Deity :</strong><?=$data['LIFE']['gem_deity']?></span>
                    <span class="ng-binding"><strong>Metal :</strong><?=$data['LIFE']['wear_metal']?></span>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">
                <figure>
                    <img src="https://vedicrishi.in/img/member/<?=$data['BENEFIC']['name']?>.jpg" alt="Remedial Astrology" class="gemestoneImg ng-scope">
                </figure>
            </div>
            <div class="col-sm-10 gemestoneRemedies-container">
                <p class="remedies-legends">Benefic Stone : <span class="figcaption ng-binding"><?=$data['BENEFIC']['name']?></span></p>
                <div class="gemestoneRemediesDetails">
                    <span class="ng-binding"><strong>Substitute :</strong><?=$data['BENEFIC']['semi_gem']?></span>
                    <span class="ng-binding"><strong>Finger :</strong><?=$data['BENEFIC']['wear_finger']?></span>
                    <span class="ng-binding"><strong>Weight :</strong><?=$data['BENEFIC']['weight_caret']?></span>
                    <span class="ng-binding"><strong>Day : </strong><?=$data['BENEFIC']['wear_day']?></span>
                    <span class="ng-binding"><strong>Deity :</strong><?=$data['BENEFIC']['gem_deity']?></span>
                    <span class="ng-binding"><strong>Metal :</strong><?=$data['BENEFIC']['wear_metal']?></span>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">
                <figure>
                    <img src="https://vedicrishi.in/img/member/<?=$data['LUCKY']['name']?>.jpg" alt="Remedial Astrology" class="gemestoneImg ng-scope">
                </figure>
            </div>
            <div class="col-sm-10 gemestoneRemedies-container">
                <p class="remedies-legends">Lucky Stone : <span class="figcaption ng-binding"><?=$data['LUCKY']['name']?></span></p>
                <div class="gemestoneRemediesDetails">
                    <span class="ng-binding"><strong>Substitute :</strong><?=$data['LUCKY']['semi_gem']?></span>
                    <span class="ng-binding"><strong>Finger :</strong><?=$data['LUCKY']['wear_finger']?></span>
                    <span class="ng-binding"><strong>Weight :</strong><?=$data['LUCKY']['weight_caret']?></span>
                    <span class="ng-binding"><strong>Day : </strong><?=$data['LUCKY']['wear_day']?></span>
                    <span class="ng-binding"><strong>Deity :</strong><?=$data['LUCKY']['gem_deity']?></span>
                    <span class="ng-binding"><strong>Metal :</strong><?=$data['LUCKY']['wear_metal']?></span>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<?php require_once("../footer.php"); ?>