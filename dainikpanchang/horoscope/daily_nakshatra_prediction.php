<div id="nak"></div>

<script type="text/x-handlebars-template" id="nakTpl">

    <div class="row">
        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <h2>Moon Sign And Nakshatra</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-5 text-center">
                            <div class="birth-sign-nak nak-star text-success  ng-binding" style="padding: 40px 0;font-size: 1.3em;">{{birth_moon_sign}}</div>
                            <p style="color: #DD2323;margin-top: 10px;font-size: 1.3em;">Moon Sign</p>
                        </div>
                        <div class="col-sm-6 col-sm-offset-1 text-center">
                            <div class="birth-sign-nak nak-star text-success ng-binding" style="padding: 40px 0">{{birth_moon_nakshatra}}</div>
                            <p style="color: #3f87dd;margin-top: 10px;font-size: 1.3em;">Nakshatra</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="UI-II ng-scope">
                <h2>Health</h2>
                <p class="ng-binding">{{prediction.health}}</p>
            </div>
            <div class="UI-II ng-scope">
                <h2>Personal Life</h2>
                <p class="ng-binding">{{prediction.personal_life}}</p>
            </div>
            <div class="UI-II ng-scope">
                <h2>Travel</h2>
                <p class="ng-binding">{{prediction.travel}}</p>
            </div>

        </div>

        <div class="col-md-6">
            <div class="UI-II ng-scope">
                <h2>Profession</h2>
                <p class="ng-binding">{{prediction.profession}}</p>
            </div>
            <div class="UI-II ng-scope">
                <h2>Luck</h2>
                <p class="ng-binding">{{prediction.luck}}</p>
            </div>
            <div class="UI-II ng-scope">
                <h2>Emotions</h2>
                <p class="ng-binding">{{prediction.emotions}}</p>
            </div>
        </div>
    </div>

</script>

<script type="text/javascript">

    getNakshatra();

</script>