<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Biorhythms Details";
require_once("../header.php");
$planet = null;
$api = new ApiCall();

$data = $api->horoscopeApiCall('biorhythm');

?>
    <div class="UI-II ng-scope">
        <h2>BioRhythm Status</h2>

        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="col-sm-3">
                    <h3>Physical</h3>
                    <div class="circleshape" style="margin-top: 15%">
                        <p class="text-center circon"><?=$data['physical']['percent']?>%</p>
                    </div>
                </div>

                <div class="col-sm-3">
                    <h3>Emotional</h3>
                    <div class="circleshape" style="margin-top: 15% ; background-color: salmon">
                        <p class="text-center circon"><?=$data['emotional']['percent']?>%</p>
                    </div>
                </div>

                <div class="col-sm-3">
                    <h3>Intellectual</h3>
                    <div class="circleshape" style="margin-top: 15%; background-color: plum">
                        <p class="text-center circon"><?=$data['intellectual']['percent']?>%</p>
                    </div>
                </div>

                <div class="col-sm-3">
                    <h3>Average</h3>
                    <div class="circleshape" style="margin-top: 15%; background-color: lightseagreen">
                        <p class="text-center circon"><?=$data['average']['percent']?>%</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>