<?php
session_start();

class SessionData{

    public static function getHoroscopeRequestData()
    {
        if($_SESSION['isHoroscopeDataSet'])
        {
            return (array(
                "status"=>true,
                "name"=>$_SESSION['name'],
                "day"=>$_SESSION['day'],
                "month"=>$_SESSION['month'],
                "year"=>$_SESSION['year'],
                "hour"=>$_SESSION['hour'],
                "minute"=>$_SESSION['minute'],
                "latitude"=>$_SESSION['latitude'],
                "longitude"=>$_SESSION['longitude'],
                "timezone"=>$_SESSION['timezone']
            ));
        }
        else
        {
            return json_encode(array("status"=>false));
        }
    }

    public static function getPanchangRequestData()
    {
        if($_SESSION['isPanchangDataSet'])
        {
            return (array(
                "status"=>true,
                "day"=>$_SESSION['day'],
                "month"=>$_SESSION['month'],
                "year"=>$_SESSION['year'],
                "latitude"=>$_SESSION['latitude'],
                "longitude"=>$_SESSION['longitude'],
                "timezone"=>$_SESSION['timezone']
            ));
        }
        else
        {
            return json_encode(array("status"=>false));
        }
    }

    public static function getMatchingRequestData()
    {
        if($_SESSION['isMatchingDataSet'])
        {
            return (array(
                "status"=>true,
                "male"=>array(
                    "date"=>$_SESSION['day'],
                    "month"=>$_SESSION['month'],
                    "year"=>$_SESSION['year'],
                    "hour"=>$_SESSION['hour'],
                    "minute"=>$_SESSION['minute'],
                    "latitude"=>$_SESSION['latitude'],
                    "longitude"=>$_SESSION['longitude'],
                    "timezone"=>$_SESSION['timezone']
                ),
                "female"=>array(
                    "date"=>$_SESSION['f_day'],
                    "month"=>$_SESSION['f_month'],
                    "year"=>$_SESSION['f_year'],
                    "hour"=>$_SESSION['f_hour'],
                    "minute"=>$_SESSION['f_minute'],
                    "latitude"=>$_SESSION['f_latitude'],
                    "longitude"=>$_SESSION['f_longitude'],
                    "timezone"=>$_SESSION['f_timezone']
                )

            ));
        }
        else
        {
            return json_encode(array("status"=>false));
        }
    }

}