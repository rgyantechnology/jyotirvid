<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Panchang Planetary Details";
require_once "../panchang_header.php";

$api = new ApiCall();
$data = $api->panchangApiCall('chaughadiya_muhurta');
?>
    <div class="UI-II ng-scope">
        <h2 class="text-center">Chaughadiya Muhurta</h2>
        <div class="col-md-6">
            <h4 class="text-center">Day Chaughadiya</h4><div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr>
                    <th>Chaughadiya Name</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for($i=0;$i<count($data['chaughadiya']['day']);$i++)
                {
                ?>
                    <tr class="<?=$data['chaughadiya']['day'][$i]['muhurta']?>">
                        <td class="dark-td"><?=$data['chaughadiya']['day'][$i]['muhurta']?></td>
                        <td class="ng-binding"><?=$data['chaughadiya']['day'][$i]['time']?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        </div>
        <div class="col-md-6">
            <h4 class="text-center">Night Chaughadiya</h4><div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                <tr>
                    <th>Chaughadiya Name</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                for($i=0;$i<count($data['chaughadiya']['night']);$i++)
                {
                    ?>
                    <tr class="<?=$data['chaughadiya']['night'][$i]['muhurta']?>">
                        <td class="dark-td"><?=$data['chaughadiya']['night'][$i]['muhurta']?></td>
                        <td class="ng-binding"><?=$data['chaughadiya']['night'][$i]['time']?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
<?php require_once("../footer.php"); ?>