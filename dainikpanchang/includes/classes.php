<?php
class DB
     {
		var $host = 'localhost';
		var $user = 'testastro';
		var $password = 'testastro@123';
		var $database = 'testastro';
		
		
		var $persist = false;
        var $conn=NULL;
        var $result=false;
        var $fields;
        var $check_fields;
        var $tbname;
        var $addNewFlag=false;

        function addNew($table_name)
        {
           $this->fields=array();
           $this->addNewFlag=true;
           $this->tbname=$table_name;
        }
        function edit($table_name)
        {
           $this->fields=array();
           $this->check_fields=array();
           $this->addNewFlag=false;
           $this->tbname=$table_name;
        }
        function update()
        {
         foreach($this->fields as $field_name=>$value)
           $qry.=$field_name."='".$value."',";
         $qry=substr($qry,0,strlen($qry)-1);

          if($this->addNewFlag)
            $qry="INSERT INTO ".$this->tbname." SET ".$qry;
          else
          {
           $qry="UPDATE ".$this->tbname." SET ".$qry;
           if(count($this->check_fields)>0)
           {
               $qry.=" WHERE ";
               foreach($this->check_fields as $field_name=>$value)
                   $qry.=$field_name."='".$value."' AND ";
               $qry=substr($qry,0,strlen($qry)-5);
           }
          }

         return $this->query($qry);
        }

        function DB($host="",$user="",$password="",$dbname="",$open=false)
        {
         if($host!="")
            $this->host=$host;
         if($user!="")
            $this->user=$user;
         if($password!="")
            $this->password=$password;
         if($dbname!="")
            $this->database=$dbname;

         if($open)
           $this->open();
        }
        function open($host="",$user="",$password="",$dbname="") //
        {
         if($host!="")
            $this->host=$host;
         if($user!="")
            $this->user=$user;
         if($password!="")
            $this->password=$password;
         if($dbname!="")
            $this->database=$dbname;

         $this->connect();
         $this->select_db();
        }
        function set_host($host,$user,$password,$dbname)
        {
         $this->host=$host;
         $this->user=$user;
         $this->password=$password;
         $this->database=$dbname;
        }
        function affectedRows() //-- Get number of affected rows in previous operation
        {
         return @mysql_affected_rows($this->conn);
        }
        function close()//Close a connection to a  Server
        {
         return @mysql_close($this->conn);
        }
        function connect() //Open a connection to a Server
        {
          // Choose the appropriate connect function
          if ($this->persist)
              $func = 'mysql_pconnect';
          else
              $func = 'mysql_connect';

          // Connect to the database server
          $this->conn = $func($this->host, $this->user, $this->password);
          if(!$this->conn)
             return false;
              
        }
        function select_db($dbname="") //Select a databse
        {
          if($dbname=="")
             $dbname=$this->database;
          mysql_select_db($dbname,$this->conn);
        }
        function create_db($dbname) //Create a database
        {
          return @mysql_create_db($dbname,$this->conn);
        }
        function drop_db($dbname) //Drop a database
        {
         return @mysql_drop_db($dbname,$this->conn);
        }
        function data_seek($row) ///Move internal result pointer
        {
         return mysql_data_seek($this->result,$row);
        }
        function error() //Get last error
        {
            return (mysql_error());
        }
        function errorno() //Get error number
        {
            return mysql_errno();
        }
        function query($sql = '') //Execute the sql query
        {
            $this->result = @mysql_query($sql, $this->conn);
            return ($this->result != false);
        }
        function numRows() //Return number of rows in selected table
        {
            return (@mysql_num_rows($this->result));
        }
    	  function fieldName($field)
        {
           return (@mysql_field_name($this->result,$field));
        }
    	  function insertID()
        {
            return (@mysql_insert_id($this->conn));
        }
        function fetchObject()
        {
            return (@mysql_fetch_object($this->result, MYSQL_ASSOC));
        }
        function fetchArray($mode=MYSQL_BOTH)
        {
            return (@mysql_fetch_array($this->result,$mode));
        }
        function fetchAssoc()
        {
            return (@mysql_fetch_assoc($this->result));
        }
        function freeResult()
        {
            return (@mysql_free_result($this->result));
        }
		function getSingleResult($sql)
		{
			$this->query($sql);
			$row=$this->fetchArray(MYSQL_NUM);
			$return=$row[0];
			return $return;
		}
		function insert_data($table,$ex_flds)
		{
			$arr_types =  array("TR_", "TN_");
			while(list($key,$value)=each($_POST))
			{
				if(!in_array($key,$ex_flds))
				{
					$k=$key;
					for($p=0;$p<count($arr_types);$p++)
					{
						if(stristr($k,$arr_types[$p])!="")
						{
							$k = str_replace($arr_types[$p],"",$k);
						}
					}
					$fname.=$k."="."'$_POST[$key]'".",";
				}
			}
			$qry="INSERT INTO $table SET ".substr($fname,0,strlen($fname)-1)."";
			if($this->query($qry)==1)
			{
				return 1;
			}
		}
		
		function update_data($table,$ex_flds,$condt)
		{
			while(list($key,$value)=each($_POST))
			{
				$arr_types =  array("TR_", "TN_");
				if(!in_array($key,$ex_flds))
				{
					$k=$key;
					for($p=0;$p<count($arr_types);$p++)
					{
						if(stristr($k,$arr_types[$p])!="")
						{
							$k = str_replace($arr_types[$p],"",$k);
						}
					}
					$fname.=$k."="."'$_POST[$key]'".",";
				}
			}
			$qry="UPDATE $table SET ".substr($fname,0,strlen($fname)-1)." WHERE $condt";
			if($this->query($qry)==1)
			{
				return 1;
			}
		}
		
		function getAnyTableWhereData($table,$whereClause="")
		{
			$query="select * from $table where 1=1 $whereClause";
			$result=@mysql_query($query);
			if($row=@mysql_fetch_array($result))
			{
				@mysql_free_result($result);
				return $row;
			}
			else
			{
				return false;
			}
		}
		function getAnyTableData($table,$whereClause="")
		{
			$query="select * from $table where $whereClause";
			$result=@mysql_query($query);
			if($row=@mysql_fetch_array($result))
			{
				@mysql_free_result($result);
				return $row;
			}
			else
			{
				return false;
			}
		}
		function checkExist($tbl_name,$col_name,$val)
		{
			$query="SELECT $col_name FROM $tbl_name WHERE $col_name='$val'";
			$result=@mysql_query($query);
			if(!mysql_num_rows($result))
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		function checkUser($tbl_name,$col_fname,$col_lname,$val1,$val2)
		{
			$query="SELECT $col_fname,$col_lname FROM $tbl_name WHERE $col_fname='$val1' AND $col_lname='$val2'";
			$result=@mysql_query($query);
			if(!mysql_num_rows($result))
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
     }
?>