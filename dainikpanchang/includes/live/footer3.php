<script type="text/javascript">
	
	function openMusicCloud($val){
		$('#musicCloud').modal('show');
		$(".aplayer-list li:nth-child("+$val+")").click();
	}
	var windowWidth = $(window).width();
	if(windowWidth > 600){
$(window).scroll(function() {
  if ($(this).scrollTop() > 1){  
    $('header').addClass("sticky");
  }
  else{
    $('header').removeClass("sticky");
  }
});
}

setTimeout(function() {
                       $(".loader").fadeOut();
                        }, 5000);
</script>

    <script type="text/javascript" src="webroot/js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript" src="webroot/js/main.js"></script>
<!-- Modal -->
<div class="modal fade" id="musicCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <span class="glyphicon glyphicon-remove redcross" onclick="closeMusic()"></span>
      <div class="modal-body">
        <div id="player4" class="aplayer"></div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="loginCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog cb_modal" role="document">
    <div class="loginBox">
		<div class="__header">
				SIGN IN
				<span class="__dismiss">
					<span class="glyphicon glyphicon-remove" onclick="closeLogin()"></span>
				</span>
		</div>
		<div class="body">
		<div class="section-head">
				<h2>A Quest for <span class="jenna-sue">Truth</span></h2>
				<p>Instant sign in with</p>
			</div>
		<div class="divider"></div>
		<div class="col-xs-6 col-sm-6 col-md-6">
		
					<div class="signin-with-username">
							<p class="error-message">
								<span class="__exclaim-icon"></span>
								<span class="message">
									<span class="error-heading">error : </span> oops please enter a valid email address.
								</span>
							</p>
							<form class="signin-form struktur" id="iUserNameParent">
								<input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="email-input" placeholder="Enter your Email ID" id="iUserName" required="" minlength="1">
								<div id="dSignInErrorEmail" class="form-messages _success" style="display: none;"></div>

								<input type="password" minlength="1" class="password-input email-input" placeholder="Enter your password" id="iPwd" autocomplete="off">
								<div id="dSignInErrorPassword" class="form-messages _success" style="display: none;"></div>
								<!-- <div style="display: none;" class="form-messages _success" id="dAfterSignInSuccess">
									<p class="__text">Welcome back,<span id="spnUName"></span></p>
								</div> -->
								<a href="javascript:;" onclick="BMS.SignIn.fnValLogIn(); return false;"><div class="submit-form">SIGN IN</div></a>
							</form>
							<div class="forgot-password">
								<a href="#">
									<span class="__text" data-modal="forgotPopup">FORGOT PASSWORD?</span>
								</a>
							</div>
</div>
		
		</div>
			<div class="col-xs-6 col-sm-6 col-md-6">
			 <h1>Hello <?php session_start(); echo $_SESSION['USERNAME']; ?></h1>
			<div class="social-links">
							<a href="https://rgyan.com/includes/fbconfig.php" class="btn btn-block fsb btn-social btn-facebook" >
								<span class="fa socico fa-facebook"></span> Sign in with Facebook
							</a>
							<a class="btn btn-block gsb btn-social btn-google" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-google']);">
								<span class="fa socico fa-google"></span> Sign in with Google
							  </a>
				</div>
		</div>	
				
		<div style="clear:both;"></div>				
						<div class="divider">
						<?/*<span class="or">OR</span>*/?>
						</div>
						
						
			<div class="redirect-bottom">
						<p>Still not connected?<a class="signup-now-button" data-modal="signupPopup">Sign Up</a></p>
				</div>
		</div>
    </div>
  </div>
</div>
  </body>
</html>
<script>

var ap4 = new APlayer({
    element: document.getElementById('player4'),
    narrow: false,
    autoplay: false,
    showlrc: false,
    mutex: true,
    theme: '#ad7a86',
    music: [
    
        {
            title: 'Sunday Mantra',
            author: 'Shurya Mantra',
            url: 'http://rgyan.com/upload/song/Sunday.mp3',
            pic: 'http://rgyan.com/images/covers/surya1.jpg'
        },
        {
            title: 'Monday Mantra',
            author: 'Lord Shiva Mantra',
            url: 'http://rgyan.com/upload/song/Monday.mp3',
            pic: 'http://rgyan.com/images/covers/shiv.jpg'
        },
        {
            title: 'Tuesday Mantra',
            author: 'Hanuman Chalisha',
            url: 'http://rgyan.com/upload/song/Hanuman_Chalisa-(MyMp3Singer.com).mp3',
            pic: 'http://rgyan.com/images/covers/hanuman2.jpg'
        },
        {
            title: 'Wednesday Mantra',
            author: 'Ganesha Mantra',
            url: 'http://rgyan.com/upload/song/Ganesh_Mantra-(MyMp3Singer.com).mp3',
            pic: 'http://rgyan.com/images/covers/ganesh1.jpg'
        },
        {
            title: 'Thursday Mantra',
            author: 'Brihaspati Matra',
            url: 'http://rgyan.com/upload/song/Thursday.mp3',
            pic: 'http://rgyan.com/images/covers/vishnu1.jpg'
        },
        {
            title: 'Friday Mantra',
            author: 'Laxami Mantra',
            url: 'http://rgyan.com/upload/song/Friday.mp3',
            pic: 'http://rgyan.com/images/covers/lakshmi1.jpg'
        },
        {
            title: 'Saturday Mantra',
            author: 'Shani Mantra',
            url: 'http://rgyan.com/upload/song/Saturday.mp3',
            pic: 'http://rgyan.com/images/covers/sanidev2.jpg'
        },
        {
            title: 'Guruwani Mantra',
            author: 'Guruwani',
            url: 'http://rgyan.com/upload/song/Guruwani.mp3',
            pic: 'http://rgyan.com/images/covers/surya1.jpg'
        }
    ]
});
ap4.init();

$('#musicCloud').on('hidden.bs.modal', function () {
    ap4.pause();
})

function closeMusic(){
$('#musicCloud').modal('hide');
}
function closeLogin(){
$("#loginCloud").removeClass("in");
    $(".modal-backdrop").remove();
    $("#loginCloud").hide();
}
</script>