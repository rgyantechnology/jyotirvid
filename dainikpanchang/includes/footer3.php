<script type="text/javascript">
    function openMusicCloud($val) {
        $('#musicCloud').modal('show');
        $(".aplayer-list li:nth-child(" + $val + ")").click();
    }
    var windowWidth = $(window).width();
    if (windowWidth > 600) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $('header').addClass("sticky");
            }
            else {
                $('header').removeClass("sticky");
            }
        });
    }

    setTimeout(function () {
        $(".loader").fadeOut();
    }, 5000);
</script>
<script type="text/javascript" src="webroot/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="webroot/js/main.js"></script>
<!-- Modal -->
<style>
    #Registration input[type="submit"] {
        width:150px;
        text-align:center;
    }
    #frmLogin input[type="submit"] {
        width:150px;
        text-align:center;
    }
    .red {
        color: red;        
    }
    #loginCloud span[aria-label="Close"], #forgotPasswordCloud span[aria-label="Close"], #signupCloud span[aria-label="Close"] {
        cursor: pointer;
    }
    #loginCloud a[data-dismiss="modal"], #forgotPasswordCloud a[data-dismiss="modal"], #signupCloud a[data-dismiss="modal"] {
        cursor: pointer;
    }
</style>
<div class="modal fade" id="musicCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> <span class="glyphicon glyphicon-remove redcross" onclick="closeMusic()"></span>
            <div class="modal-body">
                <div id="player4" class="aplayer"></div>
            </div>
        </div>
    </div>
</div>
<!-- START LOGIN -->
<div class="modal fade" id="loginCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog cb_modal" role="document">
        <div class="loginBox">
            <div class="__header"> 
                Login here
                <span class="__dismiss"> 
                    <span data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove"></span></span>
                </span> 
            </div>
            <div class="body">                
                <div class="divider"></div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="signin-with-username">                        
                        <form method="post" role="form" id="frmLogin" name="frmLogin">
                            <div class="form-group" style="width:97%;">
                                <label>Email <span class="red">*</span></label>
                                <input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" placeholder="Enter your Email ID" id="user_email" name="user_email" required="" minlength="1" autocomplete="off">
                            </div>
                            <div class="form-group" style="width:97%;">
                                <label>Password <span class="red">*</span></label>
                                <input type="password" minlength="1" class="form-control" placeholder="Enter your password" id="user_password" name="user_password" autocomplete="off" required="">
                            </div>
                            <div class="form-group">           
                                <input type="submit" name="submitLoginForm" id="submitLogin" value="Submit"  class="submit-form">
                                <a data-toggle="modal" data-target="#forgotPasswordCloud" data-dismiss="modal"> FORGOT PASSWORD </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">          
                    <div class="social-links"> <a href="#" class="btn btn-block fsb btn-social btn-facebook" > <span class="fa socico fa-facebook"></span> Sign in with Facebook </a> <a class="btn btn-block gsb btn-social btn-google" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-google']);"> <span class="fa socico fa-google"></span> Sign in with Google </a> </div>
                </div>
                <div style="clear:both;"></div>
                <div class="divider">
                    <? /* <span class="or">OR</span> */ ?>
                </div>
                <div class="redirect-bottom">
                    <p>Already have an account? <a class="signup-now-button" data-toggle="modal" data-target="#signupCloud" data-dismiss="modal">New Registration</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="forgotPasswordCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog cb_modal" role="document" style="width:400px;">
        <div class="loginBox">
            <div class="__header"> Forgot Password 
                <span class="__dismiss"> 
                    <span data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove"></span></span>
                </span> 
            </div>
            <div class="body">
                <div class="divider"></div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="signin-with-username">
                        <form method="post" role="form" class="form-horizontal" id="frmLogin" name="frmLogin" action="">
                            <label>Email <span class="red">*</span></label>
                            <input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="email-input" placeholder="Enter your Email ID" id="user_email" name="user_email" required="" minlength="1">                     
                            <input type="submit" name="forgotPassword" id="submitLogin" value="Submit"  class="submit-form">
                        </form>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="divider"> </div>
                <div class="redirect-bottom">
                    <p><a class="signup-now-button" data-toggle="modal" data-target="#loginCloud" data-dismiss="modal">Login here</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- START RESG ******************************************** -->
<div class="modal fade" id="signupCloud" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog cb_modal" role="document">
        <div class="loginBox">
            <div class="__header"> New User Registration
                <span class="__dismiss"> 
                    <span data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-remove"></span></span>
                </span> 
            </div>
            <div class="body">                
                <div class="divider"></div>
                <div class="row">
                    <div class="signin-with-username">
                        <p class="error-message"> <span class="__exclaim-icon"></span> <span class="message"> <span class="error-heading">error : </span> oops please enter a valid email address. </span> </p>
                        <div class="tab-pane" id="Registration">
                            <form method="post" role="form" class="form-horizontal" id="frmReg" name="frmReg" action="">
                                <div class="col-xs-6 col-sm-8 col-md-6">
                                    <div class="form-group" style="width:97%;">
                                        <label>Name <span class="red">*</span></label>
                                        <input  class="form-control" required type="text" minlength="2" placeholder="Enter your name" id="name" name="name">
                                    </div>
                                    <div class="form-group" style="width:97%;">
                                        <label>Email <span class="red">*</span></label>
                                        <input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  class="form-control" placeholder="Enter your Email ID" id="email" name="email" required="" minlength="1" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-8 col-md-6">
                                    <div class="form-group">
                                        <label>Phone </label>
                                        <input type="text" minlength="2" class="form-control" placeholder="Enter your phone" id="phone" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label>Password <span class="red">*</span></label>
                                        <input required type="password" name="pass" minlength="1" class="password-input form-control" placeholder="Enter your password" id="iPwd" autocomplete="off">
                                    </div>              
                                </div>
                                <div class="col-xs-6 col-sm-8 col-md-6">
                                    <div class="form-group">
                                        <input type="submit" name="registrationForm" id="submit" value="Submit"  class="submit-form">
                                        <a data-toggle="modal" data-target="#forgotPasswordCloud" data-dismiss="modal"> FORGOT PASSWORD </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="divider"> </div>
                    <div class="redirect-bottom">
                        <p>
                            Already have an account?
                            <a class="signup-now-button" data-toggle="modal" data-target="#loginCloud" data-dismiss="modal">Login here</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var ap4 = new APlayer({
        element: document.getElementById('player4'),
        narrow: false,
        autoplay: false,
        showlrc: false,
        mutex: true,
        theme: '#ad7a86',
        music: [
            {
                title: 'Sunday Mantra',
                author: 'Shurya Mantra',
                url: 'http://rgyan.com/upload/song/Sunday.mp3',
                pic: 'http://rgyan.com/images/covers/surya1.jpg'
            },
            {
                title: 'Monday Mantra',
                author: 'Lord Shiva Mantra',
                url: 'http://rgyan.com/upload/song/Monday.mp3',
                pic: 'http://rgyan.com/images/covers/shiv.jpg'
            },
            {
                title: 'Tuesday Mantra',
                author: 'Hanuman Chalisha',
                url: 'http://rgyan.com/upload/song/Hanuman_Chalisa-(MyMp3Singer.com).mp3',
                pic: 'http://rgyan.com/images/covers/hanuman2.jpg'
            },
            {
                title: 'Wednesday Mantra',
                author: 'Ganesha Mantra',
                url: 'http://rgyan.com/upload/song/Ganesh_Mantra-(MyMp3Singer.com).mp3',
                pic: 'http://rgyan.com/images/covers/ganesh1.jpg'
            },
            {
                title: 'Thursday Mantra',
                author: 'Brihaspati Matra',
                url: 'http://rgyan.com/upload/song/Thursday.mp3',
                pic: 'http://rgyan.com/images/covers/vishnu1.jpg'
            },
            {
                title: 'Friday Mantra',
                author: 'Laxami Mantra',
                url: 'http://rgyan.com/upload/song/Friday.mp3',
                pic: 'http://rgyan.com/images/covers/lakshmi1.jpg'
            },
            {
                title: 'Saturday Mantra',
                author: 'Shani Mantra',
                url: 'http://rgyan.com/upload/song/Saturday.mp3',
                pic: 'http://rgyan.com/images/covers/sanidev2.jpg'
            },
            {
                title: 'Guruwani Mantra',
                author: 'Guruwani',
                url: 'http://rgyan.com/upload/song/Guruwani.mp3',
                pic: 'http://rgyan.com/images/covers/surya1.jpg'
            }
        ]
    });
    ap4.init();

    $('#musicCloud').on('hidden.bs.modal', function () {
        ap4.pause();
    })

    function closeMusic() {
        $('#musicCloud').modal('hide');
    }
    function closeLogin() {
        $("#loginCloud").removeClass("in");
        $(".modal-backdrop").remove();
        $("#loginCloud").hide();
    }
    function closeSignup() {
        $("#signupCloud").removeClass("in");
        $(".modal-backdrop").remove();
        $("#signupCloud").hide();
    }
</script>
