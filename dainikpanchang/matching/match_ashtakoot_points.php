<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Ashtakoot Points";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_ashtakoot_points');

?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <div class="UI-II zero-padding">
                    <section id="photos" class="match-ashtakoot-overlay">
                        <p style="top:15%"> Match Ashtakoot Points</p>

                        <p style="top:25%"><?=$data['total']['received_points']?> / <?=$data['total']['total_points']?></p>

                    </section>
                    <div class="table-bordered table-responsive">
                        <table class="table cust-table zero-margin">
                            <thead>
                            <tr>
                                <th>Atribute</th>
                                <th>Desc</th>
                                <th>Male</th>
                                <th>Female</th>
                                <th>Outof</th>
                                <th>Received</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Varna</td>
                                <td class="ng-binding"><?=$data['varna']['description']?></td>
                                <td class="ng-binding"><?=$data['varna']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['varna']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['varna']['total_points']?></td>
                                <td class="ng-binding"><?=$data['varna']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Vashya</td>
                                <td class="ng-binding"><?=$data['vashya']['description']?></td>
                                <td class="ng-binding"><?=$data['vashya']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vashya']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['vashya']['total_points']?></td>
                                <td class="ng-binding"><?=$data['vashya']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Tara</td>
                                <td class="ng-binding"><?=$data['tara']['description']?></td>
                                <td class="ng-binding"><?=$data['tara']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['tara']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['tara']['total_points']?></td>
                                <td class="ng-binding"><?=$data['tara']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Yoni</td>
                                <td class="ng-binding"><?=$data['yoni']['description']?></td>
                                <td class="ng-binding"><?=$data['yoni']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['yoni']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['yoni']['total_points']?></td>
                                <td class="ng-binding"><?=$data['yoni']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Maitri</td>
                                <td class="ng-binding"><?=$data['maitri']['description']?></td>
                                <td class="ng-binding"><?=$data['maitri']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['maitri']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['maitri']['total_points']?></td>
                                <td class="ng-binding"><?=$data['maitri']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Gan</td>
                                <td class="ng-binding"><?=$data['gan']['description']?></td>
                                <td class="ng-binding"><?=$data['gan']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['gan']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['gan']['total_points']?></td>
                                <td class="ng-binding"><?=$data['gan']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Bhakut</td>
                                <td class="ng-binding"><?=$data['bhakut']['description']?></td>
                                <td class="ng-binding"><?=$data['bhakut']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['bhakut']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['bhakut']['total_points']?></td>
                                <td class="ng-binding"><?=$data['bhakut']['received_points']?></td>
                            </tr>
                            <tr>
                                <td>Nadi</td>
                                <td class="ng-binding"><?=$data['nadi']['description']?></td>
                                <td class="ng-binding"><?=$data['nadi']['male_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['nadi']['female_koot_attribute']?></td>
                                <td class="ng-binding"><?=$data['nadi']['total_points']?></td>
                                <td class="ng-binding"><?=$data['nadi']['received_points']?></td>
                            </tr>
                            <tr style="background-color: #FF585A;color: #fff;">
                                <td>Total</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td class="ng-binding"><?=$data['total']['total_points']?></td>
                                <td class="ng-binding"><?=$data['total']['received_points']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="UI-II zero-padding">
                    <section id="photos" class="match-ashtakoot-overlay">
                        <p style="position: relative;top:10%"> Conclusion</p>
                            <?php
                            if($data['conclusion']['status'])
                            {
                                ?>
                                <i class="fa fa-thumbs-o-up ashtakoot-conclusion-thumb" ></i>
                                <?php
                            }
                            else
                            {
                                ?>
                                <i class="fa fa-thumbs-o-down ashtakoot-conclusion-thumb"></i>
                                <?php
                            }
                            ?>

                    </section>
                    <div class="ashtakoot-conclusion" style="padding: 10px;;">
                        <div class="ashtakoot-conculsion-report">
                            <p class="ng-binding"><?=$data['conclusion']['report']?></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php require_once("../footer.php"); ?>