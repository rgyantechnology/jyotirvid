<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Obstruction Reports";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_obstructions');

?>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="UI-II zero-padding">
                    <section id="photos" class="match-ashtakoot-overlay">
                        <p style="position: relative;top:10%"> Vedh Dosha Report</p>
                        <?php
                        if($data['is_present'])
                        {
                            ?>
                            <i class="fa fa-thumbs-o-down ashtakoot-conclusion-thumb" ></i>
                            <?php
                        }
                        else
                        {
                            ?>
                            <i class="fa fa-thumbs-o-up ashtakoot-conclusion-thumb"></i>
                            <?php
                        }
                        ?>

                    </section>
                    <div class="spacer-40 clearfix"></div>
                    <div class="ashtakoot-conculsion-report" style="padding: 10px;;">
                        <p class="ng-binding"><?=$data['vedha_report']?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>

        </div>

    </div>
<?php require_once("../footer.php"); ?>