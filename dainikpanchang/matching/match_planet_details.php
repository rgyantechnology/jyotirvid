<?php
require_once("../top.php");
require_once '../php/ApiCall.php';
$headerTitle = "Horoscope Matching Ashtakoot Points";
require_once("../matching_header.php");

$api = new ApiCall();

$data = $api->matchingApiCall('match_planet_details');

function DMS1($dec)
{
    $vars = explode(".",$dec);
    $deg = $vars[0];
    $tempma = "0.".$vars[1];
    $tempma = $tempma * 3600;
    $min = floor($tempma / 60);
    $sec =round($tempma - ($min*60));
    if($deg < 10) $deg = '0'.$deg;
    if($min < 10) $min = '0'.$min;
    if($sec < 10) $sec = '0'.$sec;
    $str = $deg . " : " . $min ." : " . $sec;
    return $str;
}

$planetData = $data['male_planet_details'];
$planetData1 = $data['female_planet_details'];
?>

<div class="row">
    <div class="col-md-12">
        <div class="UI-II ng-scope">
            <h2 class="text-info male" > <i class="fa fa-male "></i> Male Planet Details</h2>
            <div class="">
                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Planet</th>
                            <th>R</th>
                            <th>Sign</th>
                            <th>Sign Lord</th>
                            <th>Degree</th>
                            <th>Nakshatra</th>
                            <th>Nakshatra Lord</th>
                            <th>House</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Ascedent</td>
                            <?php
                            echo '<td>'."-".'</td>';
                            echo '<td>'.$planetData[9]['sign'].'</td>';
                            echo '<td>'.$planetData[9]['signLord'].'</td>';
                            echo '<td>'.DMS1($planetData[9]['normDegree']).'</td>';
                            echo '<td>'.$planetData[9]['nakshatra'].'</td>';
                            echo '<td>'.$planetData[9]['nakshatraLord'].'</td>';
                            echo '<td>'.$planetData[9]['house'].'</td>';
                            ?>
                        </tr>
                        <?php
                        $retro = " ";
                        for($i = 0; $i < 9; $i++)
                        {
                            $isRetro= $planetData[$i]['isRetro'];

                            if($isRetro === "true")
                            {
                                $retro = "R";
                            }
                            else
                            {
                                $retro = "-";
                            }
                            echo '<tr>';
                            echo '<td class="bold">'.$planetData[$i]['name'].'</td>';
                            echo '<td>'.$retro.'</td>';
                            echo '<td>'.$planetData[$i]['sign'].'</td>';
                            echo '<td>'.$planetData[$i]['signLord'].'</td>';
                            echo '<td>'.DMS1($planetData[$i]['normDegree']).'</td>';
                            echo '<td>'.$planetData[$i]['nakshatra'].'</td>';
                            echo '<td>'.$planetData[$i]['nakshatraLord'].'</td>';
                            echo '<td>'.$planetData[$i]['house'].'</td>';
                            echo '</tr>';
                        }

                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
            <div class="spacer-40 clearfix"></div>
            <h2 class="female"> <i class="fa fa-female "></i> Female Planet Details</h2>
            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Planet</th>
                        <th>R</th>
                        <th>Sign</th>
                        <th>Sign Lord</th>
                        <th>Degree</th>
                        <th>Nakshatra</th>
                        <th>Nakshatra Lord</th>
                        <th>House</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Ascedent</td>
                        <?php
                        echo '<td>'."-".'</td>';
                        echo '<td>'.$planetData1[9]['sign'].'</td>';
                        echo '<td>'.$planetData1[9]['signLord'].'</td>';
                        echo '<td>'.DMS1($planetData1[9]['normDegree']).'</td>';
                        echo '<td>'.$planetData1[9]['nakshatra'].'</td>';
                        echo '<td>'.$planetData1[9]['nakshatraLord'].'</td>';
                        echo '<td>'.$planetData1[9]['house'].'</td>';
                        ?>
                    </tr>
                    <?php
                    $retro = " ";
                    for($i = 0; $i < 9; $i++)
                    {
                        $isRetro= $planetData1[$i]['isRetro'];

                        if($isRetro === "true")
                        {
                            $retro = "R";
                        }
                        else
                        {
                            $retro = "-";
                        }
                        echo '<tr>';
                        echo '<td class="bold">'.$planetData1[$i]['name'].'</td>';
                        echo '<td>'.$retro.'</td>';
                        echo '<td>'.$planetData1[$i]['sign'].'</td>';
                        echo '<td>'.$planetData1[$i]['signLord'].'</td>';
                        echo '<td>'.DMS1($planetData1[$i]['normDegree']).'</td>';
                        echo '<td>'.$planetData1[$i]['nakshatra'].'</td>';
                        echo '<td>'.$planetData1[$i]['nakshatraLord'].'</td>';
                        echo '<td>'.$planetData1[$i]['house'].'</td>';
                        echo '</tr>';
                    }

                    ?>
                    </tbody>
                </table>

            </div>
        </div>

    </div>

</div>
<?php require_once("../footer.php"); ?>